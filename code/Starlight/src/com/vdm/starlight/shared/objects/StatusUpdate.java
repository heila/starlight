package com.vdm.starlight.shared.objects;

import java.io.Serializable;

public class StatusUpdate implements Serializable {

	private static final long serialVersionUID = 3560664777340048561L;

	public enum Status {
		ONLINE("online"), BUSY("busy"), AWAY("away"), OFFLINE("offline");
		String status;

		private Status(String status) {
			this.status = status;
		}

		@Override
		public String toString() {
			return status;
		}

	}

	private String username;
	private Status status;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
