package com.vdm.starlight.shared.objects;

import java.io.Serializable;

public class ChatMessage implements Serializable {
	private static final long serialVersionUID = -3810124498174491048L;
	private String sender;
	private String receiver;
	private String message;

	public ChatMessage() {
		sender = "";
		receiver = "";
		message = "";
	}

	public ChatMessage(String sender, String receiver, String message) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.message = message;
	}

	public String getSender() {
		return sender;
	}

	public String getReceiver() {
		return receiver;
	}

	public String getMessage() {
		return message;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
