package com.vdm.starlight.shared.objects;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(StudentWorkbookPK.class)
@Table(name = "STUDENT_WORKBOOK")
public class StudentWorkbook implements Serializable {
	private static final long serialVersionUID = -8267024062180888710L;

	@Id
	private Student student;

	@Id
	private Workbook workbook;

	@Basic
	private java.util.Date dateStarted;

	private java.util.Date dateCompleted;

	String data;

	public StudentWorkbook() {
	}

	public StudentWorkbook(Student s, Workbook w) {
		this.student = s;
		this.workbook = w;
		this.dateStarted = new java.util.Date();
		setDateCompleted();
	}

	public boolean isCompleted() {
		if (dateCompleted == null)
			return false;
		else
			return true;
	}

	public java.util.Date getDateStarted() {
		return dateStarted;
	}

	public void setDateStarted(java.util.Date dateStarted) {
		this.dateStarted = dateStarted;
	}

	public Student getStudent() {
		return student;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public void setDateCompleted() {
		if (student.getId() != 1)
			this.dateCompleted = new java.util.Date(2011 - 1900, 12, 10, 12, 12, 12);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		result = prime * result + ((workbook == null) ? 0 : workbook.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentWorkbook other = (StudentWorkbook) obj;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		if (workbook == null) {
			if (other.workbook != null)
				return false;
		} else if (!workbook.equals(other.workbook))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StudentWorkbook [studentID=" + student.getId() + ", workbookID=" + workbook.getId() + ", dateStarted="
				+ dateStarted + ", dateCompleted=" + dateCompleted + "]";
	}

}

@Embeddable
class StudentWorkbookPK implements Serializable, Comparable<StudentWorkbookPK> {
	private static final long serialVersionUID = -594849410706421474L;

	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID")
	private Student student;

	@ManyToOne
	@JoinColumn(name = "WORKBOOK_ID", referencedColumnName = "ID")
	private Workbook workbook;

	public StudentWorkbookPK() {
	}

	public StudentWorkbookPK(Student s, Workbook c) {
		this.student = s;
		this.workbook = c;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Student getStudent() {
		return student;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public int compareTo(StudentWorkbookPK arg0) {
		return (arg0 == null || arg0.getStudent() == null) ? -1 : -arg0.getStudent().compareTo(student);
	}

}
