package com.vdm.starlight.shared.objects;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * The Educator class represents an educator that has many {@link Classroom} and {@link Workbooks}. This class stores
 * their general information. It is annotated using the Java Persistence API to describe its Object/Relational Mapping.
 * 
 * The class extends {@link LightEntity} from the Gilead library to allow the transformation of persistent entities back
 * to simple Plain Old Java Object for transportation to the client.
 * 
 * @author heilavdm
 * 
 */
@Entity
@Table(name = "EDUCATOR")
public class Educator implements Serializable {

	private static final long serialVersionUID = 8299955197603856290L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Basic
	private String userName;

	private String password;

	@Basic
	private String name;

	@Basic
	private String surname;

	@Basic
	private String email;

	private boolean defaultImage;

	@ManyToOne
	private Resource image;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany
	private Set<Classroom> classes;

	@LazyCollection(LazyCollectionOption.FALSE)
	@OneToMany
	private Set<Workbook> workbooks;

	private Date startDate;

	private Date endDate;

	public Educator() {
	}

	public Educator(String userName, String password, String name, String surname, String email) {
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.startDate = new Date();
		this.endDate = null;
		this.defaultImage = true;

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return userName;
	}

	public void setName(String name) {
		this.userName = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String hashedPassword) {
		this.password = hashedPassword;
	}

	// Classroom getters and setters
	public Set<Classroom> getClassrooms() {
		return classes;
	}

	public void setClassrooms(Set<Classroom> classes) {
		this.classes = classes;
	}

	public void addClassroom(Classroom classroom) {
		if (classes == null) {
			classes = new HashSet<Classroom>();
		}
		classes.add(classroom);
	}

	// Workbook getters and setters
	public void addWorkbook(Workbook workbook) {
		if (workbooks == null) {
			workbooks = new HashSet<Workbook>();
		}
		workbooks.add(workbook);
	}

	public Set<Workbook> getWorkbooks() {
		return workbooks;
	}

	public void setWorkbooks(Set<Workbook> workbooks) {
		this.workbooks = workbooks;
	}

	// Image getters and Setters
	public void setImage(Resource imageRes) {
		defaultImage = false;
		this.image = imageRes;
	}

	public Resource getImage() {
		return this.image;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	// Override methods

	@Override
	public String toString() {
		return "Educator [id=" + id + ", userName=" + userName + ", password=" + password + ", name=" + name
				+ ", surname=" + surname + ", email=" + email + ", startDate=" + startDate + ", endDate=" + endDate
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((surname == null) ? 0 : surname.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Educator other = (Educator) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (surname == null) {
			if (other.surname != null)
				return false;
		} else if (!surname.equals(other.surname))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

}
