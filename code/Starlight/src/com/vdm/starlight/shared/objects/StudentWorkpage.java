package com.vdm.starlight.shared.objects;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(StudentWorkpagePK.class)
@Table(name = "STUDENT_WORKPAGE")
public class StudentWorkpage implements Serializable {
	private static final long serialVersionUID = -8267024062180888710L;

	@Id
	private Student student;

	@Id
	private Workpage workpage;

	@Basic
	private java.util.Date dateStarted;

	private java.util.Date dateCompleted;

	@Lob
	String data = "";

	String comment = "";

	public java.util.Date getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(java.util.Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public StudentWorkpage() {
	}

	public StudentWorkpage(Student s, Workpage w) {
		this.student = s;
		this.workpage = w;
		this.dateStarted = new java.util.Date();
	}

	public boolean isCompleted() {
		if (dateCompleted == null)
			return false;
		else
			return true;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public java.util.Date getDateStarted() {
		return dateStarted;
	}

	public void setDateStarted(java.util.Date dateStarted) {
		this.dateStarted = dateStarted;
	}

	public Student getStudent() {
		return student;
	}

	public Workpage getWorkpage() {
		return workpage;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public void setWorkpage(Workpage workpage) {
		this.workpage = workpage;
	}

	public void setDateCompleted() {
		if (student.getId() != 1)
			this.dateCompleted = new java.util.Date(2011 - 1900, 12, 10, 12, 12, 12);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		result = prime * result + ((workpage == null) ? 0 : workpage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentWorkpage other = (StudentWorkpage) obj;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		if (workpage == null) {
			if (other.workpage != null)
				return false;
		} else if (!workpage.equals(other.workpage))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StudentWorkpage [studentID=" + student.getId() + ", workpageID=" + workpage.getId() + ", dateStarted="
				+ dateStarted + ", dateCompleted=" + dateCompleted + "]";
	}

	public static class WorkpageData implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -8787653246104148915L;
		private String comment = "";
		private String data = "";
		private int studentId;
		private int pageId;
		private int workbookId;

		public WorkpageData() {

		}

		public WorkpageData(String comment, String data, int studentId, int pageId, int workbookId) {
			super();
			this.comment = comment;
			this.data = data;
			this.studentId = studentId;
			this.pageId = pageId;
			this.workbookId = workbookId;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public String getData() {
			return data;
		}

		public void setData(String data) {
			this.data = data;
		}

		public int getStudentId() {
			return studentId;
		}

		public void setStudentId(int studentId) {
			this.studentId = studentId;
		}

		public int getPageId() {
			return pageId;
		}

		public void setPageId(int pageId) {
			this.pageId = pageId;
		}

		public int getWorkbookId() {
			return workbookId;
		}

		public void setWorkbookId(int workbookId) {
			this.workbookId = workbookId;
		}

	}
}

@Embeddable
class StudentWorkpagePK implements Serializable, Comparable<StudentWorkpagePK> {

	private static final long serialVersionUID = -1828519133903442914L;

	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID")
	private Student student;

	@ManyToOne
	@JoinColumn(name = "WORKPAGE_ID", referencedColumnName = "ID")
	private Workpage workpage;

	public StudentWorkpagePK() {
	}

	public StudentWorkpagePK(Student s, Workpage c) {
		this.student = s;
		this.workpage = c;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Student getStudent() {
		return student;
	}

	public Workpage getWorkpage() {
		return workpage;
	}

	public void setWorkpage(Workpage workpage) {
		this.workpage = workpage;
	}

	public int compareTo(StudentWorkpagePK arg0) {
		return (arg0 == null || arg0.getStudent() == null) ? -1 : -arg0.getStudent().compareTo(student);
	}

}
