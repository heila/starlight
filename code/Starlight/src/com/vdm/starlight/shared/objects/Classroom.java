package com.vdm.starlight.shared.objects;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.google.gwt.view.client.ProvidesKey;

/**
 * Represents a virtual classroom. Connects an educator to students in the class.
 * 
 * @author heilamvdm
 * 
 */
@Entity
@Table(name = "CLASSROOM")
public class Classroom implements Serializable, Comparable<Classroom> {
	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<Classroom> KEY_PROVIDER = new ProvidesKey<Classroom>() {
		public Object getKey(Classroom item) {
			return item == null ? null : item.getId();
		}
	};
	private static final long serialVersionUID = 7917534894909646907L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String name;

	private String description;

	private Date dateCreated;

	private Date dateCompleted;

	private String ageGroup;

	private boolean defaultImage;

	@ManyToOne
	private Resource image;

	@ManyToOne
	private Educator creator;

	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	@OneToMany(mappedBy = "classroom")
	private Set<StudentClassroom> studentClassrooms;

	@ManyToMany(mappedBy = "classrooms")
	private Set<Student> students;

	@OneToMany(mappedBy = "classroom")
	private Set<ClassroomWorkbook> classroomWorkbooks;

	@ManyToMany
	@JoinTable(name = "CLASSROOM_WORKBOOK", joinColumns = { @JoinColumn(name = "CLASSROOM_ID") }, inverseJoinColumns = { @JoinColumn(name = "WORKBOOK_ID") })
	private Set<Workbook> workbooks;

	public Classroom() {
	}

	public Classroom(String name, Educator creator, String ageGroup, String description) {
		this.name = name;
		this.creator = creator;
		this.dateCreated = new java.util.Date();
		this.dateCompleted = null;
		this.setAgeGroup(ageGroup);
		this.setDescription(description);
		defaultImage = true;
	}

	// id getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// name getters and setters
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// creator getters and setters
	public Educator getCreator() {
		return this.creator;
	}

	public void setCreator(Educator educator) {
		this.creator = educator;
	}

	// date getters and setters
	public void setDateCreated(Date date) {
		dateCreated = date;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public boolean isClosed() {
		if (dateCompleted == null)
			return false;
		else
			return true;
	}

	public void setCompleted(Date date) {
		dateCompleted = date;
	}

	public Date getDateCompleted() {
		return dateCompleted;
	}

	// Description getters and setters
	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	// Image getter and setter
	public Resource getImage() {
		return image;
	}

	public void setImage(Resource image) {
		defaultImage = false;
		this.image = image;
	}

	// default image getter and setter
	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	// age getters and setters
	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	// Student methods
	public void addStudent(Student s, StudentClassroom sc) {
		if (students == null)
			students = new HashSet<Student>();

		if (studentClassrooms == null) {
			studentClassrooms = new HashSet<StudentClassroom>();
		}
		students.add(s);
		studentClassrooms.add(sc);
	}

	public Set<Student> getStudents() {
		return students;
	}

	public void setStudents(Set<Student> students) {
		this.students = students;
	}

	// Student classrooms methods
	public void addStudentClassroom(StudentClassroom studentClassroom) {
		if (studentClassrooms == null) {
			studentClassrooms = new HashSet<StudentClassroom>();
		}
		studentClassrooms.add(studentClassroom);
		if (!students.contains(studentClassroom.getStudent())) {
			students.add(studentClassroom.getStudent());
		}
	}

	public Set<StudentClassroom> getStudentClassrooms() {
		return studentClassrooms;
	}

	public void setStudentClassrooms(Set<StudentClassroom> result) {
		this.studentClassrooms = result;
	}

	// Workbooks
	public void addWorkbook(Workbook w) {
		if (workbooks == null)
			workbooks = new HashSet<Workbook>();

		if (classroomWorkbooks == null) {
			classroomWorkbooks = new HashSet<ClassroomWorkbook>();
		}
		classroomWorkbooks.add(new ClassroomWorkbook(this, w));
		workbooks.add(w);
	}

	public Set<Workbook> getWorkbooks() {
		return workbooks;
	}

	public void setWorkbooks(Set<Workbook> workbooks) {
		this.workbooks = workbooks;
	}

	// Classroom Workbooks
	public void addClassroomWorkbook(ClassroomWorkbook classroomWorkbook) {
		if (this.classroomWorkbooks == null)
			this.classroomWorkbooks = new HashSet<ClassroomWorkbook>();
		classroomWorkbooks.add(classroomWorkbook);
		if (this.workbooks.contains(classroomWorkbook.getWorkbook()))
			this.workbooks.add(classroomWorkbook.getWorkbook());
	}

	public Set<ClassroomWorkbook> getClassroomWorkbooks() {
		return this.classroomWorkbooks;
	}

	public void setClassroomWorkbooks(Set<ClassroomWorkbook> studentWorkbooks) {
		this.classroomWorkbooks = studentWorkbooks;
	}

	// Override Methods

	@Override
	public String toString() {
		return "Classroom [id=" + id + ", name=" + name + ", description=" + description + ", dateCreated="
				+ dateCreated + ", dateCompleted=" + dateCompleted + ", ageGroup=" + ageGroup + ", creator=" + creator
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ageGroup.hashCode();
		result = prime * result + ((dateCompleted == null) ? 0 : dateCompleted.hashCode());
		result = prime * result + ((dateCreated == null) ? 0 : dateCreated.hashCode());
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classroom other = (Classroom) obj;
		if (ageGroup != other.ageGroup)
			return false;
		if (dateCompleted == null) {
			if (other.dateCompleted != null)
				return false;
		} else if (!dateCompleted.equals(other.dateCompleted))
			return false;
		if (dateCreated == null) {
			if (other.dateCreated != null)
				return false;
		} else if (!dateCreated.equals(other.dateCreated))
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public int compareTo(Classroom o) {
		return -(o.getId() - id);
	}

}
