package com.vdm.starlight.shared.objects;

public class ChatException extends Exception {

	public ChatException() {
	}

	public ChatException(String message) {
		super(message);
	}
}
