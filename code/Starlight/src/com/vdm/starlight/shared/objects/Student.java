package com.vdm.starlight.shared.objects;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

@Entity
@Table(name = "STUDENT")
public class Student implements IsSerializable, Comparable<Student> {

	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<Student> KEY_PROVIDER = new ProvidesKey<Student>() {
		public Object getKey(Student item) {
			return item == null ? null : item.getId();
		}
	};

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Basic
	private String userName;

	private String password;

	@Basic
	private String name;

	@Basic
	private String surname;

	private int age;

	private boolean defaultImage;

	@ManyToOne
	private Resource image;

	@OneToMany(mappedBy = "student")
	private Set<StudentClassroom> studentClassrooms;

	@ManyToMany
	@JoinTable(name = "STUDENT_CLASSROOM", joinColumns = { @JoinColumn(name = "STUDENT_ID") }, inverseJoinColumns = { @JoinColumn(name = "CLASSROOM_ID") })
	private Set<Classroom> classrooms;

	@OneToMany(mappedBy = "student")
	private Set<StudentWorkbook> studentWorkbooks;

	@ManyToMany
	@JoinTable(name = "STUDENT_WORKBOOK", joinColumns = { @JoinColumn(name = "STUDENT_ID") }, inverseJoinColumns = { @JoinColumn(name = "WORKBOOK_ID") })
	private Set<Workbook> workbooks;

	private Date startDate;

	private Date endDate;

	@Transient
	private String status = "offline";

	// Constructors
	public Student() {
	}

	public Student(String userName, String password, String name, String surname, int age) {
		this.userName = userName;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.startDate = new Date();
		this.endDate = null;
		this.defaultImage = true;
		this.age = age;
	}

	// Id getters and Setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	// Username getters and Setters
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	// Password getters and Setters
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// Name getters and Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// Surname getters and Setters
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	// StartDate getters and Setters
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	// EndDate getters and Setters
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	// Status getters and Setters
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	// Student Classrooms getters and Setters
	public Set<StudentClassroom> getStudentClassrooms() {
		return studentClassrooms;
	}

	public void setStudentClassrooms(Set<StudentClassroom> studentclassrooms) {
		this.studentClassrooms = studentclassrooms;
	}

	public void addStudentClassroom(StudentClassroom studentClassroom) {
		if (this.studentClassrooms == null)
			this.studentClassrooms = new HashSet<StudentClassroom>();
		studentClassrooms.add(studentClassroom);
		if (this.classrooms.contains(studentClassroom.getClassroom()))
			this.classrooms.add(studentClassroom.getClassroom());
	}

	// Classroom getters and Setters
	public void addClassroom(Classroom classroom, StudentClassroom sc) {
		if (classrooms == null)
			classrooms = new HashSet<Classroom>();

		if (studentClassrooms == null) {
			studentClassrooms = new HashSet<StudentClassroom>();
		}
		studentClassrooms.add(sc);
		classrooms.add(classroom);
	}

	// Workbook getters and Setters
	public void addWorkbook(Workbook workbook, StudentWorkbook sw) {
		if (workbooks == null)
			workbooks = new HashSet<Workbook>();

		if (studentWorkbooks == null) {
			studentWorkbooks = new HashSet<StudentWorkbook>();
		}
		studentWorkbooks.add(sw);
		workbooks.add(workbook);
	}

	// Student Workbooks getters and Setters

	public void addStudentWorkbook(StudentWorkbook studentWorkbook) {
		if (this.studentWorkbooks == null)
			this.studentWorkbooks = new HashSet<StudentWorkbook>();
		studentWorkbooks.add(studentWorkbook);
		if (this.workbooks.contains(studentWorkbook.getWorkbook()))
			this.workbooks.add(studentWorkbook.getWorkbook());
	}

	public Set<StudentWorkbook> getStudentWorkbooks() {
		return this.studentWorkbooks;
	}

	public void setStudentWorkbooks(Set<StudentWorkbook> studentWorkbooks) {
		this.studentWorkbooks = studentWorkbooks;
	}

	// Image getters and Setters
	public void setImage(Resource imageRes) {
		defaultImage = false;
		this.image = imageRes;
	}

	public Resource getImage() {
		return this.image;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	// Age getters and Setters
	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	// Override Methods
	@Override
	public String toString() {
		return "Student [id=" + id + ", userName=" + userName + ", password=" + password + ", name=" + name
				+ ", surname=" + surname + ", startDate=" + startDate + ", endDate=" + endDate + ", status=" + status
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	@Override
	public int compareTo(Student o) {
		return -(o.getId() - id);
	}

}
