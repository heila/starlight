package com.vdm.starlight.shared.objects;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.gwt.view.client.ProvidesKey;

@Entity
@IdClass(StudentClassroomPK.class)
@Table(name = "STUDENT_CLASSROOM")
public class StudentClassroom implements Serializable {
	private static final long serialVersionUID = -3300623406264082975L;

	/**
	 * The key provider that provides the unique ID of a contact.
	 */
	public static final ProvidesKey<StudentClassroom> KEY_PROVIDER = new ProvidesKey<StudentClassroom>() {
		public Object getKey(StudentClassroom item) {
			return (item == null) ? null : new StudentClassroomPK(item.student, item.classroom);
		}
	};

	@Id
	private Student student;

	@Id
	private Classroom classroom;

	@Basic
	private Date dateEnrolled;

	@Basic
	private Date dateCompleted;

	private String status;

	@Basic(fetch = FetchType.LAZY)
	private String chatHistory;

	public StudentClassroom() {
	}

	public StudentClassroom(Student s, Classroom c) {
		this.student = s;
		this.classroom = c;
	}

	public Date getDateEnrolled() {
		return dateEnrolled;
	}

	public void setDateEnrolled(Date dateEnrolled) {
		this.dateEnrolled = dateEnrolled;
	}

	public Date getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChatHistory() {
		return chatHistory;
	}

	public void setChatHistory(String chatHistory) {
		this.chatHistory = chatHistory;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classroom == null) ? 0 : classroom.hashCode());
		result = prime * result + ((student == null) ? 0 : student.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentClassroom other = (StudentClassroom) obj;
		if (classroom == null) {
			if (other.classroom != null)
				return false;
		} else if (!classroom.equals(other.classroom))
			return false;
		if (student == null) {
			if (other.student != null)
				return false;
		} else if (!student.equals(other.student))
			return false;
		return true;
	}

}

@Embeddable
class StudentClassroomPK implements Serializable, Comparable<StudentClassroomPK> {
	private static final long serialVersionUID = 3742647960329897612L;

	@ManyToOne
	@JoinColumn(name = "STUDENT_ID", referencedColumnName = "ID")
	private Student student;
	@ManyToOne
	@JoinColumn(name = "CLASSROOM_ID", referencedColumnName = "ID")
	private Classroom classroom;

	public StudentClassroomPK() {
	}

	public StudentClassroomPK(Student s, Classroom c) {
		this.student = s;
		this.classroom = c;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Student getStudent() {
		return student;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public int compareTo(StudentClassroomPK arg0) {
		return (arg0 == null || arg0.getStudent() == null) ? -1 : -arg0.getStudent().compareTo(student);
	}
}
