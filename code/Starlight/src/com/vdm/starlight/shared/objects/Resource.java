package com.vdm.starlight.shared.objects;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * Stores the meta-data of an image stored on the server's file system. The {@link Resource} class of all images are
 * stored in the database in the {@link Resource} table.
 * 
 * @author Heila van der Merwe
 * 
 */
@Entity
@Table(name = "RESOURCE")
// The name of the table storing these resource objects
public class Resource implements Serializable {
	private static final long serialVersionUID = -409841570913114071L;

	/**
	 * The unique identifier of the image in the {@link Resource} table. This value is automatically generated by the
	 * database
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	/** The name of the image file without any extensions */
	private String name;

	/** Image file extension (type) */
	private String type;

	/** The fully qualified path to the image file on the file system of the server */
	private String path;

	/** The image bytes of the thumbnail of the image as an encoded String */
	@Lob
	@Column(name = "IMAGE")
	private String imageData;

	public Resource(String name, String type, String path, String imageData) {
		super();
		this.name = name;
		this.type = type;
		this.path = path;
		this.imageData = imageData;
	}

	private Resource() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getImageData() {
		return imageData;
	}

	public void setImageBytes(String imageData) {
		this.imageData = imageData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
}
