package com.vdm.starlight.shared.objects;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Workpage implements Serializable {
	private static final long serialVersionUID = 4713708933571962337L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;

	String name;

	private int uniqueComponentId;

	@Lob
	private String componentDataString;

	private int pageNumber;

	@ManyToOne(fetch = FetchType.LAZY)
	private Workbook workbook;

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public Workpage() {
	}

	public Workpage(String name) {
		super();
		this.name = name;
		this.uniqueComponentId = 0;
		componentDataString = "";
	}

	public Workpage(String name, Workbook workbook, int pageNumber) {
		this.name = name;
		this.workbook = workbook;
		workbook.addWorkPage(this);
		this.uniqueComponentId = 0;
		componentDataString = "";
		this.pageNumber = pageNumber;

	}

	public Workpage(Workpage page) {
		super();
		this.name = page.name;
		this.uniqueComponentId = page.uniqueComponentId;
		this.componentDataString = page.componentDataString;
		this.pageNumber = page.pageNumber;
		this.workbook = page.workbook;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setWorkBook(Workbook workbook) {
		this.workbook = workbook;
	}

	public Workbook getWorkbook() {
		return this.workbook;
	}

	public String getComponentDataString() {
		return componentDataString;
	}

	public void setComponentDataString(String componentDataString) {
		this.componentDataString = componentDataString;
	}

	public int getUniqueComponentId() {
		return uniqueComponentId;
	}

	public void setUniqueComponentId(int uniqueComponentId) {
		this.uniqueComponentId = uniqueComponentId;
	}

}