package com.vdm.starlight.shared.objects;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@IdClass(ClassroomWorkbookPK.class)
@Table(name = "CLASSROOM_WORKBOOK")
public class ClassroomWorkbook implements Serializable {
	private static final long serialVersionUID = -8267024062180888710L;

	@Id
	private Classroom classroom;

	@Id
	private Workbook workbook;

	@Basic
	private Date dateAdded;

	// @OneToMany(mappedBy = "student")
	// private Set<StudentComponent> studentComponents;
	//
	// @ManyToMany
	// @JoinTable(name = "STUDENT_COMPONENT", joinColumns = { @JoinColumn(name =
	// "STUDENT_ID") }, inverseJoinColumns = { @JoinColumn(name =
	// "COMPONENT_ID") })
	// private Set<Component> components;
	//

	public ClassroomWorkbook() {
	}

	public ClassroomWorkbook(Classroom c, Workbook w) {
		this.classroom = c;
		this.workbook = w;
	}

	public Date getDateStarted() {
		return dateAdded;
	}

	public void setDateStarted(Date dateStarted) {
		this.dateAdded = dateStarted;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public void setClassroom(Classroom classroom) {
		this.classroom = classroom;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((classroom == null) ? 0 : classroom.hashCode());
		result = prime * result + ((workbook == null) ? 0 : workbook.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ClassroomWorkbook other = (ClassroomWorkbook) obj;
		if (classroom == null) {
			if (other.classroom != null)
				return false;
		} else if (!classroom.equals(other.classroom))
			return false;
		if (workbook == null) {
			if (other.workbook != null)
				return false;
		} else if (!workbook.equals(other.workbook))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClassroomWorkbook [classID=" + classroom.getId() + ", workbookID=" + workbook.getId()
				+ ", dateStarted=" + dateAdded + "]";
	}

}

@Embeddable
class ClassroomWorkbookPK implements Serializable, Comparable<ClassroomWorkbookPK> {
	private static final long serialVersionUID = -594849410706421474L;

	@ManyToOne
	@JoinColumn(name = "CLASSROOM_ID", referencedColumnName = "ID")
	private Classroom classroom;

	@ManyToOne
	@JoinColumn(name = "WORKBOOK_ID", referencedColumnName = "ID")
	private Workbook workbook;

	public ClassroomWorkbookPK() {
	}

	public ClassroomWorkbookPK(Classroom s, Workbook c) {
		this.classroom = s;
		this.workbook = c;
	}

	public void setStudent(Classroom classroom) {
		this.classroom = classroom;
	}

	public Classroom getClassroom() {
		return classroom;
	}

	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}

	public Workbook getWorkbook() {
		return workbook;
	}

	public int compareTo(ClassroomWorkbookPK arg0) {
		return (arg0 == null || arg0.getClassroom() == null) ? -1 : -arg0.getClassroom().compareTo(classroom);
	}
}
