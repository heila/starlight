package com.vdm.starlight.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

/**
 * Object used for transport of basic information of Workpage objects between client and server side. The purpose of
 * this object is to decrease the size of the objects transported.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkpageDTO implements IsSerializable, Comparable<WorkpageDTO> {

	public static final ProvidesKey<WorkpageDTO> KEY_PROVIDER = new ProvidesKey<WorkpageDTO>() {
		public Object getKey(WorkpageDTO item) {
			return item == null ? null : item.getId();
		}
	};

	private int id;
	private String name;
	private int pageNumber;
	private int uniqueComponentId;

	public WorkpageDTO() {
	}

	public WorkpageDTO(int id, String name, int uniqueid, int pageNumber) {
		super();
		this.id = id;
		this.name = name;
		this.uniqueComponentId = uniqueid;
		this.pageNumber = pageNumber;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public int getUniqueComponentId() {
		return uniqueComponentId;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public void setUniqueComponentId(int uniqueComponentId) {
		this.uniqueComponentId = uniqueComponentId;
	}

	@Override
	public int compareTo(WorkpageDTO o) {
		return -(o.getId() - this.getId());
	}

}