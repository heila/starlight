package com.vdm.starlight.shared.dto;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

/**
 * Object used for transport of basic information of Workbook objects between client and server side. The purpose of
 * this object is to decrease the size of the objects transported.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookDTO implements IsSerializable, Comparable<WorkbookDTO> {

	public static final ProvidesKey<WorkbookDTO> KEY_PROVIDER = new ProvidesKey<WorkbookDTO>() {
		public String getKey(WorkbookDTO item) {
			return item == null ? null : item.getName();
		}
	};

	private int id;
	private String name;
	private String description;

	private Date dateCreated;
	private Date dateModified;

	private String ageGroup;
	private String image;

	private boolean defaultImage;

	public WorkbookDTO() {
	}

	public WorkbookDTO(int id, String name, String description, Date dateCreated, Date dateModified, String ageGroup,
			String thumbnail, boolean defaultImage) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateCreated = dateCreated;
		this.dateModified = dateModified;
		this.ageGroup = ageGroup;
		this.image = thumbnail;
		this.defaultImage = defaultImage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateModified() {
		return dateModified;
	}

	public void setDateModified(Date dateModified) {
		this.dateModified = dateModified;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String thumbnail) {
		this.image = thumbnail;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	@Override
	public int compareTo(WorkbookDTO o) {
		return this.getName().compareTo(o.getName());
	}

}