package com.vdm.starlight.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

public class UserDTO implements IsSerializable {

	private String username;
	private boolean isEducator;

	public UserDTO() {

	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isEducator() {
		return isEducator;
	}

	public void setEducator(boolean isEducator) {
		this.isEducator = isEducator;
	}

	public UserDTO(String username, boolean isEducator) {
		super();
		this.username = username;
		this.isEducator = isEducator;
	}

}
