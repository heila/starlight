package com.vdm.starlight.shared.dto;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * Object used for transport of all information of a Workbook object between client and server side. The purpose of this
 * object is to decrease the size of the objects transported.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookStatsDTO implements IsSerializable {
	private int numPages;
	private int numStudents;
	private int numCompleted;
	private int numClasrooms;

	public WorkbookStatsDTO() {
	}

	public WorkbookStatsDTO(int numPages, int numStudents, int numCompleted, int numClasrooms) {
		super();
		this.numPages = numPages;
		this.numStudents = numStudents;
		this.numCompleted = numCompleted;
		this.numClasrooms = numClasrooms;
	}

	public int getNumPages() {
		return numPages;
	}

	public void setNumPages(int numPages) {
		this.numPages = numPages;
	}

	public int getNumStudents() {
		return numStudents;
	}

	public void setNumStudents(int numStudents) {
		this.numStudents = numStudents;
	}

	public int getNumCompleted() {
		return numCompleted;
	}

	public void setNumCompleted(int numCompleted) {
		this.numCompleted = numCompleted;
	}

	public int getNumClasrooms() {
		return numClasrooms;
	}

	public void setNumClasrooms(int numClasrooms) {
		this.numClasrooms = numClasrooms;
	}

}
