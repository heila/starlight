package com.vdm.starlight.shared.dto;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class WorkbookPagesDTO implements IsSerializable {
	private String name;
	private int id;

	private List<WorkpageDTO> pages;

	public WorkbookPagesDTO() {
	}

	public WorkbookPagesDTO(String name, int id, List<WorkpageDTO> pages) {
		super();
		this.name = name;
		this.id = id;
		this.pages = pages;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<WorkpageDTO> getPages() {
		return pages;
	}

	public void setPages(List<WorkpageDTO> pages) {
		this.pages = pages;
	}

}
