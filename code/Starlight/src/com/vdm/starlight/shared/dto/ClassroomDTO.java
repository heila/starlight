package com.vdm.starlight.shared.dto;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;

/**
 * Object used for transport of basic information of Classroom objects between client and server side. The purpose of
 * this object is to decrease the size of the objects transported.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ClassroomDTO implements IsSerializable, Comparable<ClassroomDTO> {
	public static final ProvidesKey<ClassroomDTO> KEY_PROVIDER = new ProvidesKey<ClassroomDTO>() {
		public Object getKey(ClassroomDTO item) {
			return item == null ? null : item.getName();
		}
	};

	private int id;
	private String name;
	private String description;

	private Date dateCreated;
	private Date dateCompleted;
	private String ageGroup;

	private String image;
	private boolean defaultImage;

	public ClassroomDTO() {
	}

	public ClassroomDTO(int id, String name, String description, Date dateCreated, Date dateCompleted, String ageGroup,
			String image, boolean defaultImage) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.dateCreated = dateCreated;
		this.dateCompleted = dateCompleted;
		this.ageGroup = ageGroup;
		this.image = image;
		this.defaultImage = defaultImage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getDateCompleted() {
		return dateCompleted;
	}

	public void setDateCompleted(Date dateCompleted) {
		this.dateCompleted = dateCompleted;
	}

	public String getAgeGroup() {
		return ageGroup;
	}

	public void setAgeGroup(String ageGroup) {
		this.ageGroup = ageGroup;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	@Override
	public int compareTo(ClassroomDTO o) {
		return -(o.id - this.id);
	}

}
