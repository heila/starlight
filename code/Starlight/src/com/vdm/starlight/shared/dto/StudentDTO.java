package com.vdm.starlight.shared.dto;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.google.gwt.view.client.ProvidesKey;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;
import com.vdm.starlight.shared.objects.Student;

/**
 * Object used for transport of basic information of {@link Student} objects between client and server side. The purpose
 * of this object is to decrease the size of the objects transported.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentDTO implements IsSerializable, Comparable<StudentDTO> {
	private int id;
	private String name;
	private String username;
	private String surname;
	private int age;

	private Date startDate;
	private Date endDate;

	private String image;
	public boolean defaultImage;

	private Status status;

	public StudentDTO() {
	}

	public StudentDTO(int id, String name, String username, String surname, int age, Date startDate, Date endDate,
			String image, boolean defaultImage) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.surname = surname;
		this.age = age;
		this.startDate = startDate;
		this.endDate = endDate;
		this.image = image;
		this.defaultImage = defaultImage;
		this.status = Status.ONLINE;
	}

	public static final ProvidesKey<StudentDTO> KEY_PROVIDER = new ProvidesKey<StudentDTO>() {
		public Object getKey(StudentDTO item) {
			return item == null ? null : item.getName();
		}
	};

	@Override
	public int compareTo(StudentDTO o) {
		return -(o.getId() - id);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
