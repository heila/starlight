package com.vdm.starlight.shared.dto;

import java.util.List;

import com.google.gwt.user.client.rpc.IsSerializable;

public class WorkpageDataDTO implements IsSerializable {
	private List<String> data;
	private int workpageId;

	public WorkpageDataDTO() {

	}

	public WorkpageDataDTO(List<String> data, int workpageId) {
		super();
		this.data = data;
		this.workpageId = workpageId;
	}

	public List<String> getData() {
		return data;
	}

	public void setData(List<String> data) {
		this.data = data;
	}

	public int getWorkpageId() {
		return workpageId;
	}

	public void setWorkpageId(int workpageId) {
		this.workpageId = workpageId;
	}

}
