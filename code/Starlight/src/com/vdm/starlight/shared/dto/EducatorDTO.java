package com.vdm.starlight.shared.dto;

import java.util.Date;

import com.google.gwt.user.client.rpc.IsSerializable;
import com.vdm.starlight.shared.objects.Resource;

public class EducatorDTO implements IsSerializable {

	private boolean defaultImage;

	private Date endDate;

	private Resource image;

	private String name;

	private Date startDate;

	private String surname;

	private String userName;

	public EducatorDTO() {

	}

	public EducatorDTO(String userName, String name, String surname, boolean defaultImage, Resource image,
			Date startDate, Date endDate) {
		super();
		this.userName = userName;
		this.name = name;
		this.surname = surname;
		this.defaultImage = defaultImage;
		this.image = image;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public Resource getImage() {
		return image;
	}

	public String getName() {
		return name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public String getSurname() {
		return surname;
	}

	public String getUserName() {
		return userName;
	}

	public boolean isDefaultImage() {
		return defaultImage;
	}

	public void setDefaultImage(boolean defaultImage) {
		this.defaultImage = defaultImage;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setImage(Resource image) {
		this.image = image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
