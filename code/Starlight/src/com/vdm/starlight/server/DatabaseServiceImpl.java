package com.vdm.starlight.server;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.vdm.starlight.client.DatabaseService;
import com.vdm.starlight.server.dao.ClassroomDao;
import com.vdm.starlight.server.dao.EducatorDao;
import com.vdm.starlight.server.dao.ResourceDao;
import com.vdm.starlight.server.dao.StudentClassDao;
import com.vdm.starlight.server.dao.StudentDao;
import com.vdm.starlight.server.dao.StudentWorkbookDao;
import com.vdm.starlight.server.dao.StudentWorkpageDao;
import com.vdm.starlight.server.dao.WorkBookDao;
import com.vdm.starlight.server.dao.WorkpageDao;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.dto.ResourceDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.dto.WorkpageDataDTO;
import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Resource;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentWorkpage;
import com.vdm.starlight.shared.objects.Workbook;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * The server side implementation of the Database RPC service.
 */
public class DatabaseServiceImpl extends RemoteServiceServlet implements DatabaseService {
	static Logger logger = Logger.getLogger(" DatabaseServiceImpl ");

	private static final long serialVersionUID = -6918098966588936027L;
	ClassroomDao classDao = new ClassroomDao();

	EducatorDao educatorDao = new EducatorDao();
	ResourceDao resourceDao = new ResourceDao();

	StudentClassDao studentClassDao = new StudentClassDao();
	StudentDao studentDao = new StudentDao();

	StudentWorkbookDao studentWorkBookDao = new StudentWorkbookDao();
	StudentWorkpageDao studentWorkpageDao = new StudentWorkpageDao();
	WorkBookDao workbookDao = new WorkBookDao();

	WorkpageDao workpageDao = new WorkpageDao();

	@Override
	public void addStudentToClassroom(int studentId, int classroomId) {
		studentClassDao.persist(studentId, classroomId);
	}

	/**
	 * Adds the workbook to the classroom.
	 * 
	 * @param workbookId
	 *            The id of the workbook.
	 * @param classroomId
	 *            The id of the classroom.
	 */
	@Override
	public void addWorkbookToClassroom(int workbookId, int classroomId) {
		classDao.addWorkbook(classroomId, workbookId);
	}

	/**
	 * Adds the workbook to the classroom AND to all students in the classroom.
	 * 
	 * @param workbookId
	 * @param classroomId
	 */
	@Override
	public void addWorkbookToClassroomStudents(int workbookId, int classroomId) {
		classDao.addWorkbook(classroomId, workbookId);
		// add to students is class
		studentDao.addWorkbookToStudents(classroomId, workbookId);
	}

	/**
	 * Adds a workbook to a single student in a classroom. This implicidly adds the workbook to the classroom.
	 * 
	 * @param workbookId
	 *            The id of the workbook.
	 * @param studentId
	 *            The id of the student.
	 * @param classroomId
	 *            The id of the classroom.
	 */
	@Override
	public void addWorkbookToStudent(int workbookId, int studentId, int classroomId) {
		studentWorkBookDao.persist(studentId, workbookId);
		addWorkbookToClassroom(workbookId, classroomId);
	}

	/**
	 * Persist new {@link Classroom} object into the database. It then sets the id field of the object and returns the
	 * object with its id set.
	 * 
	 * @param classroom
	 *            - The new classroom item to add to the database.
	 * 
	 * @return the {@link Classroom} object's id field set.
	 */
	@Override
	public int createClassroom(String name, int creatorId, String ageGroup, String description) {
		return classDao.persist(name, creatorId, ageGroup, description);
	}

	/**
	 * Persist new {@link Educator} object into the database. It then sets the id field of the object and returns the
	 * object with its id set.
	 * 
	 * @param educator
	 *            - The new educator item to add to the database.
	 * 
	 * @return the {@link Educator} object's id field set.
	 */
	@Override
	public int createEducator(String userName, String password, String name, String surname, String email) {
		Educator educator = new Educator(userName, password, name, surname, email);
		educator.setId(educatorDao.persist(educator));
		return educator.getId();
	}

	/**
	 * Persist new {@link Resource} object into the database. It then sets the id field of the object and returns the
	 * object's id.
	 * 
	 * @param classroom
	 *            - The new Resource object to add to the database.
	 * 
	 * @return the {@link Resource} object's id field.
	 */
	@Override
	public int createResource(String name, String type, String path, String imageData) {
		return resourceDao.persist(new Resource(name, type, path, imageData));
	}

	/* ******************************* persist objects**************************************** */
	/**
	 * Persists a new {@link Student} object into the database. It sets the id field of the object and returns the
	 * object with its id set.
	 * 
	 * @param student
	 *            - The new student item to add to the database.
	 * 
	 * @return the {@link Student} object's id field set.
	 */
	@Override
	public int createStudent(String userName, String password, String name, String surname, int age) {
		Student student = new Student(userName, password, name, surname, age);
		student.setId(studentDao.persist(student));
		return student.getId();
	}

	@Override
	public int createStudent(String userName, String password, String name, String surname, int age, int imageID) {
		Student student = new Student(userName, password, name, surname, age);
		student.setId(studentDao.persist(student));
		if (imageID != -1) {
			setStudentImage(student.getId(), imageID);
		}
		return student.getId();
	}

	/**
	 * Persist new {@link Workbook} object into the database. It then sets the id field of the object and returns the
	 * object with its id set.
	 * 
	 * @param workbook
	 *            - The new workbook item to add to the database.
	 * 
	 * @return the {@link Workbook} object's id field set.
	 */
	@Override
	public int createWorkbook(String name, String description, String ageGroup, int creatorId) {
		return workbookDao.persist(name, description, creatorId);
	}

	@Override
	public int createWorkbook(String name, String description, String ageGroup, int creatorId, int imageID) {
		int workbookID = createWorkbook(name, description, ageGroup, creatorId);
		if (imageID > 0) {
			setWorkbookImage(workbookID, imageID);
		}
		return workbookID;
	}

	/**
	 * Persist new {@link Resource} object into the database. It then sets the id field of the object and returns the
	 * object's id.
	 * 
	 * @param classroom
	 *            - The new Resource object to add to the database.
	 * 
	 * @return the {@link Resource} object's id field.
	 */
	@Override
	public int createWorkpage(String name, int workbookId, int pageNumber) {
		return workpageDao.persist(name, workbookId, pageNumber);
	}

	@Override
	public void deleteResource(int resourceID) {
		Resource res = resourceDao.getResource(resourceID);
		try {
			File f = new File(res.getPath());
			f.delete();
		} catch (Exception e) {

		}
		resourceDao.deleteResource(res);

	}

	// Get Resource Methods
	@Override
	public List<ResourceDTO> getAllResources(int start) {
		int end = start + 12;
		// Get List off all books of the educator
		List<Resource> images = resourceDao.getAllResources();

		// Check if this is the last sublist
		if (images.size() <= end)
			if (images.size() < start) { // if last sublist had all in
				return new ArrayList<ResourceDTO>();
			} else {
				images = images.subList(start, images.size());
			}
		else {
			images = images.subList(start, end);
		}
		List<ResourceDTO> result = new ArrayList<ResourceDTO>();
		for (Resource r : images) {
			result.add(new ResourceDTO(r.getId(), r.getName(), r.getType(), r.getImageData()));
		}
		return result;
	}

	@Override
	public ClassroomDTO getClassroom(int classroomID, boolean fullImage) {
		Classroom c = classDao.getClassroom(classroomID);
		if (c == null) // no such workbook exists
			return null;

		String encodedData = "";

		if (!c.isDefaultImage()) {
			if (!fullImage) {
				encodedData = "data:image/" + c.getImage().getType() + ";base64," + c.getImage().getImageData();
			} else {
				try {
					encodedData = "data:image/" + c.getImage().getType() + ";base64,"
							+ ImageProcessing.getImageFromPath(c.getImage().getPath());
				} catch (Exception e) {
				}
			}
		}
		ClassroomDTO classroomDTO = new ClassroomDTO(c.getId(), c.getName(), c.getDescription(), c.getDateCreated(),
				c.getDateCompleted(), c.getAgeGroup(), encodedData, c.isDefaultImage());
		return classroomDTO;
	}

	public List<StudentDTO> getClassroomStudents(int classroomId, int start, int end, boolean fullImage) {
		List<StudentDTO> studentinfo = getClassroomStudents(classroomId, fullImage);
		// Check if this is the last sublist
		if (studentinfo.size() <= end)
			if (studentinfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<StudentDTO>();
			} else
				return new ArrayList<StudentDTO>(studentinfo.subList(start, studentinfo.size()));
		else
			return new ArrayList<StudentDTO>(studentinfo.subList(start, end));
	}

	@Override
	public List<StudentDTO> getClassroomStudents(int classroomId, boolean fullImage) {
		List<StudentDTO> studentinfo = new LinkedList<StudentDTO>();

		List<Student> students = classDao.getStudents(classroomId);

		if (students == null)
			return studentinfo;

		String encodedData = "";

		for (Student s : students) {
			encodedData = "";

			if (!s.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + s.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(s.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			studentinfo.add(new StudentDTO(s.getId(), s.getName(), s.getUserName(), s.getSurname(), s.getAge(), s
					.getStartDate(), s.getEndDate(), encodedData, s.isDefaultImage()));
		}
		Collections.sort(studentinfo);
		return studentinfo;
	}

	public List<WorkbookDTO> getClassroomWorkbooks(int classroomId, int start, int end, boolean fullImage) {
		List<WorkbookDTO> books = new LinkedList<WorkbookDTO>();
		books = getClassroomWorkbooks(classroomId, fullImage);

		// Check if this is the last sublist
		if (books.size() <= end)
			if (books.size() < start) { // if last sublist had 5 in
				return new LinkedList<WorkbookDTO>();
			} else
				return new LinkedList<WorkbookDTO>(books.subList(start, books.size()));
		else
			return new LinkedList<WorkbookDTO>(books.subList(start, end));

	}

	public List<WorkbookDTO> getClassroomWorkbooks(int classroomId, boolean fullImage) {
		List<WorkbookDTO> books = new LinkedList<WorkbookDTO>();

		// Get list off all books of the educator
		List<Workbook> workbooks = classDao.getWorkbooks(classroomId);

		if (workbooks == null) // no such workbooks exists
			return books;

		String encodedData = "";
		for (Workbook w : workbooks) {
			if (!w.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + w.getImage().getType() + ";base64," + w.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + w.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(w.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			books.add(new WorkbookDTO(w.getId(), w.getName(), w.getDescription(), w.getDateCreated(), w
					.getDateModified(), w.getAgeGroup(), encodedData, w.isDefaultImage()));
		}
		Collections.sort(books);

		return books;

	}

	public String getDummyImage() {
		String result = null;
		try {
			result = ImageProcessing.getImage();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

	@Override
	public EducatorDTO getEducator(int id) {
		EducatorDTO educator = null;
		Educator e = educatorDao.getEducator(id);
		if (e != null) {
			educator = new EducatorDTO(e.getUserName(), e.getName(), e.getSurname(), e.isDefaultImage(), e.getImage(),
					e.getStartDate(), e.getEndDate());
		}
		return educator;
	}

	@Override
	public List<ClassroomDTO> getEducatorClassrooms(int educatorId, int start, int end, boolean fullImage) {
		List<ClassroomDTO> classroominfo = new ArrayList<ClassroomDTO>();

		List<Classroom> classrooms = educatorDao.getClassrooms(educatorId);

		if (classrooms == null) {
			return classroominfo;
		}

		String encodedData = "";
		for (Classroom c : classrooms) {
			encodedData = "";
			if (!c.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + c.getImage().getType() + ";base64," + c.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + c.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(c.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			classroominfo.add(new ClassroomDTO(c.getId(), c.getName(), c.getDescription(), c.getDateCreated(), c
					.getDateCompleted(), c.getAgeGroup(), encodedData, c.isDefaultImage()));
		}
		Collections.sort(classrooms);

		// Check if this is the last sublist
		if (classroominfo.size() <= end)
			if (classroominfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<ClassroomDTO>();
			} else
				return new ArrayList<ClassroomDTO>(classroominfo.subList(start, classroominfo.size()));
		else
			return new ArrayList<ClassroomDTO>(classroominfo.subList(start, end));
	}

	@Override
	public List<StudentDTO> getEducatorStudents(int educatorId, int start, int end, boolean fullImage) {
		List<StudentDTO> studentinfo = new LinkedList<StudentDTO>();
		// Get List off all books of the educator
		List<Student> students = educatorDao.getStudents(educatorId);

		if (students == null)
			return studentinfo;

		String encodedData = "";
		for (Student s : students) {
			encodedData = "";

			if (!s.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + s.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(s.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			studentinfo.add(new StudentDTO(s.getId(), s.getName(), s.getUserName(), s.getSurname(), s.getAge(), s
					.getStartDate(), s.getEndDate(), encodedData, s.isDefaultImage()));
		}
		Collections.sort(studentinfo);

		// Check if this is the last sublist
		if (studentinfo.size() <= end)
			if (studentinfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<StudentDTO>();
			} else
				return new ArrayList<StudentDTO>(studentinfo.subList(start, students.size()));
		else
			return new ArrayList<StudentDTO>(studentinfo.subList(start, end));
	}

	@Override
	public List<WorkbookDTO> getEducatorWorkbooks(int educatorId, int start, int end, boolean fullImage) {
		List<WorkbookDTO> books = new LinkedList<WorkbookDTO>();

		// Get list off all books of the educator
		List<Workbook> workbooks = educatorDao.getWorkbooks(educatorId);

		if (workbooks == null) // no such workbooks exists
			return books;

		String encodedData = "";
		for (Workbook w : workbooks) {
			if (!w.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + w.getImage().getType() + ";base64," + w.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + w.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(w.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			books.add(new WorkbookDTO(w.getId(), w.getName(), w.getDescription(), w.getDateCreated(), w
					.getDateModified(), w.getAgeGroup(), encodedData, w.isDefaultImage()));
		}
		Collections.sort(books);

		// Check if this is the last sublist
		if (books.size() <= end)
			if (books.size() < start) { // if last sublist had 5 in
				return new LinkedList<WorkbookDTO>();
			} else
				return new LinkedList<WorkbookDTO>(books.subList(start, books.size()));
		else
			return new LinkedList<WorkbookDTO>(books.subList(start, end));
	}

	public String getFullImage(int id) {
		Resource res = resourceDao.getResource(id);
		String result = "";
		if (res != null) {
			try {
				result = ImageProcessing.getImageFromPath(res.getPath());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public WorkpageDTO getNewWorkpage(String name, int workbookId, int pageNumber) {
		Workpage page = workpageDao.getWorkpage(workpageDao.persist(name, workbookId, pageNumber));
		return new WorkpageDTO(page.getId(), page.getName(), page.getUniqueComponentId(), page.getPageNumber());

	}

	@Override
	public int getNumberOfPages(int workbookId) {
		int count = workpageDao.getNumPages(workbookId);
		return count;
	}

	@Override
	public StudentDTO getStudent(int studentId, boolean fullImage) {
		Student s = studentDao.getStudent(studentId);

		if (s == null) // no such workbook exists
			return null;

		String encodedData = "";

		if (!s.isDefaultImage()) {
			if (!fullImage) {
				encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
			} else {
				try {
					encodedData = "data:image/" + s.getImage().getType() + ";base64,"
							+ ImageProcessing.getImageFromPath(s.getImage().getPath());
				} catch (Exception e) {
				}
			}
		}
		StudentDTO student = new StudentDTO(s.getId(), s.getName(), s.getUserName(), s.getSurname(), s.getAge(),
				s.getStartDate(), s.getEndDate(), encodedData, s.isDefaultImage());

		return student;
	}

	@Override
	public List<ClassroomDTO> getStudentClassrooms(int studentId, boolean fullImage) {
		List<ClassroomDTO> classroominfo = new ArrayList<ClassroomDTO>();

		List<Classroom> classrooms = studentDao.getClassrooms(studentId);
		String encodedData = "";
		for (Classroom c : classrooms) {
			encodedData = "";
			if (!c.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + c.getImage().getType() + ";base64," + c.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + c.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(c.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			classroominfo.add(new ClassroomDTO(c.getId(), c.getName(), c.getDescription(), c.getDateCreated(), c
					.getDateCompleted(), c.getAgeGroup(), encodedData, c.isDefaultImage()));
		}
		Collections.sort(classrooms);
		return classroominfo;

	}

	@Override
	public List<ClassroomDTO> getStudentClassrooms(int studentId, int start, int end, boolean fullImage) {
		List<ClassroomDTO> classroominfo = getStudentClassrooms(studentId, fullImage);
		// Check if this is the last sublist
		if (classroominfo.size() <= end)
			if (classroominfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<ClassroomDTO>();
			} else
				return new ArrayList<ClassroomDTO>(classroominfo.subList(start, classroominfo.size()));
		else
			return new ArrayList<ClassroomDTO>(classroominfo.subList(start, end));
	}

	@Override
	public List<WorkbookDTO> getStudentWorkbooks(int studentId, boolean fullImage) {
		List<WorkbookDTO> workbookinfo = new LinkedList<WorkbookDTO>();

		List<Workbook> workbooks = studentDao.getWorkbooks(studentId);
		if (workbooks == null) // no such workbook exists
			return workbookinfo;

		String encodedData = "";
		for (Workbook w : workbooks) {
			encodedData = "";
			if (!w.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + w.getImage().getType() + ";base64," + w.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + w.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(w.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			workbookinfo.add(new WorkbookDTO(w.getId(), w.getName(), w.getDescription(), w.getDateCreated(), w
					.getDateModified(), w.getAgeGroup(), encodedData, w.isDefaultImage()));
		}
		Collections.sort(workbooks);

		return workbookinfo;
	}

	@Override
	public List<WorkbookDTO> getStudentWorkbooks(int studentId, int classId, boolean fullImage) {
		List<WorkbookDTO> workbookinfo = new LinkedList<WorkbookDTO>();

		List<Workbook> workbooks = studentDao.getWorkbooks(studentId, classId);
		if (workbooks == null) // no such workbook exists
			return workbookinfo;

		String encodedData = "";

		for (Workbook w : workbooks) {
			if (!w.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + w.getImage().getType() + ";base64," + w.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + w.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(w.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			workbookinfo.add(new WorkbookDTO(w.getId(), w.getName(), w.getDescription(), w.getDateCreated(), w
					.getDateModified(), w.getAgeGroup(), encodedData, w.isDefaultImage()));
		}
		Collections.sort(workbooks);

		return workbookinfo;
	}

	@Override
	public List<WorkbookDTO> getStudentWorkbooks(int studentId, int start, int end, boolean fullImage) {
		List<WorkbookDTO> books = new LinkedList<WorkbookDTO>();
		books = getStudentWorkbooks(studentId, fullImage);

		// Check if this is the last sublist
		if (books.size() <= end)
			if (books.size() < start) { // if last sublist had 5 in
				return new LinkedList<WorkbookDTO>();
			} else
				return new LinkedList<WorkbookDTO>(books.subList(start, books.size()));
		else
			return new LinkedList<WorkbookDTO>(books.subList(start, end));
	}

	@Override
	public WorkbookDTO getWorkbook(int workbookId, boolean fullImage) {
		Workbook w = workbookDao.getWorkbook(workbookId);
		if (w == null) // no such workbook exists
			return null;

		String encodedData = "";

		if (!w.isDefaultImage()) {
			if (!fullImage) {
				encodedData = "data:image/" + w.getImage().getType() + ";base64," + w.getImage().getImageData();
			} else {
				try {
					encodedData = "data:image/" + w.getImage().getType() + ";base64,"
							+ ImageProcessing.getImageFromPath(w.getImage().getPath());
				} catch (Exception e) {
				}
			}
		}
		WorkbookDTO workbookDTO = new WorkbookDTO(w.getId(), w.getName(), w.getDescription(), w.getDateCreated(),
				w.getDateModified(), w.getAgeGroup(), encodedData, w.isDefaultImage());
		return workbookDTO;
	}

	@Override
	public List<ClassroomDTO> getWorkbookClassrooms(int workbookId, int start, int end, boolean fullImage) {
		List<ClassroomDTO> classroominfo = new LinkedList<ClassroomDTO>();
		// Get List off all books of the educator
		List<Classroom> classrooms = workbookDao.getClassrooms(workbookId);

		if (classrooms == null)
			return classroominfo;

		String encodedData = "";
		for (Classroom s : classrooms) {
			encodedData = "";

			if (!s.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + s.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(s.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			classroominfo.add(new ClassroomDTO(s.getId(), s.getName(), s.getDescription(), s.getDateCreated(), s
					.getDateCompleted(), s.getAgeGroup(), encodedData, s.isDefaultImage()));
		}
		Collections.sort(classroominfo);

		// Check if this is the last sublist
		if (classroominfo.size() <= end)
			if (classroominfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<ClassroomDTO>();
			} else
				return new ArrayList<ClassroomDTO>(classroominfo.subList(start, classroominfo.size()));
		else
			return new ArrayList<ClassroomDTO>(classroominfo.subList(start, end));
	}

	@Override
	public List<ClassroomDTO> getWorkbookAvailableClassrooms(int educatorId, int workbookId, int start, int end,
			boolean fullImage) {
		List<ClassroomDTO> classroominfo = new LinkedList<ClassroomDTO>();
		// Get List off all books of the educator
		List<Classroom> classrooms = educatorDao.getAvailableClassrooms(educatorId);

		if (classrooms == null)
			return classroominfo;

		Workbook w = new Workbook();
		w.setId(workbookId);
		String encodedData = "";
		for (Classroom s : classrooms) {

			if (!contains(workbookId, s.getWorkbooks())) {
				encodedData = "";

				if (!s.isDefaultImage()) {
					if (!fullImage) {
						encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
					} else {
						try {
							encodedData = "data:image/" + s.getImage().getType() + ";base64,"
									+ ImageProcessing.getImageFromPath(s.getImage().getPath());
						} catch (Exception e) {
						}
					}
				}
				classroominfo.add(new ClassroomDTO(s.getId(), s.getName(), s.getDescription(), s.getDateCreated(), s
						.getDateCompleted(), s.getAgeGroup(), encodedData, s.isDefaultImage()));

			}
		}
		Collections.sort(classroominfo);

		// Check if this is the last sublist
		if (classroominfo.size() <= end)
			if (classroominfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<ClassroomDTO>();
			} else
				return new ArrayList<ClassroomDTO>(classroominfo.subList(start, classroominfo.size()));
		else
			return new ArrayList<ClassroomDTO>(classroominfo.subList(start, end));
	}

	public boolean contains(int workbookId, Set<Workbook> workbooks) {
		for (Workbook w : workbooks) {
			if (w.getId() == workbookId)
				return true;
		}

		return false;

	}

	public List<WorkbookDTO> getWorkbooks() {
		return new LinkedList<WorkbookDTO>();
	}

	@Override
	public WorkbookStatsDTO getWorkbookStat(int workbookId) {
		List<Integer> stats = workbookDao.getWorkbookStats(workbookId);
		return new WorkbookStatsDTO(stats.get(0), stats.get(1), stats.get(2), stats.get(3));
	}

	@Override
	public List<StudentDTO> getWorkbookStudents(int workbookId, boolean fullImage) {
		List<StudentDTO> studentinfo = new LinkedList<StudentDTO>();

		List<Student> students = workbookDao.getStudents(workbookId);
		if (students == null)
			return studentinfo;

		String encodedData = "";
		for (Student s : students) {
			encodedData = "";

			if (!s.isDefaultImage()) {
				if (!fullImage) {
					encodedData = "data:image/" + s.getImage().getType() + ";base64," + s.getImage().getImageData();
				} else {
					try {
						encodedData = "data:image/" + s.getImage().getType() + ";base64,"
								+ ImageProcessing.getImageFromPath(s.getImage().getPath());
					} catch (Exception e) {
					}
				}
			}
			studentinfo.add(new StudentDTO(s.getId(), s.getName(), s.getUserName(), s.getSurname(), s.getAge(), s
					.getStartDate(), s.getEndDate(), encodedData, s.isDefaultImage()));
		}
		Collections.sort(studentinfo);
		return studentinfo;
	}

	@Override
	public List<StudentDTO> getWorkbookStudents(int workbookId, int start, int end, boolean fullImage) {
		List<StudentDTO> studentinfo = new LinkedList<StudentDTO>();
		studentinfo = getWorkbookStudents(workbookId, fullImage);
		// Check if this is the last sublist
		if (studentinfo.size() <= end)
			if (studentinfo.size() < start) { // if last sublist had 5 in
				return new ArrayList<StudentDTO>();
			} else
				return new ArrayList<StudentDTO>(studentinfo.subList(start, studentinfo.size()));
		else
			return new ArrayList<StudentDTO>(studentinfo.subList(start, end));
	}

	@Override
	public WorkpageDataDTO getWorkpage(int workbookId, int pagenumber) {
		Workpage workpage = workpageDao.getWorkpage(workbookId, pagenumber);
		if (workpage != null) {
			String[] wpcds = workpage.getComponentDataString().split("\\$");
			List<String> result = new LinkedList<String>();
			for (int l = 1; l < wpcds.length; l++) {
				result.add(wpcds[l]);
			}
			return new WorkpageDataDTO(result, workpage.getId());
		}
		return null;
	}

	@Override
	public String getWorkpageComment(int studentId, int workpageId) {
		StudentWorkpage sw = studentWorkpageDao.getStudentWorkpage(workpageId, studentId);
		return sw.getComment();
	}

	@Override
	public List<String> getWorkpageData(int workpageId) {
		Workpage workpage = workpageDao.getWorkpage(workpageId);
		String[] wpcds = workpage.getComponentDataString().split("\\$");
		List<String> result = new LinkedList<String>();
		for (int l = 1; l < wpcds.length; l++) {
			result.add(wpcds[l]);
		}
		return result;
	}

	@Override
	public WorkbookPagesDTO getWorkpages(int workbookId) {
		// Get List off all books of the educator
		Workbook workbook = workbookDao.getWorkbook(workbookId);
		List<WorkpageDTO> workpages = workbookDao.getWorkpages(workbookId);
		return new WorkbookPagesDTO(workbook.getName(), workbookId, workpages);
	}

	@Override
	public List<String> getWorkpageStudentData(int studentId, int workpageId) {
		StudentWorkpage sw = studentWorkpageDao.getStudentWorkpage(workpageId, studentId);
		List<String> data = new LinkedList<String>();
		if (!sw.getData().equals("")) {
			String[] components = sw.getData().split("\\$");
			for (String s : components) {
				data.add(s);
			}
		}
		return data;
	}

	@Override
	public int saveImage(String filename) {
		String extension = filename.substring(filename.lastIndexOf(".") + 1);
		String name = filename.substring(0, filename.lastIndexOf("."));
		// make sure image size is within bounds and make the filename unique
		String path = ImageProcessing.getResizedImageFromPath("/starlight-data/" + filename, 948, 598, true);
		// create the resource object associated with this image
		int resourceID = createResource(name, extension, path, ImageProcessing.createThumbnailFromPath(path));
		// remove the old image file from disk
		File old = new File("/starlight-data/" + filename);
		old.delete();
		return resourceID;

	}

	@Override
	public void saveWorkbook(WorkbookDTO workbookDTO, int imageID) {
		Workbook w = workbookDao.getWorkbook(workbookDTO.getId());
		w.setAgeGroup(workbookDTO.getAgeGroup());
		w.setDescription(workbookDTO.getDescription());
		w.setName(workbookDTO.getName());
		workbookDao.updateWorkbook(w);

		if (imageID > 0) {
			setWorkbookImage(workbookDTO.getId(), imageID);
		}
	}

	public void saveWorkpageDescription(int workpageID, String description, int uniqueID) {
		workpageDao.updateDescription(workpageID, description, uniqueID);
	}

	@Override
	public void setClassroomImage(int classroomId, int resourceId) {
		resourceDao.addClassroomImage(resourceId, classroomId);
	}

	@Override
	public void setEducatorImage(int educatorId, int resourceId) {
		resourceDao.addEducatorImage(resourceId, educatorId);
	}

	@Override
	public void setStudentImage(int studentId, int resourceId) {
		resourceDao.addStudentImage(resourceId, studentId);
	}

	@Override
	public void setStudentWorkpageData(String data, int studentId, int workpageId) {
		studentWorkpageDao.saveData(studentId, workpageId, data);
	}

	@Override
	public void setWorkbookImage(int workbookId, int resourceId) {
		resourceDao.addWorkbookImage(resourceId, workbookId);
	}

	@Override
	public void setWorkpageComment(int studentId, int workpageId, String comment) {
		studentWorkpageDao.saveComment(studentId, workpageId, comment);
	}

	@Override
	public int createClassroom(String name, String description, String ageGroup, int creatorId, int imageID) {
		int classroomID = createClassroom(name, creatorId, ageGroup, description);
		if (imageID > 0) {
			setClassroomImage(classroomID, imageID);
		}
		return classroomID;
	}

	@Override
	public void saveClassroom(ClassroomDTO classroomDTO, int imageID) {
		Classroom c = classDao.getClassroom(classroomDTO.getId());
		c.setAgeGroup(classroomDTO.getAgeGroup());
		c.setDescription(classroomDTO.getDescription());
		c.setName(classroomDTO.getName());
		classDao.updateClassroom(c);

		if (imageID > 0) {
			setClassroomImage(classroomDTO.getId(), imageID);
		}
	}

}