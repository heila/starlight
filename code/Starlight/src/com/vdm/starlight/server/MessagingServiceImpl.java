package com.vdm.starlight.server;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.zschech.gwt.comet.server.CometServlet;
import net.zschech.gwt.comet.server.CometSession;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.vdm.starlight.client.MessagingService;
import com.vdm.starlight.server.dao.EducatorDao;
import com.vdm.starlight.server.dao.StudentDao;
import com.vdm.starlight.shared.dto.UserDTO;
import com.vdm.starlight.shared.objects.ChatException;
import com.vdm.starlight.shared.objects.ChatMessage;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.StatusUpdate;
import com.vdm.starlight.shared.objects.Student;

/**
 * The server side implementation of the Database RPC service.
 */
public class MessagingServiceImpl extends RemoteServiceServlet implements MessagingService {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5625365556365650470L;
	StudentDao studentDao = new StudentDao();
	EducatorDao educatorDao = new EducatorDao();

	/**
	 * A mapping of user names to CometSessions used for routing messages.
	 */
	private ConcurrentMap<String, CometSession> users = new ConcurrentHashMap<String, CometSession>();

	/**
	 * Authenticates a user by their username . If the password is correct, the user object is returned
	 */
	@Override
	public int authenticate(String username, String password) {
		// Get or create the HTTP session for the browser
		HttpSession httpSession = getThreadLocalRequest().getSession();
		int ret = -1;

		Educator educator = educatorDao.getEducator(username);
		if (educator != null && educator.getPassword().equals(password.trim())) {
			// Get or create the Comet session for the browser
			CometSession cometSession = CometServlet.getCometSession(httpSession);
			// Remember the user name for the
			httpSession.setAttribute("username", username);
			httpSession.setAttribute("UserID", educator.getId());
			httpSession.setAttribute("isEducator", true);

			// setup the mapping of user names to CometSessions
			if (users.putIfAbsent(username, cometSession) != null) {
				// some one else has already logged in with this user name
				httpSession.invalidate();
				return -2;
				// throw new ChatException("User: " + username + " already logged in");
			}

			ret = educator.getId();
		} else {
			Student student = studentDao.getStudent(username);
			if (student != null && student.getPassword().equals(password.trim())) {
				// Get or create the Comet session for the browser
				CometSession cometSession = CometServlet.getCometSession(httpSession);
				// Remember the user name for the
				httpSession.setAttribute("username", username);
				httpSession.setAttribute("UserID", student.getId());
				httpSession.setAttribute("isEducator", false);

				// setup the mapping of user names to CometSessions
				if (users.putIfAbsent(username, cometSession) != null) {
					// some one else has already logged in with this user name
					httpSession.invalidate();
					return -2;
				}
				ret = student.getId();
			}

		}
		return ret;
	}

	@Override
	public UserDTO getUsername() throws ChatException {
		// check if there is a HTTP session setup.
		HttpSession httpSession = getThreadLocalRequest().getSession(false);
		if (httpSession == null) {
			return null;
		}

		// return the user name
		return new UserDTO((String) httpSession.getAttribute("username"),
				(Boolean) httpSession.getAttribute("isEducator"));
	}

	@Override
	public boolean isValidSession(int id) {
		HttpServletRequest request = this.getThreadLocalRequest();
		// Get Session if it already exists (don't create a new one)
		HttpSession checkUserSession = request.getSession(false);
		if (checkUserSession == null) {
			return false;
		} else {
			// There is a session, but is the user logged in?
			Integer userID = ((Integer) checkUserSession.getAttribute("UserID"));
			if (userID != null && userID.equals(id))
				return true;
			else {
				checkUserSession.invalidate();
				return false;
			}
		}
	}

	/**
	 * @see net.zschech.gwt.chat.client.ChatService#logout(java.lang.String)
	 */
	@Override
	public void logout(String username) throws ChatException {
		// check if there is a HTTP session setup.
		HttpSession httpSession = getThreadLocalRequest().getSession(false);
		if (httpSession == null) {
			return;// throw new ChatException("User: " + username + " is not logged in: no http session");
		}

		// check if there is a Comet session setup. In a larger application the HTTP session may have been
		// setup via other means.
		CometSession cometSession = CometServlet.getCometSession(httpSession, false);
		if (cometSession == null) {
			return;// throw new ChatException("User: " + username + " is not logged in: no comet session");
		}

		// check the user name parameter matches the HTTP sessions user name
		if (!username.equals(httpSession.getAttribute("username"))) {
			return;// throw new ChatException("User: " + username + " is not logged in on this session");
		}

		// remove the mapping of user name to CometSession
		users.remove(username, cometSession);
		httpSession.invalidate();
	}

	/**
	 * @see net.zschech.gwt.chat.client.ChatService#send(java.lang.String)
	 */
	@Override
	public void send(String message, String receiver) throws ChatException {
		// check if there is a HTTP session setup.
		HttpSession httpSession = getThreadLocalRequest().getSession(false);
		if (httpSession == null) {
			throw new ChatException("not logged in: no http session");
		}

		// get the user name for the HTTP session.
		String username = (String) httpSession.getAttribute("username");
		if (username == null) {
			throw new ChatException("not logged in: no http session username");
		}

		// create the chat message
		ChatMessage chatMessage = new ChatMessage(username, receiver, message);

		for (Map.Entry<String, CometSession> entry : users.entrySet()) {

			if (!username.equals(entry.getKey()))
				entry.getValue().enqueue(chatMessage);
		}
	}

	@Override
	public void setStatus(StatusUpdate.Status status) throws ChatException {
		// check if there is a HTTP session setup.
		HttpSession httpSession = getThreadLocalRequest().getSession(false);
		if (httpSession == null) {
			throw new ChatException("not logged in: no http session");
		}

		// get the user name for the HTTP session.
		String username = (String) httpSession.getAttribute("username");
		if (username == null) {
			throw new ChatException("not logged in: no http session username");
		}

		// create the chat message
		StatusUpdate statusUpdate = new StatusUpdate();
		statusUpdate.setUsername(username);
		statusUpdate.setStatus(status);

		for (Map.Entry<String, CometSession> entry : users.entrySet()) {
			if (!username.equals(entry.getKey()))
				entry.getValue().enqueue(statusUpdate);
		}
	}
}
