package com.vdm.starlight.server.dao;

import org.hibernate.Query;
import org.hibernate.Session;

import com.vdm.starlight.shared.objects.Workbook;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * {@link WorkpageDao} manages all the {@link Workpage} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkpageDao extends AbstractDao {

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link Workpage} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link Workpage} object to create.
	 * @return the id field of the workpage assigned by the database.
	 */
	public int persist(String name, int workbookId, int pageNumber) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workbook wb = (Workbook) session.get(Workbook.class, workbookId);
		Workpage wp = new Workpage(name, wb, pageNumber);
		wb.addWorkPage(wp);
		wp.setWorkBook(wb);
		session.save(wp);
		session.update(wb);
		session.getTransaction().commit();
		return wp.getId();
	}

	public int create(Workpage workpage, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workbook workbook = (Workbook) session.get(Workbook.class, workbookId);

		workpage.setWorkBook(workbook);
		session.save(workpage);

		workbook.addWorkPage(workpage);
		session.update(workbook);

		session.getTransaction().commit();
		return workpage.getId();
	}

	// // relationships
	// public void addComponent(Long wp_id, Long c_id) {
	// Session session = getSessionFactory().getCurrentSession();
	// session.beginTransaction();
	//
	// Component c = (Component) session.get(Component.class, c_id);
	// Workpage wp = (Workpage) session.get(Workpage.class, wp_id);
	// wp.addComponent(c);
	// c.setWorkpage(wp);
	//
	// session.update(wp);
	// session.update(c);
	//
	// session.getTransaction().commit();
	// }

	/* ******************************** Update methods ********************************* */
	public void updateName(Long w_id, String name) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Workbook w = (Workbook) session.get(Workbook.class, w_id);
		w.setName(name);
		session.update(w);

		session.getTransaction().commit();
	}

	public void updateDescription(int workpageID, String description, int uniqueID) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Workpage w = (Workpage) session.get(Workpage.class, workpageID);
		w.setComponentDataString(description);
		w.setUniqueComponentId(uniqueID);
		session.update(w);

		session.getTransaction().commit();
	}

	/* ******************************** Get methods ********************************* */

	public Workpage getWorkpage(int workpageId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workpage e = (Workpage) session.get(Workpage.class, workpageId);

		session.getTransaction().commit();
		return e;
	}

	public Workpage getWorkpage(int workbookId, int pagenumber) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM Workpage w WHERE w.workbook.id='" + workbookId + "' AND w.pageNumber='" + pagenumber + "'";
		Query result = session.createQuery(query);
		Workpage w = (Workpage) result.uniqueResult();
		session.getTransaction().commit();
		return w;
	}

	public int getNumPages(int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "SELECT count(*) FROM Workpage w WHERE w.workbook.id='" + workbookId + "'";
		Query result = session.createQuery(query);
		Integer w = ((Long) result.uniqueResult()).intValue();
		session.getTransaction().commit();
		return w;
	}
}
