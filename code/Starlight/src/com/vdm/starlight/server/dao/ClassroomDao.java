package com.vdm.starlight.server.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.ClassroomWorkbook;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentClassroom;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * {@link ClassroomDao} manages all the {@link Classroom} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ClassroomDao {
	private static String QUERY_ID = "FROM Classroom c WHERE c.id = :id";
	private static String QUERY_WORKBOOKS = "FROM Workbook w LEFT JOIN fetch w.image WHERE w.creator.id = :id";
	private static String QUERY_STUDENTS = "SELECT DISTINCT s.student FROM StudentClassroom s WHERE s.classroom.creator.id = :id";

	public void addEndDate(int classroomId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Classroom c = (Classroom) session.get(Classroom.class, classroomId);
		c.setCompleted(new Date());
		session.update(c);
		session.getTransaction().commit();
	}

	public void addWorkbook(int classroomId, int workbookId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Classroom c = (Classroom) session.get(Classroom.class, classroomId);
		Workbook w = (Workbook) session.get(Workbook.class, workbookId);

		ClassroomWorkbook sw = new ClassroomWorkbook(c, w);

		session.save(sw);

		session.getTransaction().commit();
	}

	/* ******************************** get methods ********************************* */

	public Classroom getClassroom(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_ID);
		result.setParameter("id", id);
		Classroom e = (Classroom) result.uniqueResult();
		session.getTransaction().commit();
		return e;
	}

	public List<StudentClassroom> getStudentClassrooms(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = " FROM Classroom c LEFT JOIN fetch c.studentClassrooms WHERE c.id='" + id + "'";
		Query result = session.createQuery(query);
		Classroom e = (Classroom) result.uniqueResult();
		session.getTransaction().commit();
		return new ArrayList<StudentClassroom>(e.getStudentClassrooms());
	}

	public List<Student> getStudents(int classroomId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = " select distinct s.student FROM StudentClassroom s WHERE s.classroom.id='" + classroomId + "'";
		Query result = session.createQuery(query);
		List<Student> students = (List<Student>) result.list();
		session.getTransaction().commit();
		return students;
	}

	public List<Workbook> getWorkbooks(int classroomId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = " select distinct s.workbook FROM ClassroomWorkbook s WHERE s.classroom.id='" + classroomId
				+ "'";
		Query result = session.createQuery(query);
		List<Workbook> workbooks = (List<Workbook>) result.list();
		session.getTransaction().commit();
		return workbooks;
	}

	/* ******************************** persist ********************************* */

	/**
	 * Creates a new {@link Classroom} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link Classroom} object to create.
	 * @return the id field of the classroom assigned by the database.
	 */
	public int persist(String name, int creatorId, String ageGroup, String description) {
		int ret = -1; // if workbook could not be persisted

		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Educator e = (Educator) session.get(Educator.class, creatorId);
		if (e != null) {
			Classroom w = new Classroom(name, e, ageGroup, description);
			e.addClassroom(w);
			session.save(w);
			session.update(e);
			ret = w.getId();
		}
		session.getTransaction().commit();
		return ret;

	}

	/* ******************************** update ********************************* */

	public void updateName(int classroomId, String name) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Classroom c = (Classroom) session.get(Classroom.class, classroomId);
		c.setName(name);
		session.update(c);
		session.getTransaction().commit();
	}

	public int updateClassroom(Classroom c) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.update(c);
		session.getTransaction().commit();
		return c.getId();
	}

}
