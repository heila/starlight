package com.vdm.starlight.server.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.ClassroomWorkbook;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentClassroom;
import com.vdm.starlight.shared.objects.StudentWorkbook;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * {@link StudentDao} manages all the {@link Student} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentDao extends AbstractDao {

	private static String QUERY_USERNAME = "FROM Student s WHERE LCASE (s.userName) = :username";
	private static String QUERY_ID = "FROM Student s WHERE s.id = :id";
	private static String QUERY_CLASSROOMS = "FROM StudentClassroom sc left join fetch sc.classroom c left join fetch c.image WHERE sc.student.id = :id";
	private static String QUERY_WORKBOOKS = "FROM StudentWorkbook  sc left join fetch  sc.workbook c left join fetch c.image WHERE sc.student.id = :id";
	private static String QUERY_WORKBOOKS_CLASSROOM = "SELECT cw.workbook FROM Workbook w, ClassroomWorkbook cw, StudentWorkbook sw WHERE cw.classroom.id = :classId AND sw.student.id = :id AND sw.workbook.id = w.id AND cw.workbook.id = w.id";

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link Student} object in the database and returns its new database-generated id. If the username
	 * is already taken the object is not persisted and -1 is returned.
	 * 
	 * @param student
	 *            The {@link Student} object to create.
	 * @return the id field of the educator assigned by the database.
	 */
	public int persist(Student student) {
		int result = -1;

		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Query query = session.createQuery(QUERY_USERNAME);
		query.setParameter("username", student.getUserName().toLowerCase().trim());
		Student s = (Student) query.uniqueResult();

		if (s == null) { // this username is unique
			session.save(student);
			result = student.getId();
		}

		session.getTransaction().commit();
		return result;
	}

	/* ******************************** Get methods ********************************* */

	/**
	 * Returns a {@link List} of all the {@link Student} objects with the given username.
	 * 
	 * @param username
	 *            The username to look-up in the db.
	 * @return the list of students with the given username.
	 */
	public Student getStudent(String username) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_USERNAME);
		result.setParameter("username", username);
		Student s = (Student) result.uniqueResult();
		session.getTransaction().commit();
		return s;

	}

	/**
	 * Looks-up and returns the student with a given id from the database.
	 * 
	 * @param id
	 *            The id of the student.
	 * @return the Student with the given id.
	 */
	public Student getStudent(int id) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_ID);
		result.setParameter("id", id);
		Student student = (Student) result.uniqueResult();
		session.getTransaction().commit();
		return student;

	}

	/* ******************************** Update methods ********************************* */

	public void updateName(int studentId, String name) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student s = (Student) session.get(Student.class, studentId);
		s.setName(name);
		session.update(s);
		session.getTransaction().commit();
	}

	public void updatePassword(int studentId, String password) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student s = (Student) session.get(Student.class, studentId);
		s.setPassword(password);
		session.update(s);
		session.getTransaction().commit();
	}

	/* ******************************** Add methods ********************************* */
	/**
	 * Assigns the specified {@link Workbook} to all students in the given {@link Classroom}.
	 * 
	 * @param classroomId
	 *            The {@link Classroom} id.
	 * @param workbookId
	 *            The {@link Workbook} id.
	 */
	public void addWorkbookToStudents(int classroomId, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		String query = "SELECT c.students FROM Classroom c WHERE c.id='" + classroomId + "'";
		Query result = session.createQuery(query);
		List<Student> students = (List<Student>) result.list();

		Workbook w = (Workbook) session.get(Workbook.class, workbookId);
		StudentWorkbook sw;

		if (students != null) {
			for (Student s : students) {
				sw = new StudentWorkbook(s, w);
				session.save(sw);
			}
		}
		session.getTransaction().commit();

	}

	/**
	 * Adds the specified {@link Student} to the {@link Classroom}.
	 * 
	 * @param studentId
	 *            The id of the Student
	 * @param classroomId
	 *            The id of the Classroom
	 */
	public void addClassroom(int studentId, int classroomId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Student s = (Student) session.get(Student.class, studentId);
		Classroom c = (Classroom) session.get(Classroom.class, classroomId);
		StudentClassroom sc = new StudentClassroom(s, c);

		session.save(sc);
		session.getTransaction().commit();
	}

	/**
	 * Adds a {@link Workbook} to the specified {@link Student}.
	 * 
	 * @param studentId
	 *            The id of the student.
	 * @param workbookId
	 *            The id of the workbook.
	 */
	public void addWorkbook(int studentId, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Student s = (Student) session.get(Student.class, studentId);
		Workbook w = (Workbook) session.get(Workbook.class, workbookId);
		StudentWorkbook sw = new StudentWorkbook(s, w);

		session.save(sw);

		session.getTransaction().commit();
	}

	public List<Classroom> getClassrooms(int studentId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_CLASSROOMS);
		result.setParameter("id", studentId);
		List<StudentClassroom> scList = (List<StudentClassroom>) result.list();
		session.getTransaction().commit();
		List<Classroom> classrooms = new ArrayList<Classroom>();
		for (StudentClassroom studentClassroom : scList) {
			classrooms.add(studentClassroom.getClassroom());
		}
		return classrooms;
	}

	public List<Workbook> getWorkbooks(int studentId, int classId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_WORKBOOKS_CLASSROOM);
		result.setParameter("id", studentId);
		result.setParameter("classId", classId);
		List<Workbook> scList = (List<Workbook>) result.list();
		session.getTransaction().commit();
		return scList;
	}

	public List<Workbook> getWorkbooks(int studentId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_WORKBOOKS);
		result.setParameter("id", studentId);
		List<ClassroomWorkbook> scList = (List<ClassroomWorkbook>) result.list();
		session.getTransaction().commit();
		List<Workbook> workbooks = new ArrayList<Workbook>();
		for (ClassroomWorkbook classroomWorkbook : scList) {
			workbooks.add(classroomWorkbook.getWorkbook());
		}
		return workbooks;
	}

}
