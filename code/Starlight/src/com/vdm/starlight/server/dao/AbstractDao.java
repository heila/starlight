package com.vdm.starlight.server.dao;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.ClassroomWorkbook;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Resource;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentClassroom;
import com.vdm.starlight.shared.objects.StudentWorkbook;
import com.vdm.starlight.shared.objects.StudentWorkpage;
import com.vdm.starlight.shared.objects.Workbook;
import com.vdm.starlight.shared.objects.Workpage;

public class AbstractDao {
	private static final SessionFactory sessionFactory;

	static {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			sessionFactory = new AnnotationConfiguration().addAnnotatedClass(Educator.class)
					.addAnnotatedClass(Classroom.class).addAnnotatedClass(Workbook.class)
					.addAnnotatedClass(Student.class).addAnnotatedClass(StudentWorkbook.class)
					.addAnnotatedClass(StudentClassroom.class).addAnnotatedClass(Workpage.class)
					.addAnnotatedClass(ClassroomWorkbook.class).addAnnotatedClass(StudentWorkpage.class)
					.addAnnotatedClass(Resource.class).configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
