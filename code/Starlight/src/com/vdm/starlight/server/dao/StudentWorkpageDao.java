package com.vdm.starlight.server.dao;

import org.hibernate.Query;
import org.hibernate.classic.Session;

import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentWorkbook;
import com.vdm.starlight.shared.objects.StudentWorkpage;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * {@link StudentWorkpageDao} manages all the {@link StudentWorkbook} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentWorkpageDao extends AbstractDao {

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link StudentWorkbook} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link StudentWorkbook} object to create.
	 */
	public void persist(int studentId, int workpageId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student s = (Student) session.get(Student.class, studentId);
		Workpage w = (Workpage) session.get(Workpage.class, workpageId);
		StudentWorkpage sw = new StudentWorkpage(s, w);
		session.save(sw);
		session.getTransaction().commit();

	}

	public StudentWorkpage getStudentWorkpage(int workpageId, int studentId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM StudentWorkpage sw WHERE sw.student.id='" + studentId + "' AND sw.workpage.id='"
				+ workpageId + "'";
		Query result = session.createQuery(query);
		StudentWorkpage sw = (StudentWorkpage) result.uniqueResult();

		if (sw == null) {
			Student s = (Student) session.get(Student.class, studentId);
			Workpage w = (Workpage) session.get(Workpage.class, workpageId);
			sw = new StudentWorkpage(s, w);
			session.save(sw);
		}

		session.getTransaction().commit();
		return sw;

	}

	// public WorkpageData getData(int studentId, int workpageId) {
	// Session session = getSessionFactory().getCurrentSession();
	// session.beginTransaction();
	// String query = "FROM StudentWorkpage sw WHERE sw.student.id='" + studentId + "' AND sw.workpage.id='"
	// + workpageId + "'";
	// Query result = session.createQuery(query);
	// StudentWorkpage s = (StudentWorkpage) result.uniqueResult();
	// session.getTransaction().commit();
	// if (s != null) {
	// WorkpageData w = new WorkpageData(s.getComment(), s.getData(), studentId, workpageId);
	// }
	// // WorkpageData w = new WorkpageData("", "", studentId, workpageId);
	// // return w;
	// return null;
	// }

	public void saveData(int studentId, int workpageId, String data) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM StudentWorkpage sw WHERE sw.student.id='" + studentId + "' AND sw.workpage.id='"
				+ workpageId + "'";
		Query result = session.createQuery(query);
		StudentWorkpage s = (StudentWorkpage) result.uniqueResult();
		s.setData(data);
		session.update(s);
		session.getTransaction().commit();
	}

	public void saveComment(int studentId, int workpageId, String comment) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM StudentWorkpage sw WHERE sw.student.id='" + studentId + "' AND sw.workpage.id='"
				+ workpageId + "'";
		Query result = session.createQuery(query);
		StudentWorkpage sw = (StudentWorkpage) result.uniqueResult();
		if (sw == null) {
			Student s = (Student) session.get(Student.class, studentId);
			Workpage w = (Workpage) session.get(Workpage.class, workpageId);
			sw = new StudentWorkpage(s, w);
			sw.setComment(comment);
			session.save(sw);
		} else {
			sw.setComment(comment);
			session.update(sw);
		}
		session.getTransaction().commit();

	}

}
