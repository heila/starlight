package com.vdm.starlight.server.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Resource;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * {@link ResourceDao} manages all the {@link Resource} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ResourceDao extends AbstractDao {

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link Resource} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link Resource} object to create.
	 * @return the id field of Resource assigned by the database.
	 */
	public int persist(Resource resource) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(resource);
		session.getTransaction().commit();
		return resource.getId();

	}

	/* ******************************** update methods ********************************* */
	public void update(Resource resource) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.update(resource);
		session.getTransaction().commit();
	}

	/* ********************************** add methods ********************************* */
	public void addStudentImage(int resourceId, int studentId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Student s = (Student) session.get(Student.class, studentId);
		Resource r = (Resource) session.get(Resource.class, resourceId);
		s.setImage(r);
		session.update(r);
		session.update(s);
		session.getTransaction().commit();

	}

	public void addEducatorImage(int resourceId, int educatorId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Educator s = (Educator) session.get(Educator.class, educatorId);
		Resource r = (Resource) session.get(Resource.class, resourceId);
		s.setImage(r);
		session.update(r);
		session.update(s);
		session.getTransaction().commit();
	}

	public void addWorkbookImage(int resourceId, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Workbook s = (Workbook) session.get(Workbook.class, workbookId);
		Resource r = (Resource) session.get(Resource.class, resourceId);
		s.setImage(r);
		session.update(r);
		session.update(s);
		session.getTransaction().commit();
	}

	public void addClassroomImage(int resourceId, int classroomId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Classroom s = (Classroom) session.get(Classroom.class, classroomId);
		Resource r = (Resource) session.get(Resource.class, resourceId);
		s.setImage(r);
		session.update(r);
		session.update(s);
		session.getTransaction().commit();
	}

	/* ********************************** get methods ********************************* */
	public List<Resource> getAllResources() {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Resource> images = new ArrayList<Resource>(session.createQuery("from Resource r order by r.id desc")
				.list());
		session.getTransaction().commit();
		return images;
	}

	public Resource getResource(int id) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Resource r = (Resource) session.createQuery("from Resource r WHERE r.id='" + id + "'").uniqueResult();
		session.getTransaction().commit();
		return r;
	}

	/* ********************************** delete methods ********************************* */
	public void deleteResource(Resource res) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(res);
		session.getTransaction().commit();
	}
}
