package com.vdm.starlight.server.dao;

import org.hibernate.Session;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentClassroom;

/**
 * {@link StudentClassDao} manages all the {@link StudentClassroom} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentClassDao extends AbstractDao {

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link StudentClassroom} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link StudentClassroom} object to create.
	 */
	public void persist(int studentId, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student s = (Student) session.get(Student.class, studentId);
		Classroom c = (Classroom) session.get(Classroom.class, workbookId);
		StudentClassroom sc = new StudentClassroom(s, c);
		session.save(sc);
		session.getTransaction().commit();
	}
}