package com.vdm.starlight.server.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.Workbook;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * {@link WorkBookDao} manages all the {@link WorkBook} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkBookDao extends AbstractDao {

	/**
	 * Creates a new Workbook object
	 * 
	 * @param w
	 * @param educatorId
	 * @return
	 */
	public int create(Workbook w, int educatorId) {

		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Educator e = (Educator) session.get(Educator.class, educatorId);
		Workbook workbook = new Workbook(w.getName(), w.getDescription(), e);
		session.save(workbook);

		e.addWorkbook(workbook);

		session.update(e);

		session.getTransaction().commit();
		return w.getId();

	}

	/* ******************************** get methods ********************************* */

	public List<Workpage> getPages(int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM Workpage w WHERE w.workbook.id='" + workbookId + "' order by w.id";
		Query result = session.createQuery(query);
		List<Workpage> e = (List<Workpage>) result.list();
		session.getTransaction().commit();
		return e;
	}

	public List<Student> getStudents(int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "select distinct s.student FROM StudentWorkbook s WHERE s.workbook.id='" + workbookId + "'";
		Query result = session.createQuery(query);
		List<Student> students = (List<Student>) result.list();
		session.getTransaction().commit();
		return students;
	}

	public List<Classroom> getClassrooms(int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "select distinct s.classroom FROM ClassroomWorkbook s WHERE s.workbook.id='" + workbookId + "'";
		Query result = session.createQuery(query);
		List<Classroom> classrooms = (List<Classroom>) result.list();
		session.getTransaction().commit();
		return classrooms;
	}

	public Workbook getWorkbook(int workbookid) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM Workbook w LEFT JOIN fetch w.image LEFT JOIN fetch w.creator WHERE w.id='" + workbookid
				+ "'";
		Query result = session.createQuery(query);
		Workbook w = (Workbook) result.uniqueResult();
		session.getTransaction().commit();
		return w;
	}

	public List<Integer> getWorkbookStats(int workbookId) {
		List<Integer> stats = new ArrayList<Integer>();
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		// number of pages
		String query = "SELECT count(*) FROM Workpage w WHERE w.workbook.id='" + workbookId + "'";
		Query result = session.createQuery(query);
		stats.add(((Long) result.uniqueResult()).intValue());
		// number of students
		query = "SELECT count(*) FROM StudentWorkbook s WHERE s.workbook.id='" + workbookId + "'";
		result = session.createQuery(query);
		stats.add(((Long) result.uniqueResult()).intValue());
		// number of students completed
		query = "SELECT count(*) FROM StudentWorkbook s WHERE s.workbook.id='" + workbookId
				+ "' AND s.dateCompleted is NOT NULL";
		result = session.createQuery(query);
		stats.add(((Long) result.uniqueResult()).intValue());
		// number of classrooms
		query = "SELECT count(*) FROM ClassroomWorkbook c WHERE c.workbook.id='" + workbookId + "'";
		result = session.createQuery(query);
		stats.add(((Long) result.uniqueResult()).intValue());
		// average time
		// query = "SELECT dateStarted, dateCompleted FROM StudentWorkbook s WHERE s.workbook.id='" + workbookId
		// + "' AND s.dateCompleted is NOT NULL";
		// result = session.createQuery(query);
		// List<Object[]> dates = (List<Object[]>) result.list();
		// Period sum = new Period();
		// session.getTransaction().commit();
		// Period p;
		// long ave = 0;
		// for (Object[] date : dates) {
		// DateTime startDate = new DateTime(date[0]);
		// DateTime endDate = new DateTime(date[1]);
		// p = new Period(startDate, endDate);
		// p = p.getMillis()/
		// sum = sum.plus(p);
		// }
		// Seconds s = Seconds.standardSecondsIn(sum.normalizedStandard());
		// s = s.dividedBy(dates.size());
		// sum = new Period(s);
		return stats;
	}

	public List<WorkpageDTO> getWorkpages(int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		String query = "FROM Workpage w WHERE w.workbook.id='" + workbookId + "' order by w.id";
		Query result = session.createQuery(query);
		List<Workpage> e = (List<Workpage>) result.list();
		session.getTransaction().commit();
		List<WorkpageDTO> pages = new ArrayList<WorkpageDTO>();
		for (Workpage w : e) {
			pages.add(new WorkpageDTO(w.getId(), w.getName(), w.getUniqueComponentId(), w.getPageNumber()));
		}

		return pages;
	}

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link WorkBook} object in the database and returns its new database-generated id.
	 * 
	 * @param name
	 *            The display name of the {@link Workbook}
	 * @param description
	 *            The description of the {@link Workbook}
	 * @param educatorId
	 *            The integer ID of the {@link Educator} that created the {@link Workbook}.
	 * @returnn The newly generated integer ID of the peristed {@link Workbook} or -1 if the Workbook could not be
	 *          persisted
	 */
	public int persist(String name, String description, int educatorId) {
		int ret = -1; // if workbook could not be persisted

		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Educator e = (Educator) session.get(Educator.class, educatorId);
		if (e != null) {
			Workbook w = new Workbook(name, description, e);
			e.addWorkbook(w);
			session.save(w);
			session.update(e);
			ret = w.getId();
		}
		session.getTransaction().commit();
		return ret;
	}

	/* ******************************** update methods ********************************* */

	public void updateDescription(int w_id, String description) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workbook w = (Workbook) session.get(Workbook.class, w_id);
		w.setDescription(description);
		session.update(w);
		session.getTransaction().commit();
	}

	public void updateImage(int w_id, String description) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workbook w = (Workbook) session.get(Workbook.class, w_id);
		w.setDescription(description);
		session.update(w);
		session.getTransaction().commit();
	}

	public void updateName(int w_id, String name) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Workbook w = (Workbook) session.get(Workbook.class, w_id);
		w.setName(name);
		session.update(w);
		session.getTransaction().commit();
	}

	public int updateWorkbook(Workbook w) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.update(w);
		session.getTransaction().commit();
		return w.getId();
	}

}
