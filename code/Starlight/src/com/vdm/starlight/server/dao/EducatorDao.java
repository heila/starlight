package com.vdm.starlight.server.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.vdm.starlight.shared.objects.Classroom;
import com.vdm.starlight.shared.objects.Educator;
import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * {@link EducatorDao} manages all the {@link Educator} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class EducatorDao {
	private static String QUERY_USERNAME = "FROM Educator e WHERE LCASE (e.userName) = :username";
	private static String QUERY_ID = "FROM Educator e WHERE e.id = :id";
	private static String QUERY_CLASSROOMS = "FROM Classroom w LEFT JOIN fetch w.image WHERE w.creator.id = :id";
	private static String QUERY_WORKBOOKS = "FROM Workbook w LEFT JOIN fetch w.image WHERE w.creator.id = :id";
	private static String QUERY_STUDENTS = "SELECT DISTINCT s.student FROM StudentClassroom s WHERE s.classroom.creator.id = :id";
	private static String QUERY_WORKBOOKS_CLASSROOM = "SELECT DISTINCT(c) FROM Classroom c LEFT JOIN FETCH c.workbooks w  WHERE c.creator.id = :educatorId";

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link Educator} object in the database and returns its new database-generated id. If the username
	 * is already taken the object is not persisted and -1 is returned.
	 * 
	 * @param student
	 *            The {@link Educator} object to create.
	 * @return the id field of the educator assigned by the database.
	 */
	public int persist(Educator educator) {
		int result = -1;

		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Query query = session.createQuery(QUERY_USERNAME);
		query.setParameter("username", educator.getUserName().toLowerCase().trim());
		Educator e = (Educator) query.uniqueResult();

		if (e == null) { // this username is unique
			session.save(educator);
			result = educator.getId();
		}

		session.getTransaction().commit();
		return result;
	}

	/* ******************************** add methods ********************************* */
	public void addClassroom(int educatorId, int classroomId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Educator e = (Educator) session.get(Educator.class, educatorId);
		Classroom c = (Classroom) session.get(Classroom.class, classroomId);

		c.setCreator(e);
		e.addClassroom(c);
		session.update(c);
		session.update(e);
		session.getTransaction().commit();
	}

	public void addWorkbook(int educatorId, int workbookId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Educator e = (Educator) session.get(Educator.class, educatorId);
		Workbook w = (Workbook) session.get(Workbook.class, workbookId);

		w.setCreator(e);
		e.addWorkbook(w);
		session.update(w);
		session.update(e);
		session.getTransaction().commit();
	}

	/* ******************************** update methods ********************************* */
	public void update(Educator educator) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		session.update(educator);

		session.getTransaction().commit();
	}

	/* ******************************** get methods ********************************* */

	/**
	 * Returns the {@link List} of {@link Workbook} objects that the {@link Educator} created.
	 * 
	 * @param id
	 *            the id of the {@link Educator}
	 * @return the {@link List} of {@link Workbook} objects of the educator or null
	 */
	public List<Classroom> getClassrooms(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_CLASSROOMS);
		result.setParameter("id", id);
		List<Classroom> classes = (List<Classroom>) result.list();
		session.getTransaction().commit();
		return classes;
	}

	/**
	 * Returns the {@link List} of {@link Workbook} objects that the {@link Educator} created.
	 * 
	 * @param id
	 *            the id of the {@link Educator}
	 * @return the {@link List} of {@link Workbook} objects of the educator or null
	 */
	public List<Classroom> getAvailableClassrooms(int educatorId) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_WORKBOOKS_CLASSROOM);
		result.setParameter("educatorId", educatorId);

		List<Classroom> classes = (List<Classroom>) result.list();
		session.getTransaction().commit();
		return classes;
	}

	/**
	 * Returns the {@link List} of {@link Classroom} objects for which the {@link Educator} is responsible.
	 * 
	 * @param id
	 *            the id of the {@link Educator}
	 * @return the {@link List} of {@link Classroom} objects of the educator or null
	 */
	public List<Workbook> getWorkbooks(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_WORKBOOKS);
		result.setParameter("id", id);
		List<Workbook> workbooks = (List<Workbook>) result.list();
		session.getTransaction().commit();
		return workbooks;
	}

	public List<Student> getStudents(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_STUDENTS);
		result.setParameter("id", id);
		List<Student> students = (List<Student>) result.list();
		session.getTransaction().commit();
		return students;
	}

	/**
	 * Returns the {@link Educator} with a specific user name if it exists otherwise returns null.
	 * 
	 * @param username
	 *            the username of the {@link Educator}
	 * @return an {@link Educator} object with the specified username of null
	 */
	public Educator getEducator(String username) {
		Educator e = null;
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();

		Query result = session.createQuery(QUERY_USERNAME);
		result.setParameter("username", username.toLowerCase().trim());

		e = (Educator) result.uniqueResult();
		session.getTransaction().commit();

		return e;

	}

	/**
	 * Returns the {@link Educator} with a specific id if it exists otherwise returns null.
	 * 
	 * @param id
	 *            the id of the {@link Educator}
	 * @return an {@link Educator} object with the specified id of null
	 */
	public Educator getEducator(int id) {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Query result = session.createQuery(QUERY_ID);
		result.setParameter("id", id);
		Educator e = (Educator) result.uniqueResult();

		session.getTransaction().commit();
		return e;

	}

	/**
	 * Returns a list of all {@link Educator} objects otherwise returns null..
	 * 
	 * @return list of all {@link Educator}
	 */
	public List<Educator> getEducators() {
		Session session = AbstractDao.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		@SuppressWarnings("unchecked")
		List<Educator> educators = new ArrayList<Educator>(session.createQuery("from Educator").list());
		session.getTransaction().commit();
		return educators;
	}

}
