package com.vdm.starlight.server.dao;

import org.hibernate.classic.Session;

import com.vdm.starlight.shared.objects.Student;
import com.vdm.starlight.shared.objects.StudentWorkbook;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * {@link StudentWorkbookDao} manages all the {@link StudentWorkbook} transactions between the server and the database.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentWorkbookDao extends AbstractDao {

	/* ******************************** Create methods ********************************* */
	/**
	 * Creates a new {@link StudentWorkbook} object in the database and returns its new database-generated id.
	 * 
	 * @param student
	 *            The {@link StudentWorkbook} object to create.
	 */
	public void persist(int studentId, int workbookId) {
		Session session = getSessionFactory().getCurrentSession();
		session.beginTransaction();
		Student s = (Student) session.get(Student.class, studentId);
		Workbook w = (Workbook) session.get(Workbook.class, workbookId);
		StudentWorkbook sw = new StudentWorkbook(s, w);
		session.save(sw);
		session.getTransaction().commit();

	}

}
