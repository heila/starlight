package com.vdm.starlight.server;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;

import com.mortennobel.imagescaling.ResampleOp;

/**
 * Library used to process the images.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ImageProcessing {
	/**
	 * The longest length/width of the image will be scaled to this size for the thumbnail view of the image.
	 */
	public final static int THUMBNAIL_SIZE = 100;

	/**
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	private static byte[] loadFile(File file) throws IOException {
		InputStream in = new FileInputStream(file);
		try {
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();

			byte[] barr = new byte[1024];
			while (true) {
				int r = in.read(barr);
				if (r <= 0) {
					break;
				}
				buffer.write(barr, 0, r);
			}

			return buffer.toByteArray();
		} finally {
			in.close();
		}
	}

	/**
	 * 
	 * @return
	 */
	public static String getImage() {

		String hostname = "";
		try {
			InetAddress addr = InetAddress.getLocalHost();
			// Get hostname
			hostname = addr.getHostName();
		} catch (UnknownHostException e) {
		}
		File imageFile = null;
		if (hostname.equals("callisto")) {
			imageFile = new File(
					"/home/archlight/workspace-heila/Starlight/src/com/vdm/starlight/client/resources/ster.png");
		} else {
			imageFile = new File("/home/heila/Projek/workspace/Starlight/ster.png");
		}
		byte[] bytes;
		try {
			bytes = loadFile(imageFile);
		} catch (IOException e1) {
			return "";
		}

		// all chars in encoded are guaranteed to be 7-bit ASCII
		byte[] encoded = Base64.encodeBase64(bytes);

		String result = null;
		try {
			result = new String(encoded, "ASCII");
		} catch (UnsupportedEncodingException e) {
		}
		return result;
	}

	/**
	 * This method takes a file path as parameter and returns the encoded String representing the image.
	 * 
	 * @param path
	 *            the fully qualified path of the image on the server's filesystem.
	 * @return
	 * @throws Exception
	 */
	public static String getImageFromPath(String path) throws Exception {
		File imageFile = new File(path);
		byte[] bytes = loadFile(imageFile);
		byte[] encoded = Base64.encodeBase64(bytes);
		String result = null;
		try {
			result = new String(encoded, "ASCII");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * This method takes a file path as parameter and returns the encoded String representing a thumbnail size version
	 * of the image.
	 * 
	 * @param path
	 * @return
	 * @throws Exception
	 */
	public static String createThumbnailFromPath(String path) {
		return getResizedImageFromPath(path, THUMBNAIL_SIZE, THUMBNAIL_SIZE);
	}

	/**
	 * 
	 * @param path
	 * @param maxWidth
	 * @param maxHeight
	 * @return
	 */
	public static String getResizedImageFromPath(String path, int maxWidth, int maxHeight) {
		return getResizedImageFromPath(path, maxWidth, maxHeight, false);
	}

	/**
	 * 
	 * @param path
	 * @param maxWidth
	 * @param maxHeight
	 * @param saveToDisk
	 * @return
	 */
	public static String getResizedImageFromPath(String path, int maxWidth, int maxHeight, boolean saveToDisk) {
		BufferedImage buffImage = null;
		String extension = path.substring(path.lastIndexOf(".") + 1);
		String pathStart = path.substring(0, path.lastIndexOf("."));
		int width = 0;
		int height = 0;
		try {
			buffImage = ImageIO.read(new File(path));
			width = buffImage.getWidth();
			height = buffImage.getHeight();
		} catch (IOException e1) {
			return null;
		}
		boolean scaleIt = false;
		if (width > maxWidth) {
			scaleIt = true;
			float scale = ((float) width) / maxWidth;
			width = maxWidth;
			height = Math.round(height / scale);
		}
		if (height > maxHeight) {
			scaleIt = true;
			float scale = ((float) height) / maxHeight;
			height = maxHeight;
			width = Math.round(width / scale);
		}
		byte[] imageInBytes = null;
		if (scaleIt) {
			ResampleOp resampleOp = new ResampleOp(width, height);
			buffImage = resampleOp.filter(buffImage, null);
		}
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			if (extension.equalsIgnoreCase("bmp")) {
				extension = "png";
			}
			ImageIO.write(buffImage, extension, baos);
			baos.flush();
			imageInBytes = baos.toByteArray();
			baos.close();
		} catch (IOException e) {
			return null;
		}
		String result = null;
		if (saveToDisk) {
			try {
				String uuid = UUID.randomUUID().toString();
				path = pathStart + uuid + "." + extension;
				FileOutputStream fos = new FileOutputStream(new File(path));
				fos.write(baos.toByteArray());
				fos.flush();
				fos.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return path;
		} else {
			byte[] encoded = Base64.encodeBase64(imageInBytes);
			try {
				result = new String(encoded, "ASCII");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
}
