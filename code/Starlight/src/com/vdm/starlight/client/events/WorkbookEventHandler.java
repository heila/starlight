package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface WorkbookEventHandler extends EventHandler {
	void workbookEvent(WorkbookEvent event);
}
