package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface StudentsEventHandler extends EventHandler {
	void studentsEvent(StudentsEvent event);
}
