package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface MediaEventHandler extends EventHandler {
	void mediaEvent(MediaEvent event);
}
