package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface NewWorkbookEventHandler extends EventHandler {
	void create(NewWorkbookEvent event);
}
