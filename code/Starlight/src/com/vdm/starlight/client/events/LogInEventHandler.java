package com.vdm.starlight.client.events;


import com.google.gwt.event.shared.EventHandler;

public interface LogInEventHandler extends EventHandler {
	void onLogIn(LogInEvent event);
}
