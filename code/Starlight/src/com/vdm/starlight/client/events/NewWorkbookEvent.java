package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class NewWorkbookEvent extends GwtEvent<NewWorkbookEventHandler> {
	public static Type<NewWorkbookEventHandler> TYPE = new Type<NewWorkbookEventHandler>();

	private final String name;
	private final String description;

	public NewWorkbookEvent(String name, String description) {
		this.name = name;
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<NewWorkbookEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(NewWorkbookEventHandler handler) {
		handler.create(this);
	}

}
