package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface MessageEventHandler extends EventHandler {
	void message(MessageEvent event);
}
