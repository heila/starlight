package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class StudentEvent extends GwtEvent<StudentEventHandler> {
	public static Type<StudentEventHandler> TYPE = new Type<StudentEventHandler>();

	private final StudentEventType type;
	private final int studentId;

	public StudentEvent(StudentEventType type, int studentId) {
		this.type = type;
		this.studentId = studentId;
	}

	public StudentEventType getType() {
		return type;
	}

	public int getStudentId() {
		return studentId;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<StudentEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(StudentEventHandler handler) {
		handler.studentEvent(this);
	}

	public static enum StudentEventType {
		INFO, EDITOR, VIEW, NEW, EDIT, STUDENTS, CLASSROOMS;

	}

}
