package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class MessageEvent extends GwtEvent<MessageEventHandler> {
	public static Type<MessageEventHandler> TYPE = new Type<MessageEventHandler>();

	private final String message;
	private final String username;
	private final MessageType type;

	public MessageEvent(String message, String receiver, MessageType type) {
		this.message = message;
		this.username = receiver;
		this.type = type;
	}

	public String getMessage() {
		return message;
	}

	public String getReceiver() {
		return username;
	}

	public MessageType getType() {
		return type;
	}

	@Override
	protected void dispatch(MessageEventHandler handler) {
		handler.message(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MessageEventHandler> getAssociatedType() {
		return TYPE;
	}

}
