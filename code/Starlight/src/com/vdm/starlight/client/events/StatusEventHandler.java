package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface StatusEventHandler extends EventHandler {

	public void getStatus(StatusEvent event);

}
