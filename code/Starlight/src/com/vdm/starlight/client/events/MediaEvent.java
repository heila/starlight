package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class MediaEvent extends GwtEvent<MediaEventHandler> {
	public static Type<MediaEventHandler> TYPE = new Type<MediaEventHandler>();

	private final MediaEventType type;

	public MediaEvent(MediaEventType type) {
		this.type = type;

	}

	public MediaEventType getType() {
		return type;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<MediaEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(MediaEventHandler handler) {
		handler.mediaEvent(this);
	}

	public static enum MediaEventType {
		GRID(1, "grid"), UPLOAD(2, "upload");

		int id;
		String desc;

		private MediaEventType(int id, String desc) {
			this.id = id;
			this.desc = desc;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}

}
