package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface StudentEventHandler extends EventHandler {
	void studentEvent(StudentEvent event);
}
