package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class LogInEvent extends GwtEvent<LogInEventHandler> {
	public static Type<LogInEventHandler> TYPE = new Type<LogInEventHandler>();

	private final String name;
	private final String password;
	private final UserType type;

	public LogInEvent(String name, String password, UserType type) {
		this.name = name;
		this.password = password;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<LogInEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(LogInEventHandler handler) {
		handler.onLogIn(this);
	}

	public UserType getType() {
		return this.type;
	}

	public enum UserType {
		STUDENT, EDUCATOR;
	}

}
