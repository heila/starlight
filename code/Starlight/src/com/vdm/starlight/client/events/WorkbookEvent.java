package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class WorkbookEvent extends GwtEvent<WorkbookEventHandler> {
	public static Type<WorkbookEventHandler> TYPE = new Type<WorkbookEventHandler>();

	private final WorkbookEventType type;
	private final int workbookId;

	public WorkbookEvent(WorkbookEventType type, int workbookId) {
		this.type = type;
		this.workbookId = workbookId;
	}

	public WorkbookEventType getType() {
		return type;
	}

	public int getWorkbookId() {
		return workbookId;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<WorkbookEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(WorkbookEventHandler handler) {
		handler.workbookEvent(this);
	}

	public static enum WorkbookEventType {
		INFO(1, "info"), EDITOR(2, "editor"), VIEW(3, "view"), NEW(4, "new"), EDIT(5, "edit"), STUDENTS(6, "student"), CLASSROOMS(
				7, "classrooms"), WORKBOOKS(8, "workbooks");

		int id;
		String desc;

		private WorkbookEventType(int id, String desc) {
			this.id = id;
			this.desc = desc;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

	}

}
