package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class ClassroomEvent extends GwtEvent<ClassroomEventHandler> {
	public static Type<ClassroomEventHandler> TYPE = new Type<ClassroomEventHandler>();

	private final ClassroomEventType type;
	private final int classroomId;

	public ClassroomEvent(ClassroomEventType type, int classroomId) {
		this.type = type;
		this.classroomId = classroomId;
	}

	public ClassroomEventType getType() {
		return type;
	}

	public int getClassroomId() {
		return classroomId;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ClassroomEventHandler> getAssociatedType() {
		return TYPE;
	}

	@Override
	protected void dispatch(ClassroomEventHandler handler) {
		handler.classroomEvent(this);
	}

	public static enum ClassroomEventType {
		INFO, EDITOR, VIEW, NEW, EDIT, STUDENTS, CLASSROOMS;

	}

}
