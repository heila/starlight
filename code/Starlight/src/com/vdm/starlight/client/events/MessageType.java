package com.vdm.starlight.client.events;

public enum MessageType {
	SEND, RECEIVE
}
