package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.EventHandler;

public interface ClassroomEventHandler extends EventHandler {
	void classroomEvent(ClassroomEvent event);
}
