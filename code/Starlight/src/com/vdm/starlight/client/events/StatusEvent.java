package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

public class StatusEvent extends GwtEvent<StatusEventHandler> {
	public static Type<StatusEventHandler> TYPE = new Type<StatusEventHandler>();

	private final Status status;
	private final String username;
	private final MessageType type;

	public StatusEvent(Status status, String username, MessageType type) {
		super();
		this.status = status;
		this.username = username;
		this.type = type;
	}

	public Status getStatus() {
		return status;
	}

	public String getUsername() {
		return username;
	}

	public MessageType getType() {
		return type;
	}

	@Override
	protected void dispatch(StatusEventHandler handler) {
		handler.getStatus(this);
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<StatusEventHandler> getAssociatedType() {
		return TYPE;
	}

}
