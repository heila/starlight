package com.vdm.starlight.client.events;

import com.google.gwt.event.shared.GwtEvent;

public class StudentsEvent extends GwtEvent<StudentsEventHandler> {
	public static Type<StudentsEventHandler> TYPE = new Type<StudentsEventHandler>();

	private final StudentEventType type;
	private final int studentId;
	private final int classId;
	private final int workbookId;
	private final String comment;
	private final String image;

	public StudentsEvent(StudentEventType type, int studentId, int classId, int workbookId, String comment, String image) {
		super();
		this.type = type;
		this.studentId = studentId;
		this.classId = classId;
		this.workbookId = workbookId;
		this.comment = comment;
		this.image = image;
	}

	public int getStudentId() {
		return studentId;
	}

	public int getClassId() {
		return classId;
	}

	public int getWorkbookId() {
		return workbookId;
	}

	public String getComment() {
		return comment;
	}

	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<StudentsEventHandler> getAssociatedType() {
		return TYPE;
	}

	public String getImage() {
		return image;
	}

	@Override
	protected void dispatch(StudentsEventHandler handler) {
		handler.studentsEvent(this);
	}

	public static enum StudentEventType {
		CLASSROOMS, WORKBOOKS, VIEW, MESSAGE;

	}

	public StudentEventType getType() {
		return type;
	}

}
