package com.vdm.starlight.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Starlight implements EntryPoint {
	private static final HandlerManager eventBus = new HandlerManager(null);
	private static final DatabaseServiceAsync studentManagerService = GWT.create(DatabaseService.class);
	private static final MessagingServiceAsync messagingService = GWT.create(MessagingService.class);
	static private AppController app;
	final VerticalPanel backPanel = new VerticalPanel();

	/**
	 * This is the entry point method.
	 */
	@Override
	public void onModuleLoad() {

		if (app == null) {
			app = new AppController(studentManagerService, messagingService, eventBus);

			backPanel.setStyleName("vp");
			backPanel.setWidth("100%");
			backPanel.setHeight(Window.getClientHeight() + "px");
			Window.addResizeHandler(new ResizeHandler() {

				public void onResize(ResizeEvent event) {
					int height = event.getHeight();
					backPanel.setHeight(height + "px");
				}

			});
			RootPanel.get().add(backPanel);

		}

		app.go(backPanel);

	}
}
