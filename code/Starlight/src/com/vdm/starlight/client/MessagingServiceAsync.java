package com.vdm.starlight.client;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.vdm.starlight.shared.dto.UserDTO;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

/**
 * The async counterpart of <code>MessagingServiceService</code>.
 */
public interface MessagingServiceAsync {

	void getUsername(AsyncCallback<UserDTO> callback);

	void logout(String username, AsyncCallback<Void> callback);

	void send(String message, String receiver, AsyncCallback<Void> callback);

	void setStatus(Status status, AsyncCallback<Void> callback);

	void isValidSession(int id, AsyncCallback<Boolean> callback);

	void authenticate(String username, String password, AsyncCallback<Integer> callback);

}