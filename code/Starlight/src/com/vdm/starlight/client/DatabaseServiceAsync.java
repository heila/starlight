package com.vdm.starlight.client;

import java.util.List;

import com.google.gwt.user.client.rpc.AsyncCallback;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.dto.ResourceDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.dto.WorkpageDataDTO;

/**
 * The async counterpart of <code>StudentManagerService</code>.
 */
public interface DatabaseServiceAsync {

	void addStudentToClassroom(int studentId, int classroomId, AsyncCallback<Void> callback);

	void addWorkbookToClassroom(int workbookId, int classroomId, AsyncCallback<Void> callback);

	void addWorkbookToClassroomStudents(int workbookId, int classroomId, AsyncCallback<Void> callback);

	void addWorkbookToStudent(int workbookId, int studentId, int classroomId, AsyncCallback<Void> callback);

	void createEducator(String userName, String password, String name, String surname, String email,
			AsyncCallback<Integer> callback);

	void createResource(String name, String type, String path, String imageData, AsyncCallback<Integer> callback);

	void createStudent(String userName, String password, String name, String surname, int age,
			AsyncCallback<Integer> callback);

	void createStudent(String userName, String password, String name, String surname, int age, int imageID,
			AsyncCallback<Integer> callback);

	void createWorkbook(String name, String description, String ageGroup, int creatorId, int imageID,
			AsyncCallback<Integer> callback);

	void createWorkbook(String name, String description, String ageGroup, int creatorId, AsyncCallback<Integer> callback);

	void deleteResource(int resourceID, AsyncCallback<Void> callback);

	void getClassroom(int classroomID, boolean fullImage, AsyncCallback<ClassroomDTO> callback);

	void getClassroomStudents(int classroomId, boolean fullImage, AsyncCallback<List<StudentDTO>> callback);

	void getDummyImage(AsyncCallback<String> callback);

	void getClassroomWorkbooks(int classId, boolean fullImage, AsyncCallback<List<WorkbookDTO>> callback);

	void getAllResources(int start, AsyncCallback<List<ResourceDTO>> callback);

	void getEducator(int id, AsyncCallback<EducatorDTO> callback);

	void getEducatorClassrooms(int educatorId, int start, int end, boolean fullImage,
			AsyncCallback<List<ClassroomDTO>> callback);

	void getEducatorWorkbooks(int educatorID, int start, int end, boolean fullImage,
			AsyncCallback<List<WorkbookDTO>> callback);

	void getNewWorkpage(String name, int workbookId, int pageNumber, AsyncCallback<WorkpageDTO> callback);

	void getFullImage(int id, AsyncCallback<String> callback);

	void getEducatorStudents(int educatorId, int start, int end, boolean fullImage,
			AsyncCallback<List<StudentDTO>> callback);

	void getWorkbook(int workbookId, boolean fullImage, AsyncCallback<WorkbookDTO> callback);

	void getStudent(int studentId, boolean fullImage, AsyncCallback<StudentDTO> callback);

	void getWorkpageComment(int studentId, int workpageId, AsyncCallback<String> callback);

	void getWorkpages(int workpageId, AsyncCallback<WorkbookPagesDTO> callback);

	void getNumberOfPages(int workbookId, AsyncCallback<Integer> callback);

	void getWorkpageStudentData(int studentId, int workpageId, AsyncCallback<List<String>> callback);

	void getWorkbookStudents(int workbookId, boolean fullImage, AsyncCallback<List<StudentDTO>> callback);

	void getWorkpageData(int workbookId, AsyncCallback<List<String>> callback);

	void getWorkpage(int workbookId, int pagenumber, AsyncCallback<WorkpageDataDTO> callback);

	void getWorkbooks(AsyncCallback<List<WorkbookDTO>> callback);

	void saveImage(String filename, AsyncCallback<Integer> callback);

	void createWorkpage(String name, int workbookId, int pageNumber, AsyncCallback<Integer> callback);

	void getWorkbookStat(int workbookId, AsyncCallback<WorkbookStatsDTO> callback);

	void setClassroomImage(int classroomId, int resourceId, AsyncCallback<Void> callback);

	void saveWorkpageDescription(int workpageID, String description, int uniqueID, AsyncCallback<Void> callback);

	void saveWorkbook(WorkbookDTO WorkbookDTO, int imageID, AsyncCallback<Void> callback);

	void setEducatorImage(int educatorId, int resourceId, AsyncCallback<Void> callback);

	void setStudentImage(int studentId, int resourceId, AsyncCallback<Void> callback);

	void setStudentWorkpageData(String data, int studentId, int workpageId, AsyncCallback<Void> callback);

	void setWorkbookImage(int workbookId, int resourceId, AsyncCallback<Void> callback);

	void setWorkpageComment(int studentId, int workpageId, String comment, AsyncCallback<Void> callback);

	void getWorkbookClassrooms(int workbookId, int start, int end, boolean b,
			AsyncCallback<List<ClassroomDTO>> asyncCallback);

	void getWorkbookStudents(int filter, int start, int end, boolean b, AsyncCallback<List<StudentDTO>> asyncCallback);

	void getStudentClassrooms(int studentId, boolean fullImage, AsyncCallback<List<ClassroomDTO>> callback);

	void getStudentClassrooms(int studentId, int start, int end, boolean fullImage,
			AsyncCallback<List<ClassroomDTO>> callback);

	void getStudentWorkbooks(int studentId, boolean fullImage, AsyncCallback<List<WorkbookDTO>> callback);

	void getStudentWorkbooks(int studentId, int classId, boolean fullImage, AsyncCallback<List<WorkbookDTO>> callback);

	void getStudentWorkbooks(int studentId, int start, int end, boolean fullImage,
			AsyncCallback<List<WorkbookDTO>> callback);

	void getWorkbookAvailableClassrooms(int educatorId, int workbookId, int start, int end, boolean fullImage,
			AsyncCallback<List<ClassroomDTO>> callback);

	void createClassroom(String name, String description, String ageGroup, int creatorId, int imageID,
			AsyncCallback<Integer> asyncCallback);

	void saveClassroom(ClassroomDTO w, int imageID, AsyncCallback<Void> asyncCallback);

	void createClassroom(String name, int creatorId, String ageGroup, String description,
			AsyncCallback<Integer> callback);

}