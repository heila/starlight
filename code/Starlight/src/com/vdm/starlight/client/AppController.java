package com.vdm.starlight.client;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.zschech.gwt.comet.client.CometClient;
import net.zschech.gwt.comet.client.CometListener;
import net.zschech.gwt.comet.client.CometSerializer;
import net.zschech.gwt.comet.client.SerialTypes;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Cookies;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.educator.presenter.EducatorHomePresenter;
import com.vdm.starlight.client.educator.view.EducatorHomeViewImpl;
import com.vdm.starlight.client.events.LogInEvent;
import com.vdm.starlight.client.events.LogInEventHandler;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.LogOutEventHandler;
import com.vdm.starlight.client.events.MessageEvent;
import com.vdm.starlight.client.events.MessageEventHandler;
import com.vdm.starlight.client.events.MessageType;
import com.vdm.starlight.client.events.StatusEvent;
import com.vdm.starlight.client.events.StatusEventHandler;
import com.vdm.starlight.client.presenter.LogInPresenter;
import com.vdm.starlight.client.resources.LoginResources;
import com.vdm.starlight.client.student.presenter.StudentHomePresenter;
import com.vdm.starlight.client.student.view.StudentHomeViewImpl;
import com.vdm.starlight.client.view.LogInViewImpl;
import com.vdm.starlight.shared.dto.UserDTO;
import com.vdm.starlight.shared.objects.ChatMessage;
import com.vdm.starlight.shared.objects.StatusUpdate;

/**
 * Main Controller abstract class of the application. Manages log on/off sessions and history stack.
 * 
 * @author Heila van der Merwe
 * @version 2.2
 * 
 */
public class AppController implements ValueChangeHandler<String> {
	Logger logger = Logger.getLogger("AppController");

	/** Listens for registered events */
	private final HandlerManager eventBus;

	private final MessagingServiceAsync messagingService;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** Container of the this application (body) */
	private VerticalPanel container;

	/** Sub widgets */
	private LogInViewImpl logInView = null;
	private Widget homeView = null;

	/** The logged in user's id */
	private int userId;
	private String username;
	/** Indicates if the logged in user is an educator */
	private boolean isEducator = true;

	LoginResources res = LoginResources.INSTANCE;

	private CometClient cometClient;

	@SerialTypes({ ChatMessage.class, StatusUpdate.class })
	public static abstract class ChatCometSerializer extends CometSerializer {
	}

	public AppController(DatabaseServiceAsync dbService, MessagingServiceAsync messagingservice, HandlerManager eventBus) {
		logger.log(Level.INFO, "Constructor");
		this.eventBus = eventBus;
		this.dbService = dbService;
		this.messagingService = messagingservice;
		bind();

	}

	private void bind() {
		// Listens for Log in page re-directions
		History.addValueChangeHandler(this);

		eventBus.addHandler(LogInEvent.TYPE, new LogInEventHandler() {
			@Override
			public void onLogIn(LogInEvent event) {
				messagingService.authenticate(event.getName(), event.getPassword(), new AsyncCallback<Integer>() {

					@Override
					public void onFailure(Throwable caught) {
						logInView
								.setStatus("Log in failed. Could not authenticate the username and password to the database");
					}

					@Override
					public void onSuccess(Integer result) {
						setCookies(result);
						History.newItem("home");

					}

				});
			}

		});

		eventBus.addHandler(LogOutEvent.TYPE, new LogOutEventHandler() {
			@Override
			public void onLogOut(LogOutEvent event) {
				logout();
			}

		});

		eventBus.addHandler(MessageEvent.TYPE, new MessageEventHandler() {

			@Override
			public void message(MessageEvent event) {
				if (event.getType().equals(MessageType.SEND))
					messagingService.send(event.getMessage(), event.getReceiver(), new AsyncCallback<Void>() {
						@Override
						public void onSuccess(Void result) {
						}

						@Override
						public void onFailure(Throwable caught) {
							// output(caught.toString(), "red");// TODO
						}
					});

			}
		});

		eventBus.addHandler(StatusEvent.TYPE, new StatusEventHandler() {

			@Override
			public void getStatus(StatusEvent event) {
				if (event.getType().equals(MessageType.SEND))
					messagingService.setStatus(event.getStatus(), new AsyncCallback<Void>() {
						@Override
						public void onSuccess(Void result) {
						}

						@Override
						public void onFailure(Throwable caught) {
							// TODO output(caught.toString(), "red");
						}
					});

			}
		});

	}

	private void loggedOn() {
		logger.log(Level.INFO, "logged in as " + username);
		CometSerializer serializer = GWT.create(ChatCometSerializer.class);
		cometClient = new CometClient(GWT.getModuleBaseURL() + "comet", serializer, new CometListener() {
			public void onConnected(int heartbeat) {
				// logger.log(Level.INFO, "connected " + heartbeat);
			}

			public void onDisconnected() {
				logger.log(Level.INFO, "disconnected");
			}

			public void onError(Throwable exception, boolean connected) {
				// logger.log(Level.INFO, "error " + connected + " " + exception);
			}

			public void onHeartbeat() {
				// logger.log(Level.INFO, "heartbeat");
			}

			public void onRefresh() {
				// logger.log(Level.INFO, "refresh");
			}

			public void onMessage(List<? extends Serializable> messages) {
				for (Serializable message : messages) {
					if (message instanceof ChatMessage) {
						ChatMessage chatMessage = (ChatMessage) message;
						logger.log(Level.INFO, chatMessage.getReceiver() + ": " + chatMessage.getMessage());
						eventBus.fireEvent(new MessageEvent(chatMessage.getMessage(), chatMessage.getReceiver(),
								MessageType.RECEIVE));

					} else if (message instanceof StatusUpdate) {
						StatusUpdate statusUpdate = (StatusUpdate) message;
						logger.log(Level.INFO, statusUpdate.getUsername() + ": " + statusUpdate.getStatus());
						eventBus.fireEvent(new StatusEvent(statusUpdate.getStatus(), statusUpdate.getUsername(),
								MessageType.RECEIVE));

					} else {
						logger.log(Level.INFO, "unrecognised message " + message);
					}
				}
			}

		});
		cometClient.start();

	}

	/**
	 * Checks for stored session. Validates stored session and redirects to home page otherwise redirects to log in
	 * screen.
	 * 
	 * @param container
	 */
	public void go(final HasWidgets container) {
		this.container = (VerticalPanel) container;

		History.newItem("home");

	}

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			if (token.equals("logIn")) {
				// String id = Cookies.getCookie("pid");
				// if (id != null) {
				// messagingService.getUsername(new AsyncCallback<UserDTO>() {
				// @Override
				// public void onFailure(Throwable caught) {
				// Cookies.removeCookie("pid");
				showLogInView();
				// }
				//
				// @Override
				// public void onSuccess(UserDTO result) {
				// History.newItem("home");
				// }
				// });
				//
				// } else {
				// showLogInView();

				// }
			} else if (token.contains("home")) {

				final String id = Cookies.getCookie("pid");
				if (id != null) {
					messagingService.getUsername(new AsyncCallback<UserDTO>() {
						@Override
						public void onFailure(Throwable caught) {
							Cookies.removeCookie("pid");
							History.newItem("logIn");
						}

						@Override
						public void onSuccess(UserDTO result) {
							if (result == null) {
								Cookies.removeCookie("pid");
								History.newItem("logIn");
							} else {
								logger.log(Level.INFO, "Creating home view and presenter");
								userId = Integer.parseInt(id);
								username = result.getUsername();
								isEducator = result.isEducator();
								loggedOn();
								if (homeView == null || (homeView instanceof EducatorHomeViewImpl && !isEducator)
										|| (homeView instanceof StudentHomeViewImpl && isEducator)) {
									if (isEducator)
										homeView = new EducatorHomeViewImpl();
									else
										homeView = new StudentHomeViewImpl();

								}

								if (isEducator) {
									container.removeStyleName(res.loginCSS().divBody());
									container.setStyleName("vp");
									new EducatorHomePresenter(dbService, eventBus, (EducatorHomeViewImpl) homeView,
											userId, username).go(container);
								} else {
									container.removeStyleName(res.loginCSS().divBody());
									container.setStyleName("vp");
									new StudentHomePresenter(dbService, messagingService, eventBus,
											(StudentHomeViewImpl) homeView, userId).go(container);
								}
							}
						}
					});

				} else {

					History.newItem("logIn");
				}
			}
		}
	}

	public void showLogInView() {
		logger.log(Level.INFO, "Creating Log In view and presenter");

		if (logInView == null) {
			logInView = new LogInViewImpl();
		}
		container.removeStyleName("vp");
		container.setStyleName(res.loginCSS().divBody());
		new LogInPresenter(eventBus, logInView).go(container);
	}

	private void logout() {
		Cookies.removeCookie("pid");
		// Go to LogIn View
		History.newItem("logIn");

		messagingService.logout(username, new AsyncCallback<Void>() {

			@Override
			public void onSuccess(Void result) {
				if (cometClient != null)
					cometClient.stop();
			}

			@Override
			public void onFailure(Throwable caught) {
				logger.log(Level.INFO, caught.toString());
			}
		});
	}

	/**
	 * Store Log In information on disk to persist data between refresh operations
	 * 
	 * @param data
	 */
	private void setCookies(final int id) {
		logger.log(Level.INFO, "setCookie method");
		final long DURATION = 1000 * 60 * 60;
		Date expires = new Date(System.currentTimeMillis() + DURATION);
		Cookies.setCookie("pid", String.valueOf(id), expires, null, "/", false);
	}

}