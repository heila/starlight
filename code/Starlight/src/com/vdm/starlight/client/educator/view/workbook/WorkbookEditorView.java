package com.vdm.starlight.client.educator.view.workbook;

import java.util.List;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;

public interface WorkbookEditorView {

	public interface Presenter {

		void infoLinkClick();

		void viewLinkClick();

		void studentLinkClick();

		void workpageClick(int id);

		public void savePage(int workpageID, String description, int uniqueID);

		public Widget getWidget(String property, String value, PickupDragController dragController);

		Widget getComponent(String[] description);

		void newPage(String name);

	}

	public void createWorkPageTree(WorkbookPagesDTO pages);

	void setPresenter(Presenter presenter);

	Widget asWidget();

	public void createPage(WorkpageDTO result);

	void viewPage(List<String> widgets);
}