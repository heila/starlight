package com.vdm.starlight.client.educator.view.classroom;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public interface ClassroomDialogView {

	public interface Presenter {
		void createClassroom(String[] text, int imageID);

		void updateClassroomBasicInfo(ClassroomDTO w, int imageID);

		void getClassroomInfo(int classroomID);

		void showImagePicker();

		void classroomsLinkClick();

	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void classroomCreatedResult(String string, int intValue);

	void setClassroomInfo(ClassroomDTO w);
}