package com.vdm.starlight.client.educator.view;

import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

public interface StudentsChatView {

	public interface Presenter {

		void sendMesssage(String message);

		void updateStatus(Status valueOf);

		void go(HasWidgets container);

	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void showMessage(String username, String string, String colour);

	void updateStatus(String username, Status status);

	HasData<StudentDTO> getStudentDisplay();

	void setImage(EducatorDTO value);

}