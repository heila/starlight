package com.vdm.starlight.client.educator.view.student;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.StudentDTO;

public interface StudentDialogView {

	public interface Presenter {
		void createStudent(String[] text, int imageID);

		void updateStudentInfo(StudentDTO s, int imageID);

		void getStudentInfo(int studentID);

		void showImagePicker();

		void studentsLinkClick();
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void studentCreatedResult(String string, int intValue);

	void setStudentInfo(StudentDTO s);
}