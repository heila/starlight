package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.presenter.ClassroomsListPresenter.FilterTypes;
import com.vdm.starlight.client.educator.view.EducatorHomeView;
import com.vdm.starlight.client.educator.view.EducatorHomeViewImpl;
import com.vdm.starlight.client.educator.view.classroom.ClassView;
import com.vdm.starlight.client.educator.view.classroom.ClassViewImpl;
import com.vdm.starlight.client.educator.view.classroom.ClassroomDialogView;
import com.vdm.starlight.client.educator.view.classroom.ClassroomDialogViewImpl;
import com.vdm.starlight.client.educator.view.classroom.ClassroomsListView;
import com.vdm.starlight.client.educator.view.classroom.ClassroomsListViewImpl;
import com.vdm.starlight.client.educator.view.media.MediaGridView;
import com.vdm.starlight.client.educator.view.media.MediaGridViewImpl;
import com.vdm.starlight.client.educator.view.media.MediaUploadView;
import com.vdm.starlight.client.educator.view.media.MediaUploadViewImpl;
import com.vdm.starlight.client.educator.view.student.StudentDialogView;
import com.vdm.starlight.client.educator.view.student.StudentDialogViewImpl;
import com.vdm.starlight.client.educator.view.student.StudentViewImpl;
import com.vdm.starlight.client.educator.view.student.StudentsListView;
import com.vdm.starlight.client.educator.view.student.StudentsListViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookDialogView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookDialogViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookEditorView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookEditorViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookInfoView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookInfoViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbooksListView;
import com.vdm.starlight.client.educator.view.workbook.WorkbooksListViewImpl;
import com.vdm.starlight.client.events.ClassroomEvent;
import com.vdm.starlight.client.events.ClassroomEvent.ClassroomEventType;
import com.vdm.starlight.client.events.ClassroomEventHandler;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.MediaEvent;
import com.vdm.starlight.client.events.MediaEventHandler;
import com.vdm.starlight.client.events.MessageEvent;
import com.vdm.starlight.client.events.MessageEventHandler;
import com.vdm.starlight.client.events.StatusEvent;
import com.vdm.starlight.client.events.StatusEventHandler;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.events.StudentEventHandler;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.events.WorkbookEventHandler;
import com.vdm.starlight.client.presenter.Presenter;

public class EducatorHomePresenter implements Presenter, EducatorHomeView.Presenter {
	Logger logger = Logger.getLogger("ProfessorHomePresenter");

	public int educatorId;

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** Sub widgets */
	private final EducatorHomeViewImpl view;

	private StudentsChatPresenter chatPresenter;

	/* *************************** WorkbookViews **************************************** */
	private WorkbookView workbookView;
	private WorkbookEditorView workbookEditorView;
	private WorkbookInfoView workbookInfoView;
	private WorkbooksListView worbooksListView;
	private WorkbookDialogView workbookDialog;

	/* *************************** ClassroomViews **************************************** */
	private ClassView classroomInfoView;
	private ClassroomsListView classroomsListView;
	private ClassroomDialogView classroomDialog;

	/* *************************** StudentViews **************************************** */
	private StudentViewImpl studentInfoView;
	private StudentsListView studentsListView;
	private StudentDialogView studentDialogView;

	/* *************************** MediaViews **************************************** */
	private MediaGridView mediaGridView;
	private MediaUploadView mediaUploadView;

	/* *************************** Constructor **************************************** */
	public EducatorHomePresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			EducatorHomeViewImpl homeView, int educatorId, String username) {
		this.educatorId = educatorId;
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = homeView;
		this.view.setPresenter(this);
		this.chatPresenter = new StudentsChatPresenter(dbService, eventBus, username, educatorId);
		bind();

	}

	private void bind() {
		eventBus.addHandler(WorkbookEvent.TYPE, new WorkbookEventHandler() {
			@Override
			public void workbookEvent(WorkbookEvent event) {
				if (event.getType().equals(WorkbookEvent.WorkbookEventType.INFO)) {
					if (workbookInfoView == null) {
						workbookInfoView = new WorkbookInfoViewImpl();
					}
					new WorkbookInfoPresenter(dbService, eventBus, workbookInfoView, event.getWorkbookId(), educatorId)
							.go(view.getPageContainer());

				} else if (event.getType().equals(WorkbookEvent.WorkbookEventType.EDITOR)) {
					if (workbookEditorView == null) {
						workbookEditorView = new WorkbookEditorViewImpl();
					}
					new WorkbookEditorPresenter(dbService, eventBus, workbookEditorView, event.getWorkbookId()).go(view
							.getPageContainer());

				} else if (event.getType().equals(WorkbookEvent.WorkbookEventType.VIEW)) {
					if (workbookView == null) {
						workbookView = new WorkbookViewImpl();
					}
					new WorkbookViewPresenter(dbService, eventBus, workbookView, event.getWorkbookId()).go(view
							.getPageContainer());
				} else if (event.getType().equals(WorkbookEvent.WorkbookEventType.NEW)) {
					if (workbookDialog == null) {
						workbookDialog = new WorkbookDialogViewImpl();
					}
					new WorkbookDialogPresenter(dbService, eventBus, workbookDialog, educatorId).go(view
							.getPageContainer());
				} else if (event.getType().equals(WorkbookEvent.WorkbookEventType.WORKBOOKS)) {
					workbooksItemClicked();
				}

			}
		});

		eventBus.addHandler(StudentEvent.TYPE, new StudentEventHandler() {
			@Override
			public void studentEvent(StudentEvent event) {
				if (event.getType().equals(StudentEvent.StudentEventType.INFO)) {
					if (studentInfoView == null) {
						studentInfoView = new StudentViewImpl();
					}
					new StudentInfoPresenter(dbService, eventBus, studentInfoView, event.getStudentId()).go(view
							.getPageContainer());

				} else if (event.getType().equals(StudentEvent.StudentEventType.STUDENTS)) {
					studentsItemClicked();
				} else if (event.getType().equals(StudentEvent.StudentEventType.NEW)) {
					if (studentDialogView == null) {
						studentDialogView = new StudentDialogViewImpl();
					}
					new StudentDialogPresenter(dbService, eventBus, studentDialogView, 0).go(view.getPageContainer());

				}
			}
		});

		eventBus.addHandler(ClassroomEvent.TYPE, new ClassroomEventHandler() {
			@Override
			public void classroomEvent(ClassroomEvent event) {
				if (event.getType().equals(ClassroomEvent.ClassroomEventType.INFO)) {
					if (classroomInfoView == null) {
						classroomInfoView = new ClassViewImpl();
					}
					new ClassroomInfoPresenter(dbService, eventBus, classroomInfoView, event.getClassroomId()).go(view
							.getPageContainer());
				} else if (event.getType().equals(ClassroomEvent.ClassroomEventType.NEW)) {
					if (classroomDialog == null) {
						classroomDialog = new ClassroomDialogViewImpl();
					}
					new ClassroomDialogPresenter(dbService, eventBus, classroomDialog, educatorId).go(view
							.getPageContainer());
				} else if (event.getType().equals(ClassroomEventType.CLASSROOMS)) {
					classroomsItemClicked();
				}
			}
		});
		eventBus.addHandler(MediaEvent.TYPE, new MediaEventHandler() {
			@Override
			public void mediaEvent(MediaEvent event) {
				if (event.getType().equals(MediaEvent.MediaEventType.GRID)) {
					if (mediaGridView == null) {
						mediaGridView = new MediaGridViewImpl();
					}
					new MediaGridPresenter(dbService, eventBus, mediaGridView).go(view.getPageContainer());

				} else if (event.getType().equals(MediaEvent.MediaEventType.UPLOAD)) {
					if (mediaUploadView == null) {
						mediaUploadView = new MediaUploadViewImpl();
					}
					new MediaUploadPresenter(dbService, eventBus, mediaUploadView).go(view.getPageContainer());

				}

			}
		});
		eventBus.addHandler(MessageEvent.TYPE, new MessageEventHandler() {

			@Override
			public void message(MessageEvent event) {
				if (event.getType() == com.vdm.starlight.client.events.MessageType.RECEIVE) {
					view.newMessage(true);
					chatPresenter.showMessage(event.getMessage(), event.getReceiver());
				}
			}
		});

		eventBus.addHandler(StatusEvent.TYPE, new StatusEventHandler() {

			@Override
			public void getStatus(StatusEvent event) {
				if (event.getType() == com.vdm.starlight.client.events.MessageType.RECEIVE) {
					view.newMessage(true);
					chatPresenter.showUpdatedStatus(event.getUsername(), event.getStatus());
				}
			}
		});

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		if (worbooksListView == null) {
			worbooksListView = new WorkbooksListViewImpl();

		}
		new WorkbooksListPresenter(dbService, eventBus, worbooksListView, educatorId).go(view.getPageContainer());
		container.add(view.asWidget());

	}

	@Override
	public void workbooksItemClicked() {
		if (worbooksListView == null) {
			worbooksListView = new WorkbooksListViewImpl();

		}
		new WorkbooksListPresenter(dbService, eventBus, worbooksListView, educatorId).go(view.getPageContainer());
	}

	@Override
	public void classroomsItemClicked() {
		if (classroomsListView == null) {
			classroomsListView = new ClassroomsListViewImpl();

		}
		new ClassroomsListPresenter(dbService, eventBus, classroomsListView, FilterTypes.FILTER_BY_EDUCATOR,
				educatorId, -1, -1).go(view.getPageContainer());

	}

	@Override
	public void settingsItemClicked() {
		chatPresenter.go(view.getPageContainer());
		this.view.newMessage(false);
	}

	@Override
	public void mediaItemClicked() {
		if (mediaGridView == null) {
			mediaGridView = new MediaGridViewImpl();
		}
		new MediaGridPresenter(dbService, eventBus, mediaGridView).go(view.getPageContainer());
	}

	@Override
	public void logoutItemClicked() {
		eventBus.fireEvent(new LogOutEvent());
	}

	@Override
	public void studentsItemClicked() {
		if (studentsListView == null) {
			studentsListView = new StudentsListViewImpl();

		}
		new StudentsListPresenter(dbService, eventBus, studentsListView, educatorId).go(view.getPageContainer());

	}

}
