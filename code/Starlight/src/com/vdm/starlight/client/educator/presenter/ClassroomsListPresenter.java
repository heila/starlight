package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.classroom.ClassroomsListView;
import com.vdm.starlight.client.events.ClassroomEvent;
import com.vdm.starlight.client.events.ClassroomEvent.ClassroomEventType;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.events.WorkbookEvent.WorkbookEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomsListPresenter implements Presenter, ClassroomsListView.Presenter {
	Logger logger = Logger.getLogger("ProfessorHomePresenter");

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int educatorId;
	private int workbookId;
	private int studentId;

	/** View */
	private final ClassroomsListView view;

	AsyncDataProvider<ClassroomDTO> dataProvider;

	private FilterTypes FILTER;

	Range range;
	int start;
	int end;

	public ClassroomsListPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, ClassroomsListView view,
			FilterTypes type, final int educatorId, final int workbookId, final int studentId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = view;
		this.workbookId = workbookId;
		this.educatorId = educatorId;
		this.studentId = studentId;
		this.FILTER = type;
		bind();
		this.view.setPresenter(this);

	}

	public void bind() {
		// Create a data provider.
		dataProvider = new AsyncDataProvider<ClassroomDTO>() {
			@Override
			protected void onRangeChanged(HasData<ClassroomDTO> display) {
				range = display.getVisibleRange();
				start = range.getStart();
				end = start + range.getLength();

				if (FILTER.equals(FilterTypes.FILTER_BY_EDUCATOR)) { // no filters
					dbService.getEducatorClassrooms(educatorId, start, end, false, new ClassroomListAsyncCallback());
				} else if (FILTER.equals(FilterTypes.FILTER_BY_WORKBOOK)) {
					dbService.getWorkbookClassrooms(workbookId, start, end, false, new ClassroomListAsyncCallback());
				} else if (FILTER.equals(FilterTypes.FILTER_BY_STUDENT)) {
					dbService.getStudentClassrooms(studentId, start, end, false, new ClassroomListAsyncCallback());
				} else if (FILTER.equals(FilterTypes.FILTER_BY_EDUCATOR_WORKBOOK)) {
					dbService.getWorkbookAvailableClassrooms(educatorId, workbookId, start, end, false,
							new ClassroomListAsyncCallback());
				}

			}
		};
		// Connect the list to the data provider.
		dataProvider.addDataDisplay(this.view.getDataDisplay());
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		view.clear(); // clear selection
		container.add(view.asWidget());

	}

	@Override
	public void infoLinkClick(int selectedItem) {
		eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.INFO, selectedItem));
	}

	@Override
	public void newLinkClick() {
		eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.NEW, -1));
	}

	@Override
	public void addWorkbookToClassroom(int classroomId) {
		dbService.addWorkbookToClassroomStudents(workbookId, classroomId, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Workbook not added to classroom, call to server failed");

			}

			@Override
			public void onSuccess(Void result) {
				eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.INFO, workbookId));
				Window.alert("Added workbook to classroom");

			}
		});

	}

	public static enum FilterTypes {
		FILTER_BY_WORKBOOK, FILTER_BY_STUDENT, FILTER_BY_EDUCATOR, FILTER_BY_EDUCATOR_WORKBOOK;
	}

	class ClassroomListAsyncCallback implements AsyncCallback<List<ClassroomDTO>> {

		@Override
		public void onFailure(Throwable caught) {
			Window.alert("Error fetching class' students details" + caught.getMessage());

		}

		@Override
		public void onSuccess(List<ClassroomDTO> result) {
			if (result.size() == 0) {
				ClassroomsListPresenter.this.view.getDataDisplay().setVisibleRange(start - 5, start);
			} else
				ClassroomsListPresenter.this.view.getDataDisplay().setRowData(start, result);
		}

	}

}
