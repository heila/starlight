package com.vdm.starlight.client.educator.view;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.cellview.client.AbstractPager;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasRows;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.resources.EducatorResources;

public class RangePager<T> extends AbstractPager {
	/**
	 * The default increment size.
	 */
	private static final int DEFAULT_INCREMENT = 5;

	/**
	 * The increment size.
	 */
	private int incrementSize = DEFAULT_INCREMENT;

	private HorizontalPanel pagePanel = new HorizontalPanel();

	EducatorResources res = EducatorResources.INSTANCE;

	/**
	 * The label that shows the current range.
	 */
	private final Anchor nextLabel = new Anchor("Next");
	/**
	 * The label that shows the current range.
	 */
	private final Anchor prevLabel = new Anchor("Prev");

	/**
	 * Construct a new {@link RangePager}.
	 */
	public RangePager() {
		nextLabel.setStyleName(res.starlightCSS().pagerLabelRight());
		prevLabel.setStyleName(res.starlightCSS().pagerLabelLeft());
		pagePanel.setVerticalAlignment(HorizontalPanel.ALIGN_MIDDLE);
		pagePanel.setHeight("40px");
		pagePanel.add(prevLabel);
		pagePanel.add(nextLabel);
		initWidget(pagePanel);
		nextLabel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				HasRows display = getDisplay(); // Link to celllist
				Range r = display.getVisibleRange();
				int rows = display.getRowCount(); // Total number of rows
				int end = r.getStart() + 5;

				if (rows >= end) {
					display.setVisibleRange(end, incrementSize);
				}
			}
		});
		prevLabel.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				HasRows display = getDisplay();

				int start = display.getVisibleRange().getStart() - incrementSize;
				if (start >= 0)
					display.setVisibleRange(start, incrementSize);
				// Window.alert("next pressed " + end + " " + incrementSize);
			}
		});
	}

	/**
	 * Get the number of rows by which the range is increased when the scrollbar reaches the bottom.
	 * 
	 * @return the increment size
	 */
	public int getIncrementSize() {
		return incrementSize;
	}

	@Override
	public void setDisplay(HasRows display) {
		assert display instanceof Widget : "display must extend Widget";
		super.setDisplay(display);
	}

	/**
	 * Set the number of rows by which the range is increased when the scrollbar reaches the bottom.
	 * 
	 * @param incrementSize
	 *            the incremental number of rows
	 */
	public void setIncrementSize(int incrementSize) {
		this.incrementSize = incrementSize;
	}

	@Override
	protected void onRangeOrRowCountChanged() {
		// HasRows display = getDisplay();
		//
		// Range range = display.getVisibleRange();
		//
		// int start;
		// int end;
		//
		// start = range.getStart();
		// end = start + range.getLength();

	}

	public void enableNext(boolean b) {
		this.nextLabel.setEnabled(b);
	}

	public void enablePrev(boolean b) {
		this.nextLabel.setEnabled(b);
	}
}
