package com.vdm.starlight.client.educator.view.workbook;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.client.educator.view.classroom.ClassroomList;
import com.vdm.starlight.client.educator.view.student.StudentsList;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;

public interface WorkbookInfoView {

	public interface Presenter {

		// void infoLinkClick();

		void designLinkClick();

		void studentLinkClick();

		void viewLinkClick();

		void editLinkClick();

		void addStudentLinkClick();

		void addClassLinkClick();

		void publishLinkClick();

		void printLinkClick();

		void deleteLinkClick();

		void onClassroomClick(int id);

		void onStudentClick(int selectedItem);

		void getWorkbookStudents(int start, int end, HasData<StudentDTO> view);

		void getWorkbookClassrooms(int start, int end, HasData<ClassroomDTO> view);
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void setStats(WorkbookStatsDTO result);

	void setWorkbookInfo(WorkbookDTO result);

	ClassroomList getClassroomListDisplay();

	StudentsList getStudentsListDisplay();
}