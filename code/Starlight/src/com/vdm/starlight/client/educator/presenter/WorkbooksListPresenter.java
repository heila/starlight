package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.workbook.WorkbooksListView;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.events.WorkbookEvent.WorkbookEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.WorkbookDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbooksListPresenter implements Presenter, WorkbooksListView.Presenter {
	Logger logger = Logger.getLogger(" WorkbooksListPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int educatorId;

	/** View */
	private final WorkbooksListView view;

	AsyncDataProvider<WorkbookDTO> dataProvider;

	public WorkbooksListPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, WorkbooksListView view,
			final int educatorId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = view;
		this.view.setPresenter(this);
		this.educatorId = educatorId;
		bind();

	}

	public void bind() {
		// Create a data provider.
		dataProvider = new AsyncDataProvider<WorkbookDTO>() {
			@Override
			protected void onRangeChanged(HasData<WorkbookDTO> display) {
				final Range range = display.getVisibleRange();
				final int start = range.getStart();
				int end = start + range.getLength();

				dbService.getEducatorWorkbooks(educatorId, start, end, false, new AsyncCallback<List<WorkbookDTO>>() {
					public void onSuccess(List<WorkbookDTO> result) {
						if (result.size() == 0) {
							WorkbooksListPresenter.this.view.getDataDisplay().setVisibleRange(start - 5, start);
						} else
							WorkbooksListPresenter.this.view.getDataDisplay().setRowData(start, result);
					}

					public void onFailure(Throwable caught) {
						Window.alert("Error fetching class' workbooks details: " + caught.getMessage());
					}
				});
			}
		};

		// Connect the list to the data provider.
		dataProvider.addDataDisplay(this.view.getDataDisplay());
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		view.clear(); // clear selection
		container.add(view.asWidget());
	}

	@Override
	public void infoLinkClick(int selectedItem) {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.INFO, selectedItem));
	}

	@Override
	public void newLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.NEW, -1));
	}

}
