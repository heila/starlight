package com.vdm.starlight.client.educator.view.student;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.client.educator.view.RangePager;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentsList extends Composite {
	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("StudentsList.ui.xml")
	interface StudentsListUiBinder extends UiBinder<Widget, StudentsList> {
	}

	private static StudentsListUiBinder uiBinder = GWT.create(StudentsListUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of ClassroomInfo objects */
	@UiField(provided = true)
	CellList<StudentDTO> studentList;

	@UiField
	RangePager<StudentDTO> pager;

	public StudentsList() {
		StudentCell cell = new StudentCell();
		studentList = new CellList<StudentDTO>(cell);
		pager = new RangePager<StudentDTO>();
		initWidget(uiBinder.createAndBindUi(this));

		studentList.setVisibleRange(0, DEFAULT_LIST_SIZE);

		pager.setDisplay(studentList);

	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget implementing HasData which displays the list of classrooms
	 */
	public HasData<StudentDTO> getDataDisplay() {
		return this.studentList;
	}

	public void enableNext(boolean b) {
		this.pager.enableNext(b);
	}

	public void enablePrev(boolean b) {
		this.pager.enablePrev(b);
	}

}
