package com.vdm.starlight.client.educator.view.classroom;

import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.client.educator.view.student.StudentsList;
import com.vdm.starlight.client.educator.view.workbook.WorkbookList;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public interface ClassView {
	public interface Presenter {
		void getClassData();

		void listLinkClick();

		void newLinkClick();

		void getClassroomWorkbooks(int start, int end, CellList<WorkbookDTO> dataDisplay);

		void getClassroomStudents(int start, int end, HasData<StudentDTO> dataDisplay);

		void onWorkbookClick(int id);

		void onStudentClick(int id);

		void editLinkClick();

		void studentLinkClick();

		void classLinkClick();

		void closeLinkClick();
	}

	void setPresenter(Presenter presenter);

	void setClassData(ClassroomDTO c);

	Widget asWidget();

	WorkbookList getWorkbookListDisplay();

	StudentsList getStudentsListDisplay();
}
