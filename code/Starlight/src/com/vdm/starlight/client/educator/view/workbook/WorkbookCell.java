package com.vdm.starlight.client.educator.view.workbook;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.objects.Workbook;

/**
 * The Cell used to render a {@link Workbook}.
 */
public class WorkbookCell extends AbstractCell<WorkbookDTO> {
	static EducatorResources resources = EducatorResources.INSTANCE;

	@Override
	public void render(Context context, WorkbookDTO value, SafeHtmlBuilder sb) {
		// Value can be null, so do a null check..
		if (value == null) {
			return;
		}
		sb.appendHtmlConstant("<div style=\"border-bottom: 1px solid #ccb;width:450px;\"><table>");
		String imageHTML = "";

		if (value.isDefaultImage()) {
			ImageResource im = resources.book();
			imageHTML = "<img src=\"" + im.getSafeUri().asString() + "\" height=\"50px\"/>";
		} else {
			imageHTML = "<img src=\"" + value.getImage() + "\" height=\"50px\"/>";
		}

		sb.appendHtmlConstant("<tr><td rowspan='2' width='50px' align='center'>");
		sb.appendHtmlConstant(imageHTML);
		sb.appendHtmlConstant("</td>");

		// Add the name and date created.
		sb.appendHtmlConstant("<td width='250px' style='font-size:110%; font-weight: bold;padding-left: 2px;'>");
		sb.appendEscaped(value.getName());
		sb.appendHtmlConstant("</td></td><td style='color:#bbb;'>");
		sb.appendEscaped(value.getDateCreated().toGMTString()
				.substring(0, value.getDateCreated().toGMTString().length() - 4));
		sb.appendHtmlConstant("</td></tr><tr><td style='color: #0887D0;padding-left: 2px;font-size:80%;'>");
		sb.appendEscaped(value.getDescription());
		sb.appendHtmlConstant("</td></tr></table></div>");
	}

}