package com.vdm.starlight.client.educator.view.media;

import com.google.gwt.user.client.ui.Widget;

public interface MediaUploadView {

	public interface Presenter {

		void gridLinkClick();

		void createResource(String filename);

		// void getImage(int id);
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

}