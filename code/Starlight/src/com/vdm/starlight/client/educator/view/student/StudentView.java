package com.vdm.starlight.client.educator.view.student;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.StudentDTO;

public interface StudentView {

	public interface Presenter {

		void newLinkClick();

		void editLinkClick();

		void listLinkClick();

		void getStudentInfo();
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void setData(StudentDTO student);
}