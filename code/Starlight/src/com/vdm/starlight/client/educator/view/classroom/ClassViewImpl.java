package com.vdm.starlight.client.educator.view.classroom;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.educator.view.student.StudentsList;
import com.vdm.starlight.client.educator.view.workbook.WorkbookList;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;

/**
 * This class sets up the interface of the class widget. It also creates and registers events on the table of students
 * enrolled for the class.
 * 
 * @author Heila van der Merwe
 * @date Oct 7 2011
 * 
 */
public class ClassViewImpl extends Composite implements ClassView {
	static Logger logger = Logger.getLogger("ClassViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	@UiTemplate("ClassView.ui.xml")
	interface ClassViewUiBinder extends UiBinder<Widget, ClassViewImpl> {
	}

	private static ClassViewUiBinder uiBinder = GWT.create(ClassViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	@UiField
	Anchor newLink;
	@UiField
	Anchor listLink;

	@UiField
	Anchor editLink;
	@UiField
	Anchor classLink;
	@UiField
	Anchor studentLink;
	@UiField
	Anchor closeLink;

	@UiField
	Label dateCreatedLabel;
	@UiField
	Label descriptionTextBox;
	@UiField
	Label nameLabel;
	@UiField
	Label ageLabel;

	@UiField
	Image imagehtml;

	@UiField
	Grid grid;

	@UiField
	WorkbookList workbookList;

	@UiField
	StudentsList studentsList;

	private Presenter presenter;

	public ClassViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		grid.getCellFormatter().getElement(0, 0).setAttribute("colspan", "2");

		workbookList.getDataDisplay().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				final Range range = event.getNewRange();
				final int start = range.getStart();
				int end = start + range.getLength();
				if (presenter != null)
					presenter.getClassroomWorkbooks(start, end, getWorkbookListDisplay().getDataDisplay());
			}
		});

		studentsList.getDataDisplay().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				final Range range = event.getNewRange();
				final int start = range.getStart();
				int end = start + range.getLength();
				if (presenter != null)
					presenter.getClassroomStudents(start, end, getStudentsListDisplay().getDataDisplay());
			}
		});

		// Add a selection model so we can select cells.
		final SingleSelectionModel<WorkbookDTO> selectionModel = new SingleSelectionModel<WorkbookDTO>(
				WorkbookDTO.KEY_PROVIDER);

		workbookList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				WorkbookDTO workbook = selectionModel.getSelectedObject();
				if (workbook != null)
					presenter.onWorkbookClick(selectionModel.getSelectedObject().getId());
			}
		});

		// Add a selection model so we can select cells.
		final SingleSelectionModel<StudentDTO> studentSelectionModel = new SingleSelectionModel<StudentDTO>(
				StudentDTO.KEY_PROVIDER);

		studentsList.getDataDisplay().setSelectionModel(studentSelectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				StudentDTO student = studentSelectionModel.getSelectedObject();
				if (student != null)
					presenter.onStudentClick(studentSelectionModel.getSelectedObject().getId());
			}
		});

	}

	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	public Widget asWidget() {
		return this;
	}

	@Override
	public void setClassData(ClassroomDTO c) {
		dateCreatedLabel.setText(c.getDateCreated().toGMTString());
		ageLabel.setText(c.getAgeGroup());
		descriptionTextBox.setText(c.getDescription());
		nameLabel.setText(c.getName());

		if (c.isDefaultImage()) {
			ImageResource im = resources.deur();
			imagehtml.setUrl(im.getSafeUri());
		} else {
			imagehtml.setUrl(c.getImage());
		}

		if (imagehtml.getHeight() > imagehtml.getWidth()) {
			imagehtml.setHeight("160px");
		} else {
			imagehtml.setWidth("160px");
		}
	}

	@UiHandler("newLink")
	void onNewLinkClick(ClickEvent event) {
		this.presenter.newLinkClick();

	}

	@UiHandler("listLink")
	void onListLinkClick(ClickEvent event) {
		this.presenter.listLinkClick();
	}

	@Override
	public WorkbookList getWorkbookListDisplay() {
		return workbookList;
	}

	@Override
	public StudentsList getStudentsListDisplay() {
		return studentsList;
	}

	@UiHandler("editLink")
	void onEditLinkClick(ClickEvent event) {
		this.presenter.editLinkClick();
	}

	@UiHandler("studentLink")
	void onStudentLinkClick(ClickEvent event) {
		this.presenter.studentLinkClick();
	}

	@UiHandler("classLink")
	void onClassLinkClick(ClickEvent event) {
		this.presenter.classLinkClick();
	}

	@UiHandler("closeLink")
	void onCloseLinkClick(ClickEvent event) {
		this.presenter.closeLinkClick();
	}

}
