package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.HasImage;
import com.vdm.starlight.client.editor.ImagePickerViewImpl;
import com.vdm.starlight.client.educator.view.media.MediaGridView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookDialogView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookEditDialogViewImpl;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.events.WorkbookEvent.WorkbookEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public class WorkbookDialogPresenter implements Presenter, WorkbookDialogView.Presenter {

	Logger logger = Logger.getLogger(" WorkbookNewPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final WorkbookDialogView view;

	int educatorID;

	int workbookID;

	MediaGridView imagePickerView;

	public WorkbookDialogPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			WorkbookDialogView workbookDialogView, int educatorID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookDialogView;
		this.view.setPresenter(this);
		this.educatorID = educatorID;
		this.workbookID = -1;

	}

	public WorkbookDialogPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			WorkbookDialogView workbookDialogView, int educatorID, int workbookID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookDialogView;
		this.view.setPresenter(this);
		this.educatorID = educatorID;
		this.workbookID = workbookID;
	}

	@Override
	public void go(HasWidgets container) {

		container.clear();
		container.add(view.asWidget());

	}

	@Override
	public void createWorkbook(final String[] text, final int imageID) {
		dbService.createWorkbook(text[0], text[1], text[2], educatorID, imageID, new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error creating workbook: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Integer result) {
				WorkbookDialogPresenter.this.view.workbookCreatedResult(text[0], result.intValue());
			}
		});

	}

	@Override
	public void workbooksLinkClick() {
		if (eventBus != null) {
			eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.WORKBOOKS, 0));
		}
	}

	@Override
	public void showImagePicker() {
		boolean loadImages = false;
		if (imagePickerView == null) {
			imagePickerView = new ImagePickerViewImpl();
			loadImages = true;
		}
		MediaGridPresenter pres = new MediaGridPresenter(dbService, imagePickerView, (HasImage) this.view);
		if (loadImages) {
			pres.go(null);
		}

		imagePickerView.asWidget();

	}

	@Override
	public void getWorkbookInfo(int workbookID) {
		dbService.getWorkbook(workbookID, true, new AsyncCallback<WorkbookDTO>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Could not retrieve workbook information");
			}

			@Override
			public void onSuccess(WorkbookDTO result) {
				view.setWorkbookInfo(result);
			}
		});
	}

	@Override
	public void updateWorkbookBasicInfo(final WorkbookDTO w, int imageID) {
		dbService.saveWorkbook(w, imageID, new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error updating workbook: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Void result) {
				eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.INFO, w.getId()));
				((WorkbookEditDialogViewImpl) view).editWorkbookDialog.hide();
			}
		});

	}

}