package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.classroom.ClassView;
import com.vdm.starlight.client.educator.view.classroom.ClassroomDialogView;
import com.vdm.starlight.client.educator.view.classroom.ClassroomEditDialogViewImpl;
import com.vdm.starlight.client.events.ClassroomEvent;
import com.vdm.starlight.client.events.ClassroomEvent.ClassroomEventType;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.events.StudentEvent.StudentEventType;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.events.WorkbookEvent.WorkbookEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class ClassroomInfoPresenter implements Presenter, ClassView.Presenter {
	Logger logger = Logger.getLogger(" ClassroomInfoPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int classroomId;

	/** View */
	private final ClassView view;

	private ClassroomDialogView classroomEditDialog;

	public ClassroomInfoPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			ClassView classroomInfoView, int classroomId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = classroomInfoView;
		this.classroomId = classroomId;
		this.view.setPresenter(this);

	}

	public void bind() {
		getClassData();

		view.getStudentsListDisplay().getDataDisplay().setVisibleRangeAndClearData(new Range(0, 5), true);
		view.getWorkbookListDisplay().getDataDisplay().setVisibleRangeAndClearData(new Range(0, 5), true);

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		bind();
	}

	@Override
	public void listLinkClick() {
		eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.CLASSROOMS, 0));

	}

	@Override
	public void newLinkClick() {
		eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.NEW, 0));

	}

	@Override
	public void getClassData() {
		dbService.getClassroom(classroomId, true, new AsyncCallback<ClassroomDTO>() {
			public void onSuccess(ClassroomDTO result) {
				view.setClassData(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching classroom info" + caught.getMessage());
			}
		});

	}

	@Override
	public void onWorkbookClick(int selectedItem) {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.INFO, selectedItem));
	}

	@Override
	public void onStudentClick(int selectedItem) {
		eventBus.fireEvent(new StudentEvent(StudentEventType.INFO, selectedItem));
	}

	@Override
	public void getClassroomWorkbooks(final int start, int end, final CellList<WorkbookDTO> dataDisplay) {
		dbService.getClassroomWorkbooks(classroomId, false, new AsyncCallback<List<WorkbookDTO>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching classroom workbooks details" + caught.getMessage());

			}

			@Override
			public void onSuccess(List<WorkbookDTO> result) {
				dataDisplay.setRowData(start, result);
			}
		});
	}

	@Override
	public void getClassroomStudents(final int start, int end, final HasData<StudentDTO> dataDisplay) {
		dbService.getClassroomStudents(classroomId, false, new AsyncCallback<List<StudentDTO>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching classroom workbooks details" + caught.getMessage());

			}

			@Override
			public void onSuccess(List<StudentDTO> result) {
				dataDisplay.setRowData(start, result);
			}
		});

	}

	@Override
	public void editLinkClick() {
		if (classroomEditDialog == null) {
			classroomEditDialog = new ClassroomEditDialogViewImpl(classroomId);
		}
		new ClassroomDialogPresenter(dbService, eventBus, classroomEditDialog, classroomId)
				.getClassroomInfo(classroomId);

		classroomEditDialog.asWidget();

	}

	@Override
	public void studentLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void classLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void closeLinkClick() {
		// TODO Auto-generated method stub

	}

}
