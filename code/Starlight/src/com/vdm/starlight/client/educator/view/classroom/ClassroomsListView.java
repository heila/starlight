package com.vdm.starlight.client.educator.view.classroom;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public interface ClassroomsListView {

	public interface Presenter {

		void newLinkClick();

		void infoLinkClick(int id);

		public void addWorkbookToClassroom(int classroomId);

	}

	void setPresenter(Presenter presenter);

	public HasData<ClassroomDTO> getDataDisplay();

	Widget asWidget();

	void clear();

}