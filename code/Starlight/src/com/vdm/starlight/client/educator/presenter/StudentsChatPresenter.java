package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.StudentsChatView;
import com.vdm.starlight.client.educator.view.StudentsChatViewImpl;
import com.vdm.starlight.client.events.MessageEvent;
import com.vdm.starlight.client.events.MessageType;
import com.vdm.starlight.client.events.StatusEvent;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.objects.StatusUpdate;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

public class StudentsChatPresenter implements StudentsChatView.Presenter {
	Logger logger = Logger.getLogger(" StudentDialogPresenter");
	static EducatorResources resources = EducatorResources.INSTANCE;

	/** Registration of global events */
	private final HandlerManager eventBus;

	private DatabaseServiceAsync dbservice;

	private String username;

	private StudentsChatView view;

	private int educatorId;

	public StudentsChatPresenter(DatabaseServiceAsync dbservice, HandlerManager eventBus, String username,
			int educatorId) {
		this.eventBus = eventBus;
		this.username = username;
		view = new StudentsChatViewImpl(username);
		view.setPresenter(this);
		this.educatorId = educatorId;
		this.dbservice = dbservice;
		showImage();
	}

	public void showImage() {
		dbservice.getEducator(educatorId, new AsyncCallback<EducatorDTO>() {

			@Override
			public void onSuccess(EducatorDTO value) {
				view.setImage(value);
			}

			@Override
			public void onFailure(Throwable caught) {

			}
		});

	}

	public void showMessage(String message, String username) {
		view.showMessage(username + ": ", message, "black");
	}

	public void showError(String message) {
		view.showMessage("", "ERROR: " + message, "red");
	}

	public void setListContacts(List<StatusUpdate> contactList) {
		for (StatusUpdate s : contactList) {
			view.updateStatus(s.getUsername(), s.getStatus());
		}
	}

	public void updateStatus(Status newStatus) {
		eventBus.fireEvent(new StatusEvent(newStatus, this.username, MessageType.SEND));

	}

	public void showUpdatedStatus(String username, Status status) {
		view.updateStatus(username, status);
	}

	@Override
	public void sendMesssage(String message) {
		eventBus.fireEvent(new MessageEvent(message, this.username, com.vdm.starlight.client.events.MessageType.SEND));
		view.showMessage("me: ", message, "black");
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
	}

}
