package com.vdm.starlight.client.educator.view.student;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.shared.dto.StudentDTO;

public interface StudentsListView {

	public interface Presenter {

		void newLinkClick();

		// void listLinkClick();

		void infoLinkClick(int id);
	}

	void setPresenter(Presenter presenter);

	public HasData<StudentDTO> getDataDisplay();

	Widget asWidget();

	void clear();
}