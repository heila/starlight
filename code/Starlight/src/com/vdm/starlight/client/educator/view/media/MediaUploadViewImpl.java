package com.vdm.starlight.client.educator.view.media;

import gwtupload.client.IUploadStatus.Status;
import gwtupload.client.IUploader;
import gwtupload.client.IUploader.UploadedInfo;
import gwtupload.client.MultiUploader;
import gwtupload.client.PreloadedImage;
import gwtupload.client.PreloadedImage.OnLoadPreloadedImageHandler;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.ResourceDTO;

public class MediaUploadViewImpl extends Composite implements MediaUploadView {
	static Logger logger = Logger.getLogger(" MediaUploadViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	@UiTemplate("MediaUploadView.ui.xml")
	interface MediaUploadViewUiBinder extends UiBinder<Widget, MediaUploadViewImpl> {
	}

	private static MediaUploadViewUiBinder uiBinder = GWT.create(MediaUploadViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	@UiField
	VerticalPanel rightPanel;

	private Presenter presenter;

	List<ResourceDTO> resourceList;

	Button uploadButton;

	String spinnerHTML;

	DialogBox dialogBox;

	Image previewImage;

	SimplePanel imagePanel;

	MultiUploader defaultUploader;

	FlowPanel panelImages;

	Label done;

	public MediaUploadViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		rightPanel.add(new Label("Please select an image file to upload and then click Upload"));
		defaultUploader = new MultiUploader();
		defaultUploader.avoidRepeatFiles(true);
		rightPanel.add(defaultUploader);
		defaultUploader.setValidExtensions("png", "jpg", "jpeg", "gif", "bmp");
		defaultUploader.addOnFinishUploadHandler(onFinishUploaderHandler);
		panelImages = new FlowPanel();
		done = new Label("Sucessfully uploaded images:");
		rightPanel.add(done);
		done.setVisible(false);
		rightPanel.add(panelImages);

	}

	// Load the image in the document and in the case of success attach it to the viewer
	private IUploader.OnFinishUploaderHandler onFinishUploaderHandler = new IUploader.OnFinishUploaderHandler() {
		public void onFinish(IUploader uploader) {
			if (uploader.getStatus() == Status.SUCCESS) {

				new PreloadedImage(uploader.fileUrl(), showImage);

				// The server sends useful information to the client by default
				UploadedInfo info = uploader.getServerInfo();
				logger.log(Level.INFO, "File name " + info.name);
				logger.log(Level.INFO, "File content-type " + info.ctype);
				logger.log(Level.INFO, "File size " + info.size);
				presenter.createResource(info.name);
				// You can send any customized message and parse it
				logger.log(Level.INFO, "Server message " + info.message);
			}
		}
	};

	// Attach an image to the pictures viewer
	private OnLoadPreloadedImageHandler showImage = new OnLoadPreloadedImageHandler() {
		public void onLoad(PreloadedImage image) {
			done.setVisible(true);
			image.setWidth("100px");
			panelImages.add(image);
		}
	};

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public void showImage(String data) {
		Image img = new Image();
		img.setUrl("data:image/" + "png" + ";base64," + data);
		imagePanel.clear();
		imagePanel.add(img);
		previewImage = img;
		dialogBox.show();
	}

	@UiHandler("gridLink")
	void onGridLinkClick(ClickEvent event) {
		this.presenter.gridLinkClick();
	}

}
