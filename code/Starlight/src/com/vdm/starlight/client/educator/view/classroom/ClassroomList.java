package com.vdm.starlight.client.educator.view.classroom;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.educator.view.RangePager;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.objects.Classroom;

public class ClassroomList extends Composite {
	static Logger logger = Logger.getLogger("ClassroomListViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("ClassroomList.ui.xml")
	interface ClassroomListUiBinder extends UiBinder<Widget, ClassroomList> {
	}

	private static ClassroomListUiBinder uiBinder = GWT.create(ClassroomListUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of ClassroomDTO objects */
	@UiField(provided = true)
	CellList<ClassroomDTO> classroomList;

	@UiField
	RangePager<ClassroomDTO> pager;

	public ClassroomList() {
		ClassroomCell cell = new ClassroomCell();
		classroomList = new CellList<ClassroomDTO>(cell);
		pager = new RangePager<ClassroomDTO>();
		initWidget(uiBinder.createAndBindUi(this));

		classroomList.setVisibleRange(0, DEFAULT_LIST_SIZE);

		pager.setDisplay(classroomList);

	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget displaying the list of {@link Classroom} objects
	 */
	public CellList<ClassroomDTO> getDataDisplay() {
		return this.classroomList;
	}

	public void enableNext(boolean b) {
		this.pager.enableNext(b);
	}

	public void enablePrev(boolean b) {
		this.pager.enablePrev(b);
	}

}
