package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.student.StudentView;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.events.StudentEvent.StudentEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.StudentDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentInfoPresenter implements Presenter, StudentView.Presenter {
	Logger logger = Logger.getLogger(" StudentInfoPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int studentID;

	/** View */
	private final StudentView view;

	public StudentInfoPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, StudentView studentView,
			int studentID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = studentView;
		this.view.setPresenter(this);
		this.studentID = studentID;

	}

	public void bind() {
		// Create a data provider.

		// dbService.getWorkbookStat(studentID, new AsyncCallback<WorkbookStatInfo>() {
		// public void onSuccess(WorkbookStatInfo result) {
		// view.setStats(result);
		// }
		//
		// public void onFailure(Throwable caught) {
		// Window.alert("Error fetching" + caught.getMessage());
		// }
		// });

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		// bind();
		getStudentInfo();
	}

	@Override
	public void editLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void newLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void listLinkClick() {
		eventBus.fireEvent(new StudentEvent(StudentEventType.STUDENTS, -1));

	}

	@Override
	public void getStudentInfo() {
		dbService.getStudent(studentID, true, new AsyncCallback<StudentDTO>() {
			public void onSuccess(StudentDTO result) {
				view.setData(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching student info " + caught.getMessage());
			}
		});

	}

}
