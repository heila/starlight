package com.vdm.starlight.client.educator.view;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.DivElement;
import com.google.gwt.dom.client.Document;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.objects.StatusUpdate;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

public class StudentsChatViewImpl extends Composite implements StudentsChatView {
	static Logger logger = Logger.getLogger("StudentsListViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	@UiTemplate("StudentsChatView.ui.xml")
	interface StudentsChatViewUiBinder extends UiBinder<Widget, StudentsChatViewImpl> {
	}

	static {
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	private static StudentsChatViewUiBinder uiBinder = GWT.create(StudentsChatViewUiBinder.class);
	@UiField
	Button sendButton;
	@UiField
	ListBox status;
	@UiField
	TextArea input;
	@UiField
	HTML output;
	@UiField
	Image image;
	@UiField
	Label lblName;

	/** The list of students assigned the workbook */
	@UiField(provided = true)
	CellList<StudentDTO> studentList;

	/** Selection model for students */
	SingleSelectionModel<StudentDTO> selectionModel;

	private Presenter presenter;

	public StudentsChatViewImpl(String username) {
		StudentCell cell = new StudentCell();
		studentList = new CellList<StudentDTO>(cell);
		logger.log(Level.INFO, "WorkbookView Constructor");
		initWidget(uiBinder.createAndBindUi(this));
		lblName.setText(username);
		ImageResource im = resources.defaultProfile();
		image.setUrl(im.getSafeUri());
		image.setWidth("128px");
		image.setHeight("128px");

		// Set the current status of the educator to online
		for (Status s : StatusUpdate.Status.values()) {
			if (s.equals(Status.OFFLINE))
				continue;
			status.addItem(s.toString());
		}
		status.setSelectedIndex(0);

		studentList.setVisibleRange(0, 1000);

		selectionModel = new SingleSelectionModel<StudentDTO>(StudentDTO.KEY_PROVIDER);

		studentList.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				int id = selectionModel.getSelectedObject().getId();
				logger.log(Level.INFO, "Student id = " + id + " selected.");
				// presenter.getStudentData();
			}
		});

	}

	@Override
	public HasData<StudentDTO> getStudentDisplay() {
		return studentList;
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@UiHandler("sendButton")
	void onSendButtonClick(ClickEvent event) {
		String message = input.getText().trim();
		if (message.length() > 0) {
			this.presenter.sendMesssage(message);
			this.input.setValue("");
		}
	}

	@UiHandler("status")
	void onStatusChange(ChangeEvent event) {
		this.presenter.updateStatus(Status.valueOf(status.getItemText(status.getSelectedIndex())));
	}

	@Override
	public void showMessage(String username, String string, String colour) {
		DivElement div = Document.get().createDivElement();
		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendHtmlConstant("<table width =500px>");
		sb.appendHtmlConstant("<tr ><td rowspan='2' width='32px' align='left' valign='top' >");
		String imageHTML = "";
		ImageResource im = resources.defaultIcon();
		imageHTML = "<img src=\"" + im.getSafeUri().asString() + "\" height=\"32px\"/>";
		sb.appendHtmlConstant(imageHTML);
		sb.appendHtmlConstant("</td>");

		// Add the name and date created.
		sb.appendHtmlConstant("<td  style='font-size:110%; padding-left: 2px; color:" + colour
				+ "'><span style='color:blue; font-weight:bold; '>");

		sb.appendEscaped(username);
		sb.appendHtmlConstant("</span>");
		sb.appendEscaped(string);
		sb.appendHtmlConstant("</td>");
		sb.appendHtmlConstant("</tr>");
		sb.appendHtmlConstant("</table>");

		div.setInnerHTML(sb.toSafeHtml().asString());

		output.getElement().appendChild(div);
	}

	public void updateStatus(String username, Status status) {
		// for (int index = 0; index < contacts.getItemCount(); index++) {
		// if(contacts.getItemText(index).e)
		// }
		// ToDO show cellist with status
		showMessage("", username + "is now " + status.toString(), "green");

	}

	/**
	 * The Cell used to render a student
	 */
	static class StudentCell extends AbstractCell<StudentDTO> {

		@Override
		public void render(Context context, StudentDTO value, SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}
			sb.appendHtmlConstant("<div style=\"border-bottom: 1px solid #ccb;width:150px;\"><table>");
			String imageHTML = "";

			if (value.isDefaultImage()) {
				ImageResource im = resources.defaultIcon();
				imageHTML = "<img src=\"" + im.getSafeUri().asString() + "\" height=\"16px\"/>";
			} else {
				imageHTML = "<img src=\"" + value.getImage() + "\" height=\"16px\"/>";
			}

			sb.appendHtmlConstant("<tr><td rowspan='2' width='50px' align='center'>");
			sb.appendHtmlConstant(imageHTML);
			sb.appendHtmlConstant("</td>");

			// Add the name and date created.
			sb.appendHtmlConstant("<td width='150px' style='font-size:110%; font-weight: bold;padding-left: 2px;'>");
			sb.appendEscaped(value.getName());
			sb.appendHtmlConstant("</td><td style='color:#bbb;'>");
			sb.appendEscaped(value.getStatus().toString());

			sb.appendHtmlConstant("</td></tr>");
			sb.appendHtmlConstant("</table></div>");
		}
	}

	@Override
	public void setImage(EducatorDTO value) {
		String imageHTML = "";

		if (value.isDefaultImage()) {
			ImageResource im = resources.defaultIcon();
			image.setUrl(im.getSafeUri().asString());
		} else {
			image.setUrl("data:image/" + value.getImage().getType() + ";base64," + value.getImage().getImageData());
		}
	}
}
