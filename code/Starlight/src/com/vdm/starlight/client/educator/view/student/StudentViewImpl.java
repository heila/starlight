package com.vdm.starlight.client.educator.view.student;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DateLabel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentViewImpl extends Composite implements StudentView {

	static Logger logger = Logger.getLogger(" WorkbookInfoViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	private static StudentViewUiBinder uiBinder = GWT.create(StudentViewUiBinder.class);
	@UiField
	Label studentFullName;
	@UiField
	DateLabel joinDate;
	@UiField
	DateLabel leftDate;
	@UiField
	Label ageGroup;
	@UiField
	Label password;
	@UiField
	TextArea studentDescription;
	@UiField
	Label studentName;
	@UiField
	Label studentStatus;
	@UiField
	Image imagehtml;

	private Presenter presenter;

	@UiTemplate("StudentView.ui.xml")
	interface StudentViewUiBinder extends UiBinder<Widget, StudentViewImpl> {
	}

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	public StudentViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public StudentViewImpl(String firstName) {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	@Override
	public void setData(StudentDTO student) {
		studentFullName.setText(student.getName());
		joinDate.setValue(student.getStartDate());
		ageGroup.setText(String.valueOf(student.getAge()));
		studentName.setText(student.getUsername());
		if (student.isDefaultImage()) {
			ImageResource im = resources.defaultProfile();
			imagehtml.setUrl(im.getSafeUri());
		} else {
			imagehtml.setUrl("data:image/png;base64," + student.getImage());
		}
		if (imagehtml.getHeight() > imagehtml.getWidth()) {
			imagehtml.setHeight("160px");
		} else {
			imagehtml.setWidth("160px");
		}
	}

	@UiHandler("newLink")
	void onNewLinkClick(ClickEvent event) {
		this.presenter.newLinkClick();

	}

	@UiHandler("listLink")
	void onListLinkClick(ClickEvent event) {
		this.presenter.listLinkClick();

	}

}
