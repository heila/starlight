package com.vdm.starlight.client.educator.view.workbook;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.client.editor.objects.Component;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.StudentDTO;

/**
 * This is the view class that displays the workbook to the educator with the data of the students filled in.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookViewImpl extends Composite implements WorkbookView {
	Logger logger = Logger.getLogger(" WorkbooksView");

	static EducatorResources res = EducatorResources.INSTANCE;

	@UiTemplate("WorkbookView.ui.xml")
	interface WorkbookViewUiBinder extends UiBinder<Widget, WorkbookViewImpl> {
	}

	private static WorkbookViewUiBinder uiBinder = GWT.create(WorkbookViewUiBinder.class);

	/** Navigation bar links */
	@UiField
	Anchor infoLink;
	@UiField
	Anchor designLink;

	/** The text area where the educator can comment on the workpage of the selected student */
	@UiField
	TextArea commentBox;

	/** The pager used to page through the open workbook */
	@UiField
	HorizontalPanel pagerPanel;

	/** Shows the next page in the workbook */
	private final Anchor nextLabel = new Anchor("Next");

	/** Shows the previous page in the workbook */
	private final Anchor prevLabel = new Anchor("Prev");

	/** The panel containing the workpage */
	@UiField
	AbsolutePanel targetPanel;

	/** The list of students assigned the workbook */
	@UiField(provided = true)
	CellList<StudentDTO> studentList;

	/** Selection model for students */
	SingleSelectionModel<StudentDTO> selectionModel;

	/** Label displaying the page number */
	Label pageNumber = new Label("Page 0 of 0");

	Presenter presenter;

	/** Components of the current workpage */
	private List<Component> components = new ArrayList<Component>();

	/**
	 * Default constructor
	 */
	public WorkbookViewImpl() {
		StudentCell cell = new StudentCell();
		studentList = new CellList<StudentDTO>(cell);
		logger.log(Level.INFO, "WorkbookView Constructor");
		initWidget(uiBinder.createAndBindUi(this));
		createPager();
		createStudentList();
		createCommentBox();

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	private void createPager() {
		nextLabel.setStyleName(res.starlightCSS().pagerLabelRight());
		prevLabel.setStyleName(res.starlightCSS().pagerLabelLeft());
		pagerPanel.setHeight("40px");
		pagerPanel.add(prevLabel);
		pagerPanel.add(pageNumber);
		pagerPanel.add(nextLabel);
		nextLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.getNextPage();
			}
		});
		prevLabel.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				presenter.getPreviousPage();
			}
		});
	}

	/**
	 * Creates and displays the list of students
	 */
	public void createStudentList() {
		studentList.setVisibleRange(0, 1000);

		selectionModel = new SingleSelectionModel<StudentDTO>(StudentDTO.KEY_PROVIDER);

		studentList.setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				int id = selectionModel.getSelectedObject().getId();
				logger.log(Level.INFO, "Student id = " + id + " selected.");
				presenter.getStudentData();
			}
		});

	}

	public void createCommentBox() {
		commentBox.addValueChangeHandler(new ValueChangeHandler<String>() {
			@Override
			public void onValueChange(ValueChangeEvent<String> event) {
				logger.log(Level.INFO, "Comment box value changed");
				presenter.saveComment(selectionModel.getSelectedObject().getId(), getComment());

			}
		});
		commentBox.setEnabled(false);
	}

	@Override
	public void viewPage(List<String> widgets) {
		targetPanel.clear();
		components.clear();
		if (widgets.size() != 0) {
			for (String w : widgets) {
				addComponent(w);
			}
		}
	}

	/**
	 * Parses the String parameter into a component and places it on screen
	 * 
	 * @param component
	 */
	public void addComponent(String component) {
		String[] description = component.split("\\|");
		Widget widget = presenter.getComponent(description);
		targetPanel.add(widget);
		((Component) widget).setProperties(description[2]);
		components.add((Component) widget);
	}

	@Override
	public void setNumPages(int currentPage, int totalpages) {
		this.pageNumber.setText("Page " + currentPage + " of " + totalpages);
	}

	/**
	 * The Cell used to render a student
	 */
	static class StudentCell extends AbstractCell<StudentDTO> {

		@Override
		public void render(Context context, StudentDTO value, SafeHtmlBuilder sb) {
			// Value can be null, so do a null check..
			if (value == null) {
				return;
			}
			sb.appendHtmlConstant("<div style=\"border-bottom: 1px solid #ccb;width:150px;\"><table>");
			String imageHTML = "";

			if (value.isDefaultImage()) {
				ImageResource im = res.defaultIcon();
				imageHTML = "<img src=\"" + im.getSafeUri().asString() + "\" height=\"16px\"/>";
			} else {
				imageHTML = "<img src=\"" + value.getImage() + "\" height=\"16px\"/>";
			}

			sb.appendHtmlConstant("<tr><td rowspan='2' width='50px' align='center'>");
			sb.appendHtmlConstant(imageHTML);
			sb.appendHtmlConstant("</td>");

			// Add the name and date created.
			sb.appendHtmlConstant("<td width='150px' style='font-size:110%; font-weight: bold;padding-left: 2px;'>");
			sb.appendEscaped(value.getName());
			sb.appendHtmlConstant("</td></td><td style='color:#bbb;'>");
			sb.appendHtmlConstant("</td></tr>");
			sb.appendHtmlConstant("</table></div>");
		}

	}

	@Override
	public HasData<StudentDTO> getStudentDisplay() {
		return studentList;
	}

	@Override
	public void setStudentData(List<String> dataDescription) {
		if (dataDescription != null && dataDescription.size() > 0) { // no data was filled in yet
			logger.log(Level.INFO, dataDescription.toString());
			for (int i = 0; i < dataDescription.size(); i++) {
				String[] option = dataDescription.get(i).split("\\|");
				int id = Integer.parseInt(option[0].trim());
				if (option.length > 1) {
					String description = option[1];
					Component c = get(id);
					c.call(Property.Input.getName(), description, false); // setdata
				}
			}
		}
		commentBox.setEnabled(true);
	}

	public Component get(int componentId) {
		for (Component c : components) {
			if (c.getId() == componentId) {
				return c;
			}
		}
		return null;
	}

	public void setComment(String comment) {
		this.commentBox.setText(comment);
	}

	public String getComment() {
		return commentBox.getText();
	}

	@UiHandler("infoLink")
	void onInfoLinkClick(ClickEvent event) {
		this.presenter.infoLinkClick();
	}

	@UiHandler("designLink")
	void onDesignLinkClick(ClickEvent event) {
		this.presenter.designLinkClick();
	}

}
