package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.student.StudentsListView;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.events.StudentEvent.StudentEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentsListPresenter implements Presenter, StudentsListView.Presenter {
	Logger logger = Logger.getLogger("ProfessorHomePresenter");

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int educatorId;

	/** View */
	private final StudentsListView view;

	AsyncDataProvider<StudentDTO> dataProvider;

	public StudentsListPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, StudentsListView view,
			final int educatorId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = view;
		this.view.setPresenter(this);
		this.educatorId = educatorId;
		bind();

	}

	public void bind() {
		// Create a data provider.
		dataProvider = new AsyncDataProvider<StudentDTO>() {
			@Override
			protected void onRangeChanged(HasData<StudentDTO> display) {
				final Range range = display.getVisibleRange();
				final int start = range.getStart();
				int end = start + range.getLength();

				dbService.getEducatorStudents(educatorId, start, end, false, new AsyncCallback<List<StudentDTO>>() {
					public void onSuccess(List<StudentDTO> result) {
						if (result.size() == 0) {
							StudentsListPresenter.this.view.getDataDisplay().setVisibleRange(start - 5, start);
						} else
							StudentsListPresenter.this.view.getDataDisplay().setRowData(start, result);
					}

					public void onFailure(Throwable caught) {
						Window.alert("Error fetching class' students details" + caught.getMessage());
					}
				});
			}
		};

		// Connect the list to the data provider.
		dataProvider.addDataDisplay(this.view.getDataDisplay());
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		view.clear(); // clear selection
		container.add(view.asWidget());

	}

	@Override
	public void infoLinkClick(int selectedItem) {
		eventBus.fireEvent(new StudentEvent(StudentEventType.INFO, selectedItem));
	}

	@Override
	public void newLinkClick() {
		eventBus.fireEvent(new StudentEvent(StudentEventType.NEW, -1));
	}

}
