package com.vdm.starlight.client.educator.view.classroom;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomsListViewImpl extends Composite implements ClassroomsListView {
	static Logger logger = Logger.getLogger("ClassroomsListViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("ClassroomsListView.ui.xml")
	interface ClassroomsListViewUiBinder extends UiBinder<Widget, ClassroomsListViewImpl> {
	}

	private static ClassroomsListViewUiBinder uiBinder = GWT.create(ClassroomsListViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of ClassroomDTO objects */
	@UiField
	ClassroomList classroomList;

	@UiField
	Anchor newLink;

	private Presenter presenter;

	public ClassroomsListViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));

		// Add a selection model so we can select cells.
		final SingleSelectionModel<ClassroomDTO> selectionModel = new SingleSelectionModel<ClassroomDTO>(
				ClassroomDTO.KEY_PROVIDER);

		classroomList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				ClassroomDTO classroom = selectionModel.getSelectedObject();
				if (classroom != null)
					presenter.infoLinkClick(selectionModel.getSelectedObject().getId());
			}
		});
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget implementing HasData which displays the list of classrooms
	 */
	public HasData<ClassroomDTO> getDataDisplay() {
		return this.classroomList.getDataDisplay();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		SingleSelectionModel<ClassroomDTO> selectModel = (SingleSelectionModel<ClassroomDTO>) this.classroomList
				.getDataDisplay().getSelectionModel();
		if (selectModel.getSelectedObject() != null)
			selectModel.setSelected(selectModel.getSelectedObject(), false);
	}

	@UiHandler("newLink")
	void onNewLinkClick(ClickEvent event) {
		this.presenter.newLinkClick();

	}

}
