package com.vdm.starlight.client.educator.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.EducatorResources;

/**
 * Home view of the educator. Contains the menubar and manages all change of content on the page.
 * 
 * @author Heila van der Merwe
 * 
 */
public class EducatorHomeViewImpl extends Composite implements EducatorHomeView {

	@UiTemplate("EducatorHomeView.ui.xml")
	interface EducatorHomeViewUiBinder extends UiBinder<Widget, EducatorHomeViewImpl> {
	}

	static EducatorResources resources = EducatorResources.INSTANCE;

	private static EducatorHomeViewUiBinder uiBinder = GWT.create(EducatorHomeViewUiBinder.class);

	static {
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	@UiField
	Anchor workbooksItem;
	@UiField
	Anchor classroomItem;
	@UiField
	Anchor studentsItem;
	@UiField
	Anchor settingsItem;
	@UiField
	Anchor mediaItem;

	@UiField
	Label messageItem;

	@UiField
	SimplePanel pagePanel;

	private Presenter presenter;

	public EducatorHomeViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		messageItem.setVisible(true);

	}

	public void bind() {

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		bind();
	}

	public HasWidgets getPageContainer() {
		return pagePanel;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@UiHandler("workbooksItem")
	void onWorkbooksItemClick(ClickEvent event) {
		presenter.workbooksItemClicked();
	}

	@UiHandler("classroomItem")
	void onClassroomItemClick(ClickEvent event) {
		presenter.classroomsItemClicked();
	}

	@UiHandler("studentsItem")
	void onStudentsItemClick(ClickEvent event) {
		presenter.studentsItemClicked();
	}

	@UiHandler("mediaItem")
	void onMediaItemClick(ClickEvent event) {
		presenter.mediaItemClicked();
	}

	@UiHandler("settingsItem")
	void onSettingsItemClick(ClickEvent event) {
		presenter.settingsItemClicked();
	}

	@UiHandler("logoutItem")
	void onLogoutItemClick(ClickEvent event) {
		presenter.logoutItemClicked();
	}

	@Override
	public void newMessage(boolean show) {
		if (show)
			messageItem.setStyleName(resources.starlightCSS().newMessage());
		else
			messageItem.setStyleName(resources.starlightCSS().noNewMessage());

	}
}
