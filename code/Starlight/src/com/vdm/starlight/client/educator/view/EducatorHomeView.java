package com.vdm.starlight.client.educator.view;

import com.google.gwt.user.client.ui.Widget;

public interface EducatorHomeView {

	public interface Presenter {
		public void workbooksItemClicked();

		public void classroomsItemClicked();

		public void studentsItemClicked();

		public void settingsItemClicked();

		public void logoutItemClicked();

		public void mediaItemClicked();
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void newMessage(boolean show);
}