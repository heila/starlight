package com.vdm.starlight.client.educator.view.workbook;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.cell.client.AbstractCell;
import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.safehtml.shared.SafeHtmlUtils;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Tree;
import com.google.gwt.view.client.AbstractDataProvider;
import com.google.gwt.view.client.ListDataProvider;
import com.google.gwt.view.client.SelectionModel;
import com.google.gwt.view.client.TreeViewModel;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;

public class TreeModel implements TreeViewModel {
	public interface Images extends Tree.Resources {
		ImageResource workpage();

		ImageResource book();
	}

	private static Images images;
	private final SelectionModel<WorkpageDTO> selectionModel;

	WorkbookPagesDTO workbook;

	public TreeModel(WorkbookPagesDTO workbookpages, final SelectionModel<WorkpageDTO> selectionModel) {
		this.selectionModel = selectionModel;
		if (images == null) {
			images = GWT.create(Images.class);
		}
		this.workbook = workbookpages;
	}

	public void addWorkpageInfo(WorkpageDTO workpage) {
		this.workbook.getPages().add(workpage);
	}

	@Override
	public <T> NodeInfo<?> getNodeInfo(T value) {
		if (value == null) {
			// Return top level categories.
			List<WorkbookPagesDTO> workbookList = new ArrayList<WorkbookPagesDTO>();
			workbookList.add(workbook);
			AbstractDataProvider<WorkbookPagesDTO> workbooks = new ListDataProvider<WorkbookPagesDTO>(workbookList);
			return new DefaultNodeInfo<WorkbookPagesDTO>(workbooks, new AbstractCell<WorkbookPagesDTO>() {
				@Override
				public void render(com.google.gwt.cell.client.Cell.Context context, WorkbookPagesDTO value,
						SafeHtmlBuilder sb) {
					sb.append(SafeHtmlUtils.fromTrustedString(AbstractImagePrototype.create(images.book()).getHTML()));
					sb.appendEscaped(" ").appendEscaped(value.getName());
				}
			});
		} else if (value instanceof WorkbookPagesDTO) {
			// Return the first letters of each first name.
			WorkbookPagesDTO category = (WorkbookPagesDTO) value;
			List<WorkpageDTO> workpages = ((WorkbookPagesDTO) value).getPages();

			return new DefaultNodeInfo<WorkpageDTO>(new ListDataProvider<WorkpageDTO>(workpages),
					new AbstractCell<WorkpageDTO>() {

						@Override
						public void render(com.google.gwt.cell.client.Cell.Context context, WorkpageDTO value,
								SafeHtmlBuilder sb) {
							sb.append(SafeHtmlUtils.fromTrustedString(AbstractImagePrototype.create(images.workpage())
									.getHTML()));
							sb.appendEscaped(" ").appendEscaped(value.getName());
						}
					}, selectionModel, null);
		}

		// Unhandled type.
		String type = value.getClass().getName();
		throw new IllegalArgumentException("Unsupported object type: " + type);
	}

	@Override
	public boolean isLeaf(Object value) {
		return value instanceof WorkpageDTO;
	}
}