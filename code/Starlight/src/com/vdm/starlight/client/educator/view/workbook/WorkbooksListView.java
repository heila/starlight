package com.vdm.starlight.client.educator.view.workbook;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public interface WorkbooksListView {

	public interface Presenter {

		void infoLinkClick(int selectedItem);

		void newLinkClick();
	}

	void setPresenter(Presenter presenter);

	public HasData<WorkbookDTO> getDataDisplay();

	Widget asWidget();

	public void clear();
}