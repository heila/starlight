package com.vdm.starlight.client.educator.view.workbook;

import java.util.List;

import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.vdm.starlight.shared.dto.StudentDTO;

public interface WorkbookView {

	public interface Presenter {
		void designLinkClick();

		void studentLinkClick();

		void infoLinkClick();

		Widget getComponent(String[] description);

		void getPreviousPage();

		void getNextPage();

		void saveComment(int id, String text);

		void getStudentData();

	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	HasData<StudentDTO> getStudentDisplay();

	void setStudentData(List<String> dataDescription);

	void setComment(String comment);

	String getComment();

	void setNumPages(int totalpages, int currentPage);

	void viewPage(List<String> widgets);
}