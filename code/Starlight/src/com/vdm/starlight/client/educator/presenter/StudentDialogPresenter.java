package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.HasImage;
import com.vdm.starlight.client.editor.ImagePickerViewImpl;
import com.vdm.starlight.client.educator.view.media.MediaGridView;
import com.vdm.starlight.client.educator.view.student.StudentDialogView;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentDialogPresenter implements Presenter, StudentDialogView.Presenter {

	Logger logger = Logger.getLogger(" StudentDialogPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final StudentDialogView view;

	int studentID;

	MediaGridView imagePickerView;

	public StudentDialogPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			StudentDialogView studentDialogView, int studentID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = studentDialogView;
		this.view.setPresenter(this);
		this.studentID = studentID;

	}

	@Override
	public void go(HasWidgets container) {

		container.clear();
		container.add(view.asWidget());

	}

	@Override
	public void createStudent(final String[] text, final int imageID) {
		int age = 0;
		try {
			age = Integer.parseInt(text[4]);
		} catch (Exception e) {
		}
		dbService.createStudent(text[0], text[1], text[2], text[3], age, imageID, new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error creating student: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Integer result) {
				StudentDialogPresenter.this.view.studentCreatedResult(text[0], result.intValue());
			}
		});

	}

	@Override
	public void studentsLinkClick() {
		if (eventBus != null) {
			eventBus.fireEvent(new StudentEvent(StudentEvent.StudentEventType.STUDENTS, 0));
		}
	}

	@Override
	public void showImagePicker() {
		boolean loadImages = false;
		if (imagePickerView == null) {
			imagePickerView = new ImagePickerViewImpl();
			loadImages = true;
		}
		MediaGridPresenter pres = new MediaGridPresenter(dbService, imagePickerView, (HasImage) this.view);
		if (loadImages) {
			pres.go(null);
		}

		imagePickerView.asWidget();

	}

	@Override
	public void getStudentInfo(int studentID) {
		// dbService.getWorkbookBasicInfo(studentID, new AsyncCallback<StudentDetailInfo>() {
		//
		// @Override
		// public void onFailure(Throwable caught) {
		// Window.alert("Could not retrieve workbook information");
		//
		// }
		//
		// @Override
		// public void onSuccess(StudentDetailInfo result) {
		// view.setStudentInfo(result);
		//
		// }
		// });
	}

	@Override
	public void updateStudentInfo(final StudentDTO s, int imageID) {
		// dbService.saveStudentInfo(s, imageID, new AsyncCallback<Void>() {
		//
		// public void onFailure(Throwable caught) {
		// Window.alert("Error updating student: " + caught.getMessage());
		// }
		//
		// @Override
		// public void onSuccess(Void result) {
		// ((WorkbookEditDialogViewImpl) view).editWorkbookDialog.hide();
		// eventBus.fireEvent(new WorkbookEvent(WorkbookEventType.INFO, w.getId()));
		// }
		// });

	}

}