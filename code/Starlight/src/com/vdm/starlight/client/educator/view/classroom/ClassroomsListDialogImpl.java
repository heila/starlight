package com.vdm.starlight.client.educator.view.classroom;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomsListDialogImpl extends Composite implements ClassroomsListView {
	static Logger logger = Logger.getLogger("ClassroomsListViewImpl");

	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("ClassroomsListDialog.ui.xml")
	interface ClassroomsListDialogUiBinder extends UiBinder<Widget, ClassroomsListDialogImpl> {
	}

	private static ClassroomsListDialogUiBinder uiBinder = GWT.create(ClassroomsListDialogUiBinder.class);

	/** CellList of ClassroomDTO objects */
	@UiField
	ClassroomList classroomList;

	private Presenter presenter;
	@UiField
	DialogBox classroomListDialog;

	public ClassroomsListDialogImpl() {

		uiBinder.createAndBindUi(this);
		classroomListDialog.getElement().getStyle().setZIndex(200);
		classroomListDialog.show();
		// Add a selection model so we can select cells.
		final SingleSelectionModel<ClassroomDTO> selectionModel = new SingleSelectionModel<ClassroomDTO>(
				ClassroomDTO.KEY_PROVIDER);

		classroomList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				// presenter.infoLinkClick(selectionModel.getSelectedObject().getId());
			}
		});
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		classroomListDialog.show();

	}

	/**
	 * @return The widget implementing HasData which displays the list of classrooms
	 */
	public HasData<ClassroomDTO> getDataDisplay() {
		return this.classroomList.getDataDisplay();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		SingleSelectionModel<ClassroomDTO> selectModel = (SingleSelectionModel<ClassroomDTO>) this.classroomList
				.getDataDisplay().getSelectionModel();
		if (selectModel.getSelectedObject() != null)
			selectModel.setSelected(selectModel.getSelectedObject(), false);
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@UiHandler("addWorkbookButton")
	void onAddWorkbookButtonClick(ClickEvent event) {
		SingleSelectionModel<ClassroomDTO> selectModel = (SingleSelectionModel<ClassroomDTO>) this.classroomList
				.getDataDisplay().getSelectionModel();
		if (selectModel.getSelectedObject() != null) {
			this.presenter.addWorkbookToClassroom((selectModel.getSelectedObject().getId()));
		}
		this.classroomListDialog.hide();
	}

	@UiHandler("cancelButton")
	void onCancelButtonClick(ClickEvent event) {
		Window.alert("Workbook not added to classroom, request canceled");
		this.classroomListDialog.hide();

	}

}
