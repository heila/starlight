package com.vdm.starlight.client.educator.view.workbook;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public class WorkbooksListViewImpl extends Composite implements WorkbooksListView {
	static Logger logger = Logger.getLogger("WorkbooksListViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	@UiTemplate("WorkbooksListView.ui.xml")
	interface WorkbooksListViewUiBinder extends UiBinder<Widget, WorkbooksListViewImpl> {
	}

	private static WorkbooksListViewUiBinder uiBinder = GWT.create(WorkbooksListViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of workbook objects */
	@UiField
	WorkbookList workbookList;

	private Presenter presenter;

	@UiField
	Anchor newLink;

	public WorkbooksListViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));

		// Add a selection model so we can select cells.
		final SingleSelectionModel<WorkbookDTO> selectionModel = new SingleSelectionModel<WorkbookDTO>(
				WorkbookDTO.KEY_PROVIDER);

		workbookList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				if (selectionModel.getSelectedObject() != null)
					presenter.infoLinkClick(selectionModel.getSelectedObject().getId());
			}
		});

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget implementing HasData which displays the list of workbooks
	 */
	public HasData<WorkbookDTO> getDataDisplay() {
		return this.workbookList.getDataDisplay();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		SingleSelectionModel<WorkbookDTO> selectModel = (SingleSelectionModel<WorkbookDTO>) this.workbookList
				.getDataDisplay().getSelectionModel();
		if (selectModel.getSelectedObject() != null)
			selectModel.setSelected(selectModel.getSelectedObject(), false);
	}

	@UiHandler("newLink")
	void onNewLinkClick(ClickEvent event) {
		this.presenter.newLinkClick();
	}
}
