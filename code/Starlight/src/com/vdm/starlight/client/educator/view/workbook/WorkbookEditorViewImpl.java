package com.vdm.starlight.client.educator.view.workbook;

import java.util.ArrayList;
import java.util.List;

import com.allen_sauer.gwt.dnd.client.DragEndEvent;
import com.allen_sauer.gwt.dnd.client.DragHandler;
import com.allen_sauer.gwt.dnd.client.DragStartEvent;
import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.allen_sauer.gwt.dnd.client.VetoDragException;
import com.allen_sauer.gwt.dnd.client.drop.AbsolutePositionDropController;
import com.allen_sauer.gwt.dnd.client.drop.DropController;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.NativeEvent;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellTree;
import com.google.gwt.user.cellview.client.TreeNode;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.MenuItem;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.PalletLabel;
import com.vdm.starlight.client.editor.objects.CheckBoxObject;
import com.vdm.starlight.client.editor.objects.Component;
import com.vdm.starlight.client.editor.objects.RadioBoxObject;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.objects.Workbook;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Renders and manages the interface of the {@link Workbook} editor view.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookEditorViewImpl extends Composite implements WorkbookEditorView {
	@UiTemplate("WorkbookEditorView.ui.xml")
	interface WorkbookEditorViewUiBinder extends UiBinder<Widget, WorkbookEditorViewImpl> {
	}

	private static WorkbookEditorViewUiBinder uiBinder = GWT.create(WorkbookEditorViewUiBinder.class);
	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** Navigation bar links */
	@UiField
	Anchor infoLink;
	@UiField
	Anchor viewLink;

	/** The scrollpanel on which the workpageTree is rendered */
	@UiField
	ScrollPanel treePanel;

	/** The panel on which the list of components are rendered */
	@UiField
	VerticalPanel componentList;

	@UiField
	AbsolutePanel targetPanel;

	/** The grid displaying a list of properties */
	@UiField
	FlexTable propertyGrid;

	/** Menu bas items */
	@UiField
	MenuItem saveMenuItem;
	@UiField
	MenuItem cutMenuItem;
	@UiField
	MenuItem copyMenuItem;
	@UiField
	MenuItem pasteMenuItem;
	@UiField
	MenuItem deleteMenuItem;
	@UiField
	MenuItem newMenuItem;
	@UiField
	MenuItem backgroundItem;

	/** Refreshes the workpage */
	@UiField
	Button applyButton;

	/** Disables the delete button */
	public static boolean isModal = false;

	private Presenter presenter;

	PickupDragController dragController;

	DropController dropController;

	SingleSelectionModel<WorkpageDTO> selectionModel;
	CellTree workpageTree;
	TreeModel model;

	Workpage backupWorkpage;
	List<String> copyComponents = new ArrayList<String>();

	public WorkbookEditorViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));

		createWorkbookPanel();
		createComponentPallet();
		createMenuBar();
	}

	@Override
	public void setPresenter(Presenter presenter) {
		backupWorkpage = null;
		copyComponents = null;
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public void createMenuBar() {
		saveMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				WorkpageDTO w = selectionModel.getSelectedObject();
				presenter.savePage(w.getId(), getPageDescription(), w.getUniqueComponentId());
				Window.alert("Saved");
			}
		});
		deleteMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				Iterable<Widget> selectedWidgets = dragController.getSelectedWidgets();
				for (Widget widget : selectedWidgets) {
					widget.removeFromParent();
				}
				propertyGrid.removeAllRows();
			}
		});
		copyMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				if (copyComponents != null) {
					for (String s : copyComponents)
						copyComponents.remove(s);
				} else
					copyComponents = new ArrayList<String>();
				Iterable<Widget> selectedWidgets = dragController.getSelectedWidgets();
				for (Widget widget : selectedWidgets) {
					copyComponents.add(((Component) widget).toString());
				}
			}
		});
		cutMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				for (String s : copyComponents)
					copyComponents.remove(s);
				Iterable<Widget> selectedWidgets = dragController.getSelectedWidgets();
				for (Widget widget : selectedWidgets) {
					copyComponents.add(((Component) widget).toString());
				}
				for (Widget widget : selectedWidgets) {
					widget.removeFromParent();
				}
				propertyGrid.removeAllRows();

			}
		});
		pasteMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				for (String c : copyComponents) {
					addComponent(c);
				}
			}
		});
		newMenuItem.setCommand(new Command() {

			@Override
			public void execute() {
				WorkpageDTO w = selectionModel.getSelectedObject();
				if (w != null)
					presenter.savePage(w.getId(), getPageDescription(), w.getUniqueComponentId());
				isModal = true;
				NameDialog dia = new NameDialog();
				dia.center();

			}
		});

	}

	/**
	 * Create the tree of workbook pages
	 */
	public void createWorkPageTree(WorkbookPagesDTO pages) {
		selectionModel = new SingleSelectionModel<WorkpageDTO>();
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {

			public void onSelectionChange(SelectionChangeEvent event) {
				if (!targetPanel.isVisible()) {
					targetPanel.setVisible(true);
				}
				// save previous page
				if (backupWorkpage != null) {
					presenter.savePage(backupWorkpage.getId(), getPageDescription(),
							backupWorkpage.getUniqueComponentId());
				}
				// load next page
				presenter.workpageClick(selectionModel.getSelectedObject().getId());
				if (backupWorkpage == null) {
					backupWorkpage = new Workpage();
				}
				// save next page
				backupWorkpage.setId(selectionModel.getSelectedObject().getId());
				backupWorkpage.setUniqueComponentId(selectionModel.getSelectedObject().getUniqueComponentId());
				propertyGrid.clear();
			}
		});

		model = new TreeModel(pages, selectionModel);
		treePanel.clear();
		workpageTree = new CellTree(model, null);
		workpageTree.setAnimationEnabled(true);

		// Open the first playlist by default.
		TreeNode rootNode = workpageTree.getRootTreeNode();
		TreeNode firstPlaylist = rootNode.setChildOpen(0, true);
		if (pages.getPages().size() > 0) {
			TreeNode node = firstPlaylist.setChildOpen(0, true);
			selectionModel.setSelected(pages.getPages().get(0), true);
		} else
			targetPanel.setVisible(false);
		treePanel.add(workpageTree);

	}

	private void createWorkbookPanel() {
		targetPanel.getElement().getStyle().setOverflow(Overflow.HIDDEN);
		// Create a DragController for each logical area where a set of draggable
		// widgets and drop targets will be allowed to interact with one another.
		dragController = new PickupDragController(targetPanel, true);

		// Positioner is always constrained to the boundary panel
		// Use 'true' to also constrain the draggable or drag proxy to the boundary panel
		dragController.setBehaviorConstrainedToBoundaryPanel(true);

		// Allow multiple widgets to be selected at once using CTRL-click
		dragController.setBehaviorMultipleSelection(true);

		// create a DropController for each drop target on which draggable widgets
		// can be dropped
		dropController = new AbsolutePositionDropController(targetPanel);

		// Don't forget to register each DropController with a DragController
		dragController.registerDropController(dropController);

		// Sets the property list when an item is selected
		dragController.addDragHandler(new DragHandler() {
			@Override
			public void onPreviewDragStart(DragStartEvent event) throws VetoDragException {
				propertyGrid.removeAllRows();
				setPropertiesPanel(((Component) event.getSource()).getProperties());
			}

			@Override
			public void onPreviewDragEnd(DragEndEvent event) throws VetoDragException {
				Widget w = ((Widget) event.getSource());

				if (w.getOffsetWidth() > 952) {
					w.setWidth("950px");

				}

				if (w.getOffsetHeight() > 602) {
					w.setHeight("600px");

				}
			}

			@Override
			public void onDragStart(DragStartEvent event) {
				Widget w = ((Widget) event.getSource());
				if (w.getOffsetWidth() > 952) {
					w.setWidth("949px");

				}

				if (w.getOffsetHeight() > 602) {
					w.setHeight("600px");

				}
			}

			@Override
			public void onDragEnd(DragEndEvent event) {
				Widget w = ((Widget) event.getSource());
				if (w.getOffsetWidth() >= 950) {
					w.setWidth("949px");

				}

				if (w.getOffsetHeight() > 600) {
					w.setHeight("600px");

				}
			}
		});

		// Listens for the delete button presses and then deletes all selected items on the workpage.
		Event.addNativePreviewHandler(new Event.NativePreviewHandler() {
			public void onPreviewNativeEvent(NativePreviewEvent event) {
				NativeEvent ne = event.getNativeEvent();
				if (ne.getKeyCode() == KeyCodes.KEY_DELETE && isModal == false) {
					Iterable<Widget> selectedWidgets = dragController.getSelectedWidgets();
					for (Widget widget : selectedWidgets) {
						widget.removeFromParent();
					}
					propertyGrid.removeAllRows();
				}
			}
		});

		// Clear the current selection on the workpage when the target panel is pressed.
		targetPanel.addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dragController.clearSelection();
				propertyGrid.removeAllRows();
			}
		}, ClickEvent.getType());
		// Clear the current selection on the workpage when the target panel is pressed.
		targetPanel.getParent().addDomHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				dragController.clearSelection();
				propertyGrid.removeAllRows();
			}
		}, ClickEvent.getType());
	}

	/**
	 * Adds all components to the component panel.
	 */
	public void createComponentPallet() {
		for (ComponentType e : ComponentType.values()) {
			Widget w = new PalletLabel(e, targetPanel, dragController);
			componentList.add(w);
		}
	}

	/**
	 * Adds all the properties to the property panel.
	 * 
	 * @param properties
	 *            the list of property-value pairs to show in the property list
	 */
	private void setPropertiesPanel(final String[][] properties) {
		for (int row = 0; row < properties.length; row++) {
			String property = properties[row][0]; // property name
			String value = properties[row][1]; // property value
			Widget w = this.presenter.getWidget(property, value, dragController);

			if (w != null)
				propertyGrid.setWidget(row, 1, w);

		}
	}

	@UiHandler("infoLink")
	void onInfoLinkClick(ClickEvent event) {
		this.presenter.infoLinkClick();
	}

	@UiHandler("viewLink")
	void onViewLinkClick(ClickEvent event) {
		this.presenter.viewLinkClick();
	}

	/**
	 * Pulls string description of the current work page from the screen.
	 * 
	 * @return
	 */
	public String getPageDescription() {
		String description = "";
		for (int i = 0; i < targetPanel.getWidgetCount(); i++) {
			description += "$" + targetPanel.getWidget(i);
		}
		description = description + "$";
		return description;
	}

	/**
	 * @param newPage
	 *            represents a new page to be added to the workbook
	 */
	@Override
	public void createPage(WorkpageDTO newPage) {
		model.addWorkpageInfo(newPage);
		selectionModel.setSelected(newPage, true);
		TreeNode rootNode = workpageTree.getRootTreeNode();
		rootNode.setChildOpen(0, false);
		TreeNode firstPlaylist = rootNode.setChildOpen(0, true);
		TreeNode node = firstPlaylist.setChildOpen(0, true);

	}

	@Override
	public void viewPage(List<String> widgets) {
		targetPanel.clear();
		if (widgets.size() != 0) {
			for (String w : widgets) {
				addComponent(w);
			}
		}
		for (int i = 0; i < componentList.getWidgetCount(); i++) {
			((PalletLabel) componentList.getWidget(i)).changePage(selectionModel.getSelectedObject(), backupWorkpage);
		}
	}

	/**
	 * Parses the String parameter into a component and places it onscreen
	 * 
	 * @param component
	 */
	public void addComponent(String component) {
		String[] description = component.split("\\|");
		Widget widget = presenter.getComponent(description);
		targetPanel.add(widget);
		((Component) widget).setProperties(description[2]);
		if (widget instanceof RadioBoxObject || widget instanceof CheckBoxObject)
			dragController.makeDraggable(widget, ((Component) widget).getDragHandle());
		else
			dragController.makeDraggable(widget);
	}

	public class NameDialog extends DialogBox {
		public NameDialog() {
			// Set the dialog box's caption.
			VerticalPanel panel2 = new VerticalPanel();
			setText("Please enter name for new page:");
			NameDialog.this.getElement().getStyle().setZIndex(200);
			Label l = new Label("Name");
			final TextBox nameBox = new TextBox();
			panel2.add(l);
			panel2.add(nameBox);
			Button ok = new Button("OK");
			ok.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					String text = nameBox.getText();
					if (text.length() > 0) {
						presenter.newPage(text);
						NameDialog.this.hide();
					}
					isModal = false;
				}
			});
			Button cancel = new Button("Cancel");
			cancel.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					NameDialog.this.hide();
					isModal = false;
				}
			});

			HorizontalPanel panel = new HorizontalPanel();
			panel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			panel.add(ok);
			panel.add(cancel);
			panel.setWidth("100%");
			panel2.add(panel);
			setWidget(panel2);
		}
	}

	@UiHandler("applyButton")
	void onApplyButtonClick(ClickEvent event) {
		if (dragController != null) {
			Iterable<Widget> selectedWidgets = dragController.getSelectedWidgets();
			if (selectedWidgets != null) {
				for (Widget widget : selectedWidgets) {
					setPropertiesPanel(((Component) widget).getProperties());
					break;
				}
			}
		}
	}

}
