package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.allen_sauer.gwt.dnd.client.PickupDragController;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.client.educator.view.workbook.WorkbookEditorView;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookEditorPresenter implements Presenter, WorkbookEditorView.Presenter {
	Logger logger = Logger.getLogger(" WorkbooksListPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The id of the workbook */
	private int workbookId;

	/** View */
	private final WorkbookEditorView view;

	int pageNumber = 0;

	public WorkbookEditorPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			WorkbookEditorView workbookEditorView, int workbookId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookEditorView;
		this.view.setPresenter(this);
		this.workbookId = workbookId;
		bind();
	}

	public void savePage(int workpageID, String description, int uniqueID) {
		dbService.saveWorkpageDescription(workpageID, description, uniqueID, new AsyncCallback<Void>() {
			public void onSuccess(Void result) {
			}

			public void onFailure(Throwable caught) {
				Window.alert("Could not save");
			}
		});

	}

	public void bind() {
		dbService.getWorkpages(workbookId, new AsyncCallback<WorkbookPagesDTO>() {
			public void onSuccess(WorkbookPagesDTO result) {
				WorkbookEditorPresenter.this.view.createWorkPageTree(result);
				// set largest PageNumber
				for (WorkpageDTO w : result.getPages()) {
					if (w.getPageNumber() > pageNumber)
						pageNumber = w.getPageNumber();
				}

			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' workpages list" + caught.getMessage());
			}
		});

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());

	}

	@Override
	public void infoLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.INFO, workbookId));

	}

	@Override
	public void studentLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.STUDENTS, workbookId));
	}

	@Override
	public void viewLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.VIEW, workbookId));
	}

	@Override
	public void workpageClick(int id) {
		dbService.getWorkpageData(id, new AsyncCallback<List<String>>() {
			public void onSuccess(List<String> result) {
				WorkbookEditorPresenter.this.view.viewPage(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' workpages list" + caught.getMessage());
			}
		});
	}

	@Override
	public void newPage(String name) {
		pageNumber++;
		dbService.getNewWorkpage(name, workbookId, pageNumber, new AsyncCallback<WorkpageDTO>() {
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' workpages list" + caught.getMessage());
			}

			@Override
			public void onSuccess(WorkpageDTO result) {
				view.createPage(result);
			}
		});
	}

	@Override
	public Widget getWidget(String property, String value, PickupDragController dragController) {
		// creates a widget rendering the property
		Property.dbService = dbService;
		return Property.getPropertyWidget(property, value, dragController);

	}

	public Widget getComponent(String[] description) {
		ComponentType t = ComponentType.getCompoType(description[0]);
		return t.getEducatorWidget(Integer.parseInt(description[1]), dbService);
	}

}
