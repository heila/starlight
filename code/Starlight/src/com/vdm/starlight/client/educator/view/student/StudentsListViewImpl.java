package com.vdm.starlight.client.educator.view.student;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentsListViewImpl extends Composite implements StudentsListView {
	static Logger logger = Logger.getLogger("StudentsListViewImpl");

	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("StudentsListView.ui.xml")
	interface StudentsListViewUiBinder extends UiBinder<Widget, StudentsListViewImpl> {
	}

	private static StudentsListViewUiBinder uiBinder = GWT.create(StudentsListViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of ClassroomInfo objects */
	@UiField
	StudentsList studentList;

	@UiField
	Anchor newLink;

	private Presenter presenter;

	public StudentsListViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));

		// Add a selection model so we can select cells.
		final SingleSelectionModel<StudentDTO> selectionModel = new SingleSelectionModel<StudentDTO>(
				StudentDTO.KEY_PROVIDER);

		studentList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				StudentDTO sinfo = selectionModel.getSelectedObject();
				if (sinfo != null)
					presenter.infoLinkClick(sinfo.getId());
			}
		});
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget implementing HasData which displays the list of classrooms
	 */
	public HasData<StudentDTO> getDataDisplay() {
		return this.studentList.getDataDisplay();
	}

	@UiHandler("newLink")
	void onNewLinkClick(ClickEvent event) {
		this.presenter.newLinkClick();

	}

	public void clear() {
		SingleSelectionModel<StudentDTO> selectModel = (SingleSelectionModel<StudentDTO>) this.studentList
				.getDataDisplay().getSelectionModel();
		if (selectModel.getSelectedObject() != null)
			selectModel.setSelected(selectModel.getSelectedObject(), false);
	}
}
