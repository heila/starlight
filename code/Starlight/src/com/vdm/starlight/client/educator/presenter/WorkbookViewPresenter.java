package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.educator.view.workbook.WorkbookView;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkpageDataDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookViewPresenter implements Presenter, WorkbookView.Presenter {
	Logger logger = Logger.getLogger(" WorkbooksListPresenter");

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int workbookId;

	/** View */
	private final WorkbookView view;
	/** The unique workpage id */
	private int workpageId;

	/** The relative workpage number in the workbook */
	private int pagenumber;

	/** Total number of workpages */
	private int numPages;

	public WorkbookViewPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, WorkbookView workbookView,
			int workbookId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookView;
		this.view.setPresenter(this);
		this.workbookId = workbookId;

	}

	public void bind() {
		this.pagenumber = 1;
		getStudents();
		getNumberOfPages();
		view.getStudentDisplay().setVisibleRangeAndClearData(new Range(0, 20), true);
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		bind();
	}

	@Override
	public void infoLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.INFO, workbookId));
	}

	@Override
	public void designLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.EDITOR, workbookId));
	}

	@Override
	public void studentLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.STUDENTS, workbookId));
	}

	/**
	 * Retrieves the number of workpages in the workbook.
	 */
	private void getNumberOfPages() {
		dbService.getNumberOfPages(workbookId, new AsyncCallback<Integer>() {
			public void onSuccess(Integer result) {
				numPages = result.intValue();

				if (numPages > 0)
					getWorkpage();
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching number of pages" + caught.getMessage());
			}
		});
	}

	public void getWorkpage() {
		workpageId = 0;
		dbService.getWorkpage(workbookId, pagenumber, new AsyncCallback<WorkpageDataDTO>() {

			public void onSuccess(WorkpageDataDTO result) {
				if (result != null) { // the workpage does not exits
					view.setNumPages(pagenumber, numPages);
					workpageId = result.getWorkpageId();

					if (result.getData() != null) { // The workpage contains components
						view.viewPage(result.getData());
					}
					getStudentData();
				}
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching workpage" + caught.getMessage());
			}
		});
	}

	public void getStudents() {
		// Create a data provider.
		dbService.getWorkbookStudents(workbookId, false, new AsyncCallback<List<StudentDTO>>() {
			public void onSuccess(List<StudentDTO> result) {
				if (result.size() > 0) {
					view.getStudentDisplay().setRowData(0, result);
					view.getStudentDisplay().getSelectionModel()
							.setSelected(view.getStudentDisplay().getVisibleItem(0), true);
				}
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' students details" + caught.getMessage());
			}
		});
	}

	public Widget getComponent(String[] description) {
		ComponentType t = ComponentType.getCompoType(description[0]);
		return t.getStudentWidget(Integer.parseInt(description[1]), dbService);
	}

	@Override
	public void getNextPage() {
		if (pagenumber < numPages) {
			pagenumber += 1;
			getWorkpage();
		} else
			Window.alert("No next pages");

	}

	@Override
	public void getPreviousPage() {
		if (pagenumber > 1) {
			pagenumber -= 1;
			getWorkpage();
		} else
			Window.alert("No previous pages");
	}

	/**
	 * Retrieves the data of the selected student and displays it in the view.
	 */
	@Override
	public void getStudentData() {

		/* If a student is selected */
		@SuppressWarnings("unchecked")
		final StudentDTO student = ((SingleSelectionModel<StudentDTO>) view.getStudentDisplay().getSelectionModel())
				.getSelectedObject();
		if (student == null || workpageId <= 0 || numPages <= 0) {
			// TODO disable commentbox
			view.setComment("");
			return;
		}

		dbService.getWorkpageStudentData(student.getId(), workpageId, new AsyncCallback<List<String>>() {
			@Override
			public void onFailure(Throwable caught) {
				logger.log(Level.INFO, "Could not retrieve StudentWorkpageData for student id=" + student.getId()
						+ " and workpage id=" + workpageId);
			}

			@Override
			public void onSuccess(List<String> result) {
				logger.log(Level.INFO, "Retrieved StudentWorkpageData for student id=" + student.getId()
						+ " and workpage id=" + workpageId + " data:\"" + result + "\"");
				view.setStudentData(result);

			}
		});
		dbService.getWorkpageComment(student.getId(), workpageId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {
				logger.log(Level.INFO, "Could not retrieve comment for student id=" + student.getId()
						+ " and workpage id=" + workpageId);
			}

			@Override
			public void onSuccess(String comment) {
				logger.log(Level.INFO, "Retrieved comment for student id=" + student.getId() + " and workpage id="
						+ workpageId + " comment: \"" + comment + "\"");
				view.setComment(comment);

			}
		});
	}

	@Override
	public void saveComment(final int studentId, String text) {
		final String comment = view.getComment();
		dbService.setWorkpageComment(studentId, workpageId, comment, new AsyncCallback<Void>() {
			@Override
			public void onFailure(Throwable caught) {
				logger.log(Level.INFO, "Could nto save comment for student id=" + studentId + " and workpage id="
						+ workpageId + " comment: \"" + comment + "\"");
			}

			@Override
			public void onSuccess(Void v) {
				logger.log(Level.INFO, "Saved comment for student id=" + studentId + " and workpage id=" + workpageId
						+ " comment: \"" + comment + "\"");
			}
		});

	}

}
