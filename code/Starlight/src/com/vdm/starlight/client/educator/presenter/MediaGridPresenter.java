package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.HasImage;
import com.vdm.starlight.client.educator.view.media.MediaGridView;
import com.vdm.starlight.client.events.MediaEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.ResourceDTO;

/**
 * 
 * @author h
 * 
 */
public class MediaGridPresenter implements Presenter, MediaGridView.Presenter {
	Logger logger = Logger.getLogger(" MediaGridPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final MediaGridView view;

	private HasImage widget;

	public MediaGridPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, MediaGridView mediaGridView) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = mediaGridView;
		this.view.setPresenter(this);

	}

	public MediaGridPresenter(DatabaseServiceAsync rpcService, MediaGridView mediaGridView, HasImage widget) {
		this.view = mediaGridView;
		this.view.setPresenter(this);
		this.dbService = rpcService;
		this.eventBus = null;
		this.widget = widget;

	}

	public void fetchMore(int fromIndex) {
		dbService.getAllResources(fromIndex, new AsyncCallback<List<ResourceDTO>>() {
			public void onSuccess(List<ResourceDTO> result) {
				MediaGridPresenter.this.view.newDataToDisplay(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching media resources: " + caught.getMessage());
			}
		});
	}

	public void getImage(int id) {
		dbService.getFullImage(id, new AsyncCallback<String>() {
			public void onSuccess(String result) {
				MediaGridPresenter.this.view.showImage(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching full image: " + caught.getMessage());
			}
		});
	}

	@Override
	public void go(HasWidgets container) {
		if (container != null) {
			container.clear();
			container.add(view.asWidget());
		} else
			fetchMore(0);
	}

	@Override
	public void uploadLinkClick() {
		if (eventBus != null) {
			eventBus.fireEvent(new MediaEvent(MediaEvent.MediaEventType.UPLOAD));
		}
	}

	public void setImage(int id, String imageData, int width, int height) {
		this.widget.setImage(id, imageData, width, height);
	}

	@Override
	public void deleteResource(int resourceID) {
		dbService.deleteResource(resourceID, new AsyncCallback<Void>() {
			public void onFailure(Throwable caught) {
				Window.alert("Error deleting resource");
			}

			@Override
			public void onSuccess(Void result) {
				Window.alert("Resource successfully deleted");
				view.removeDeletedItem();
			}
		});

	}
}
