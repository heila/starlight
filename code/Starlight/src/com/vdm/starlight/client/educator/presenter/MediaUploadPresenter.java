package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.view.media.MediaUploadView;
import com.vdm.starlight.client.events.MediaEvent;
import com.vdm.starlight.client.presenter.Presenter;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class MediaUploadPresenter implements Presenter, MediaUploadView.Presenter {
	Logger logger = Logger.getLogger(" MediaGridPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final MediaUploadView view;

	public MediaUploadPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			MediaUploadView mediaUploadView) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = mediaUploadView;
		this.view.setPresenter(this);

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
	}

	@Override
	public void gridLinkClick() {
		eventBus.fireEvent(new MediaEvent(MediaEvent.MediaEventType.GRID));

	}

	@Override
	public void createResource(final String filename) {
		dbService.saveImage(filename, new AsyncCallback<Integer>() {

			@Override
			public void onSuccess(Integer result) {
				// Window.alert("Successfully uploaded " + filename);
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Could not upload " + filename);

			}
		});
	}

	// @Override
	// public void uploadLinkClick() {
	// eventBus.fireEvent(new MediaEvent(MediaEvent.MediaEventType.UPLOAD));
	// }

}
