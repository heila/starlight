package com.vdm.starlight.client.educator.view.workbook;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.NumberLabel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.Range;
import com.google.gwt.view.client.RangeChangeEvent;
import com.google.gwt.view.client.SelectionChangeEvent;
import com.google.gwt.view.client.SingleSelectionModel;
import com.vdm.starlight.client.educator.view.classroom.ClassroomList;
import com.vdm.starlight.client.educator.view.student.StudentsList;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;

/**
 * This view displays the information and statistics of a workbook. It is used to perform certain operations on the
 * workbook as well as update the workbooks information.
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookInfoViewImpl extends Composite implements WorkbookInfoView {
	static Logger logger = Logger.getLogger(" WorkbookInfoViewImpl");
	static EducatorResources resources = EducatorResources.INSTANCE;

	@UiTemplate("WorkbookInfoView.ui.xml")
	interface WorkbookInfoViewUiBinder extends UiBinder<Widget, WorkbookInfoViewImpl> {
	}

	private static WorkbookInfoViewUiBinder uiBinder = GWT.create(WorkbookInfoViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	@UiField
	Label nameLabel;
	@UiField
	Label createdLabel;
	@UiField
	Label modifiedLabel;
	@UiField
	Label ageLabel;
	@UiField
	NumberLabel<Integer> averageLabel;
	@UiField
	Label descriptionLabel;
	@UiField
	NumberLabel<Integer> numStudentsLabel;
	@UiField
	NumberLabel<Integer> numPagesLabel;
	@UiField
	NumberLabel<Integer> numCompLabel;
	@UiField
	Image imagehtml;

	@UiField
	Anchor designLink;
	@UiField
	Anchor viewLink;

	@UiField
	Anchor publishLink;
	@UiField
	Anchor deleteLink;
	@UiField
	Anchor classLink;
	@UiField
	Anchor studentLink;
	@UiField
	Anchor editLink;

	@UiField
	Grid grid;

	@UiField
	ClassroomList classroomList;

	@UiField
	StudentsList studentsList;

	private Presenter presenter;

	public WorkbookInfoViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		grid.getCellFormatter().getElement(0, 0).setAttribute("colspan", "2");

		classroomList.getDataDisplay().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				final Range range = event.getNewRange();
				final int start = range.getStart();
				int end = start + range.getLength();
				if (presenter != null)
					presenter.getWorkbookClassrooms(start, end, getClassroomListDisplay().getDataDisplay());
			}
		});

		studentsList.getDataDisplay().addRangeChangeHandler(new RangeChangeEvent.Handler() {
			@Override
			public void onRangeChange(RangeChangeEvent event) {
				final Range range = event.getNewRange();
				final int start = range.getStart();
				int end = start + range.getLength();
				if (presenter != null)
					presenter.getWorkbookStudents(start, end, getStudentsListDisplay().getDataDisplay());
			}
		});

		// Add a selection model so we can select cells.
		final SingleSelectionModel<ClassroomDTO> selectionModel = new SingleSelectionModel<ClassroomDTO>(
				ClassroomDTO.KEY_PROVIDER);

		classroomList.getDataDisplay().setSelectionModel(selectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				ClassroomDTO classroom = selectionModel.getSelectedObject();
				if (classroom != null)
					presenter.onClassroomClick(selectionModel.getSelectedObject().getId());
			}
		});

		// Add a selection model so we can select cells.
		final SingleSelectionModel<StudentDTO> studentSelectionModel = new SingleSelectionModel<StudentDTO>(
				StudentDTO.KEY_PROVIDER);

		studentsList.getDataDisplay().setSelectionModel(studentSelectionModel);
		selectionModel.addSelectionChangeHandler(new SelectionChangeEvent.Handler() {
			public void onSelectionChange(SelectionChangeEvent event) {
				StudentDTO student = studentSelectionModel.getSelectedObject();
				if (student != null)
					presenter.onStudentClick(studentSelectionModel.getSelectedObject().getId());
			}
		});

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	@Override
	public void setStats(WorkbookStatsDTO result) {

		averageLabel.setValue(result.getNumClasrooms());
		numStudentsLabel.setValue(result.getNumStudents());
		numPagesLabel.setValue(result.getNumPages());
		numCompLabel.setValue(result.getNumCompleted());
	}

	@Override
	public void setWorkbookInfo(WorkbookDTO result) {
		nameLabel.setText(result.getName());
		createdLabel.setText(result.getDateCreated().toGMTString());
		modifiedLabel.setText(result.getDateModified().toGMTString());
		ageLabel.setText(String.valueOf(result.getAgeGroup()));
		descriptionLabel.setText(result.getDescription());

		if (result.isDefaultImage()) {
			ImageResource im = resources.book();
			imagehtml.setUrl(im.getSafeUri());
		} else {
			imagehtml.setUrl(result.getImage());
		}

		if (imagehtml.getHeight() > imagehtml.getWidth()) {
			imagehtml.setHeight("160px");
		} else {
			imagehtml.setWidth("160px");
		}
	}

	@UiHandler("designLink")
	void onDesignLinkClick(ClickEvent event) {
		this.presenter.designLinkClick();
	}

	@UiHandler("viewLink")
	void onViewLinkClick(ClickEvent event) {
		this.presenter.viewLinkClick();
	}

	@UiHandler("editLink")
	void onEditLinkClick(ClickEvent event) {
		this.presenter.editLinkClick();
	}

	@UiHandler("studentLink")
	void onStudentLinkClick(ClickEvent event) {
		this.presenter.addStudentLinkClick();
	}

	@UiHandler("classLink")
	void onClassLinkClick(ClickEvent event) {
		this.presenter.addClassLinkClick();
	}

	@UiHandler("publishLink")
	void onPublishLinkClick(ClickEvent event) {
		this.presenter.publishLinkClick();
	}

	@UiHandler("printLink")
	void onPrintLinkClick(ClickEvent event) {
		this.presenter.printLinkClick();
	}

	@UiHandler("deleteLink")
	void onDeleteLinkClick(ClickEvent event) {
		this.presenter.deleteLinkClick();
	}

	@Override
	public ClassroomList getClassroomListDisplay() {
		return classroomList;
	}

	@Override
	public StudentsList getStudentsListDisplay() {
		return studentsList;
	}

}
