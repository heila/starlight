package com.vdm.starlight.client.educator.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.HasData;
import com.google.gwt.view.client.Range;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.educator.presenter.ClassroomsListPresenter.FilterTypes;
import com.vdm.starlight.client.educator.view.classroom.ClassroomsListDialogImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookDialogView;
import com.vdm.starlight.client.educator.view.workbook.WorkbookEditDialogViewImpl;
import com.vdm.starlight.client.educator.view.workbook.WorkbookInfoView;
import com.vdm.starlight.client.events.ClassroomEvent;
import com.vdm.starlight.client.events.ClassroomEvent.ClassroomEventType;
import com.vdm.starlight.client.events.StudentEvent;
import com.vdm.starlight.client.events.StudentEvent.StudentEventType;
import com.vdm.starlight.client.events.WorkbookEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookInfoPresenter implements Presenter, WorkbookInfoView.Presenter {
	Logger logger = Logger.getLogger(" WorkbooksListPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int workbookId;

	/** The logged in Educator */
	private int educatorId;

	/** View */
	private final WorkbookInfoView view;

	private WorkbookDialogView workbookEditDialog;

	private ClassroomsListDialogImpl classListDialog;

	public WorkbookInfoPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			WorkbookInfoView workbookInfoView, int workbookId, int educatorId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookInfoView;
		this.workbookId = workbookId;
		this.educatorId = educatorId;
		this.view.setPresenter(this);

	}

	public void bind() {
		// Create a data provider.

		dbService.getWorkbookStat(workbookId, new AsyncCallback<WorkbookStatsDTO>() {
			public void onSuccess(WorkbookStatsDTO result) {
				view.setStats(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching" + caught.getMessage());
			}
		});

		dbService.getWorkbook(workbookId, true, new AsyncCallback<WorkbookDTO>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching" + caught.getMessage());
			}

			@Override
			public void onSuccess(WorkbookDTO result) {
				view.setWorkbookInfo(result);

			}

		});

		view.getStudentsListDisplay().getDataDisplay().setVisibleRangeAndClearData(new Range(0, 5), true);
		view.getClassroomListDisplay().getDataDisplay().setVisibleRangeAndClearData(new Range(0, 5), true);

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		bind();
	}

	@Override
	public void designLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.EDITOR, workbookId));
	}

	@Override
	public void studentLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.STUDENTS, workbookId));
	}

	@Override
	public void viewLinkClick() {
		eventBus.fireEvent(new WorkbookEvent(WorkbookEvent.WorkbookEventType.VIEW, workbookId));
	}

	@Override
	public void editLinkClick() {
		if (workbookEditDialog == null) {
			workbookEditDialog = new WorkbookEditDialogViewImpl(workbookId);
		}
		new WorkbookDialogPresenter(dbService, eventBus, workbookEditDialog, workbookId).getWorkbookInfo(workbookId);

		workbookEditDialog.asWidget();
	}

	@Override
	public void addStudentLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addClassLinkClick() {

		if (classListDialog == null) {
			classListDialog = new ClassroomsListDialogImpl();
		}
		new ClassroomsListPresenter(dbService, eventBus, classListDialog, FilterTypes.FILTER_BY_EDUCATOR_WORKBOOK,
				educatorId, workbookId, -1);

	}

	@Override
	public void publishLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void printLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteLinkClick() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onClassroomClick(int selectedItem) {
		eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.INFO, selectedItem));
	}

	@Override
	public void onStudentClick(int selectedItem) {
		eventBus.fireEvent(new StudentEvent(StudentEventType.INFO, selectedItem));
	}

	@Override
	public void getWorkbookStudents(final int start, int end, final HasData<StudentDTO> view) {
		dbService.getWorkbookStudents(workbookId, start, end, false, new AsyncCallback<List<StudentDTO>>() {

			public void onSuccess(List<StudentDTO> result) {
				view.setRowData(start, result);
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Error fetching workbook students details" + caught.getMessage());

			}
		});
	}

	@Override
	public void getWorkbookClassrooms(final int start, int end, final HasData<ClassroomDTO> view) {
		dbService.getWorkbookClassrooms(workbookId, start, end, false, new AsyncCallback<List<ClassroomDTO>>() {
			public void onSuccess(List<ClassroomDTO> result) {
				view.setRowData(start, result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching workbook classrooms details" + caught.getMessage());
			}
		});
	}
}
