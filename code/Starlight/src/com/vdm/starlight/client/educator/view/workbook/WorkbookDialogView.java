package com.vdm.starlight.client.educator.view.workbook;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public interface WorkbookDialogView {

	public interface Presenter {
		void createWorkbook(String[] text, int imageID);

		void updateWorkbookBasicInfo(WorkbookDTO w, int imageID);

		void getWorkbookInfo(int workbookID);

		void showImagePicker();

		void workbooksLinkClick();

	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void workbookCreatedResult(String string, int intValue);

	void setWorkbookInfo(WorkbookDTO w);
}