package com.vdm.starlight.client.educator.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.HasImage;
import com.vdm.starlight.client.editor.ImagePickerViewImpl;
import com.vdm.starlight.client.educator.view.classroom.ClassroomDialogView;
import com.vdm.starlight.client.educator.view.classroom.ClassroomEditDialogViewImpl;
import com.vdm.starlight.client.educator.view.media.MediaGridView;
import com.vdm.starlight.client.events.ClassroomEvent;
import com.vdm.starlight.client.events.ClassroomEvent.ClassroomEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomDialogPresenter implements Presenter, ClassroomDialogView.Presenter {

	Logger logger = Logger.getLogger(" ClassroomNewPresenter");

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final ClassroomDialogView view;

	int educatorID;

	int classroomID;

	MediaGridView imagePickerView;

	public ClassroomDialogPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			ClassroomDialogView classroomDialogView, int educatorID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = classroomDialogView;
		this.view.setPresenter(this);
		this.educatorID = educatorID;
		this.classroomID = -1;

	}

	public ClassroomDialogPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus,
			ClassroomDialogView classroomDialogView, int educatorID, int classroomID) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = classroomDialogView;
		this.view.setPresenter(this);
		this.educatorID = educatorID;
		this.classroomID = classroomID;
	}

	@Override
	public void go(HasWidgets container) {

		container.clear();
		container.add(view.asWidget());

	}

	@Override
	public void createClassroom(final String[] text, final int imageID) {
		dbService.createClassroom(text[0], text[1], text[2], educatorID, imageID, new AsyncCallback<Integer>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error creating classroom: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Integer result) {
				ClassroomDialogPresenter.this.view.classroomCreatedResult(text[0], result.intValue());
			}
		});

	}

	@Override
	public void classroomsLinkClick() {
		if (eventBus != null) {
			eventBus.fireEvent(new ClassroomEvent(ClassroomEvent.ClassroomEventType.CLASSROOMS, 0));
		}
	}

	@Override
	public void showImagePicker() {
		boolean loadImages = false;
		if (imagePickerView == null) {
			imagePickerView = new ImagePickerViewImpl();
			loadImages = true;
		}
		MediaGridPresenter pres = new MediaGridPresenter(dbService, imagePickerView, (HasImage) this.view);
		if (loadImages) {
			pres.go(null);
		}

		imagePickerView.asWidget();

	}

	@Override
	public void getClassroomInfo(int classroomID) {
		dbService.getClassroom(classroomID, true, new AsyncCallback<ClassroomDTO>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Could not retrieve classroom information");
			}

			@Override
			public void onSuccess(ClassroomDTO result) {
				view.setClassroomInfo(result);
			}
		});
	}

	@Override
	public void updateClassroomBasicInfo(final ClassroomDTO w, int imageID) {
		dbService.saveClassroom(w, imageID, new AsyncCallback<Void>() {

			public void onFailure(Throwable caught) {
				Window.alert("Error updating classroom: " + caught.getMessage());
			}

			@Override
			public void onSuccess(Void result) {
				eventBus.fireEvent(new ClassroomEvent(ClassroomEventType.INFO, w.getId()));
				((ClassroomEditDialogViewImpl) view).editClassroomDialog.hide();
			}
		});

	}

}