package com.vdm.starlight.client.educator.view.media;

import java.util.List;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.ResourceDTO;

public interface MediaGridView {

	public interface Presenter {

		// void gridLinkClick();

		void uploadLinkClick();

		void fetchMore(int start);

		void getImage(int id);

		void setImage(int selectedImageID, String selectedImageURL, int width, int height);

		void deleteResource(int resourceID);
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();

	void newDataToDisplay(List<ResourceDTO> pNewList);

	void showImage(String data);

	void removeDeletedItem();

	// void setStats(WorkbookStatInfo result);

	// void setData(Workbook workbook);
}