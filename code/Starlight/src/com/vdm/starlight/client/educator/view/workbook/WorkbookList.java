package com.vdm.starlight.client.educator.view.workbook;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.cellview.client.CellList;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.educator.view.RangePager;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public class WorkbookList extends Composite {
	static EducatorResources resources = EducatorResources.INSTANCE;

	/** The default list size. */
	private static final int DEFAULT_LIST_SIZE = 5;

	@UiTemplate("WorkbookList.ui.xml")
	interface WorkbookListUiBinder extends UiBinder<Widget, WorkbookList> {
	}

	private static WorkbookListUiBinder uiBinder = GWT.create(WorkbookListUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		EducatorResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of workbook objects */
	@UiField(provided = true)
	CellList<WorkbookDTO> workbookList;

	/** Manages the number of workbooks shown in cellList at a time */
	@UiField
	RangePager<WorkbookDTO> pager;

	public WorkbookList() {
		WorkbookCell cell = new WorkbookCell();
		workbookList = new CellList<WorkbookDTO>(cell);
		pager = new RangePager<WorkbookDTO>();
		initWidget(uiBinder.createAndBindUi(this));

		workbookList.setVisibleRange(0, DEFAULT_LIST_SIZE);

		pager.setDisplay(workbookList);
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	/**
	 * @return The widget implementing HasData which displays the list of workbooks
	 */
	public CellList<WorkbookDTO> getDataDisplay() {
		return this.workbookList;
	}

	public void enableNext(boolean b) {
		this.pager.enableNext(b);
	}

	public void enablePrev(boolean b) {
		this.pager.enablePrev(b);
	}
}
