package com.vdm.starlight.client.editor.objects;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Component that can be added to a {@link Workpage}. It represents an text input box on the {@link Workpage}. The text
 * color, size and other {@link Property}(ies) of the {@link Component} can be set. The component is modeled using a gwt
 * {@link TextBox} object.
 * 
 * @author Heila van der Merwe
 * 
 */
public class RadioBoxObject extends VerticalPanel implements Component {

	/**
	 * This is as a unique id of the component relative to the {@link Workpage} it is on. It is given to the component
	 * on creation and is stored and reloaded when the component is created. It uniquely identifies every component on
	 * the {@link Workpage} for when user input is saved for a component.
	 */
	private final int id;

	/** List of {@link Properties} that can be set by the user */
	private final Property[] properties = { Property.Pos, Property.Height, Property.Width, Property.List,
			Property.Input, Property.Layer, Property.Caption, Property.TextSize, Property.TextColour };

	/**
	 * The type of the component. Each component type has a description and name given in the {@link ComponentType} enum
	 */
	private final ComponentType type = ComponentType.RADIOBOX;
	private Label caption = new Label("Choose an option:");
	private List<String> options = new ArrayList<String>();

	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * Constructor for a TextBox. Here all default style and other properties are set.
	 */
	public RadioBoxObject(int id) {
		super();
		this.id = id;
		this.getElement().getStyle().setFontSize(14, Unit.PT);
		this.getElement().getStyle().setColor("black");
		this.getElement().getStyle().setZIndex(58);
		this.getElement().getStyle().setPadding(10, Unit.PX);
		this.setWidth("125px");
		this.setHeight("25px");
		this.add(caption);
		String[] option = { "Option1", "Option2", "Option3" };
		for (int i = 0; i < option.length; i++) {
			RadioButton b = new RadioButton(String.valueOf(id), option[i]);
			this.add(b);
			options.add(option[i]);
		}

	}

	@Override
	public String call(String property, String param, boolean get) {
		if (property.equals(Property.Input.getName())) {
			if (get) {
				String s = "";
				Widget w = null;
				for (int i = 0; i < this.getWidgetCount(); i++) {
					w = this.getWidget(i);
					if (w instanceof RadioButton) {
						s += ((RadioButton) w).getText() + "<selected>" + ((RadioButton) w).getValue() + "<option>";
					}
				}
				return s;
			} else {
				if (!param.trim().equals("")) { // if no input has been given
					this.clear();
					options.clear();
					this.add(caption);
					String[] optionpairs = param.split("<option>"); // split into options
					for (int i = 0; i < optionpairs.length; i++) {
						String option[] = optionpairs[i].split("<selected>"); // split a option in option~value pair
						RadioButton b = new RadioButton(String.valueOf(id), option[0].trim());
						boolean checked = Boolean.parseBoolean(option[1]);
						b.setValue(checked);
						this.add(b);
						options.add(option[0].trim());
					}
				}
				return "";
			}
		} else if (property.equals(Property.TextColour.getName())) {
			if (get) {
				return this.getElement().getStyle().getColor();
			} else {
				this.getElement().getStyle().setColor(param);
				return "";
			}
		} else if (property.equals(Property.Caption.getName())) {
			if (get) {
				return caption.getText();
			} else {
				caption.setText(param);
				return "";
			}
		} else if (property.equals(Property.TextSize.getName())) {
			if (get) {
				return this.getElement().getStyle().getFontSize();
			} else {
				this.getElement().getStyle()
						.setFontSize(Double.parseDouble(param.substring(0, param.length() - 2)), Unit.PT);
				setFontSize(Double.parseDouble(param.substring(0, param.length() - 2)));
				return "";
			}
		} else if (property.equals(Property.List.getName())) {
			if (get) {
				String optionsString = "";
				for (String s : options) {
					optionsString = optionsString + s + "\n";
				}
				return optionsString;
			} else {
				if (!param.equals("")) {
					this.clear();
					this.add(caption);
					String[] optionsString = param.split("\n");
					options = new ArrayList<String>();
					for (int i = 0; i < optionsString.length; i++) {
						if (optionsString[i].trim().length() > 0 && optionsString[i].trim() != null) {
							RadioButton b = new RadioButton("group", optionsString[i].trim());
							this.add(b);
							options.add(optionsString[i].trim());
						}
					}
				}
				return "";
			}
		} else if (property.equals(Property.Layer.getName())) {
			if (get) {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				return String.valueOf(z);
			} else {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				String zIndex = this.getElement().getStyle().getZIndex();
				if (param.equals("foward")) {
					if (Integer.parseInt(zIndex) < 400)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) + 10);
				} else if (param.equals("back")) {
					if (Integer.parseInt(zIndex) > 0)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) - 10);
				} else {
					DOM.setIntStyleAttribute(this.getElement(), "zIndex", Integer.parseInt(param));
				}
				return "";
			}
		} else if (property.equals(Property.Pos.getName())) {
			if (get) {
				return "" + (this.getAbsoluteLeft() - 1 - this.getParent().getAbsoluteLeft()) + "^"
						+ (this.getAbsoluteTop() - 1 - this.getParent().getAbsoluteTop());
			} else {
				String[] pos = param.split("\\^");
				((AbsolutePanel) this.getParent()).setWidgetPosition(this, Integer.parseInt(pos[0]),
						Integer.parseInt(pos[1]));
				return "";
			}
		} else if (property.equals(Property.Height.getName())) {
			if (get) {
				String height = this.getElement().getStyle().getHeight();
				return height.substring(0, height.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setHeight(Double.parseDouble(param), Unit.PX);
				return "";
			}
		} else if (property.equals(Property.Width.getName())) {
			if (get) {
				String width = this.getElement().getStyle().getWidth();
				return width.substring(0, width.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setWidth(Double.parseDouble(param), Unit.PX);
				return "";
			}
		}

		return "DefaultValue";
	}

	private void setFontSize(double parseDouble) {
		for (int i = 0; i < this.getWidgetCount(); i++) {
			Widget w = this.getWidget(i);
			w.getElement().getStyle().setFontSize(parseDouble, Unit.PT);
		}

	}

	@Override
	public String[][] getProperties() {
		String[][] list = new String[properties.length][2];
		for (int i = 0; i < properties.length; i++) {
			list[i][0] = properties[i].getName();
			list[i][1] = call(properties[i].getName(), "", true);
		}
		return list;
	}

	@Override
	public void setProperties(String propertyList) {
		String[] property = propertyList.split("`");
		for (String s : property) {
			if (!s.equals("")) {
				String[] prop = s.split("~");
				if (prop.length < 2)
					call(prop[0], "", false);
				else
					call(prop[0], prop[1], false);
			}
		}
	}

	public Widget getDragHandle() {
		return caption;
	}

	@Override
	public String toString() {
		String output = "";
		for (Property p : properties) {
			output += ("`" + p.getName() + "~" + call(p.getName(), "", true));
		}
		return type.getName() + "|" + this.id + "|" + output;
	}

}
