package com.vdm.starlight.client.editor;

import com.allen_sauer.gwt.dnd.client.DragController;
import com.allen_sauer.gwt.dnd.client.util.DOMUtil;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.safehtml.shared.SafeHtmlBuilder;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.objects.CheckBoxObject;
import com.vdm.starlight.client.editor.objects.Component;
import com.vdm.starlight.client.editor.objects.RadioBoxObject;
import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * This class is an AnchorLink that is displayed in the component pallet. It represents a specified component that can
 * be added to a {@link Workpage}. When this Anchor Link is clicked, a new component is created and added to the panel
 * representing the currently visible {@link Workpage}.
 * 
 * @author Heila van der Merwe
 * 
 */
public class PalletLabel extends Anchor {
	private final AbsolutePanel targetPanel;
	private final DragController dragController;
	private final ComponentType type;

	WorkpageDTO workpageinfo = null;
	Workpage w = null;

	@SuppressWarnings("deprecation")
	public PalletLabel(ComponentType type, AbsolutePanel target, DragController dragController) {
		this.targetPanel = target;
		this.dragController = dragController;
		this.type = type;

		SafeHtmlBuilder sb = new SafeHtmlBuilder();
		sb.appendHtmlConstant("<table>");
		sb.appendHtmlConstant("<tr><td width='15px' height='20px' align='center'>");
		sb.appendHtmlConstant("<img src=\"" + type.getImage().getURL() + "\" height=\"20px\"/>");
		sb.appendHtmlConstant("</td>");
		sb.appendHtmlConstant("<td style='font-size:100%;'>");
		sb.appendEscaped(type.getName());
		sb.appendHtmlConstant("</td></tr></table>");
		this.setHTML(sb.toSafeHtml());

		this.setWordWrap(false);
		// create a new instance of the object this pallet label represents when it is clicked
		this.addClickHandler(new PalletLableClickHandler());
		// add tooltip popup to the pallet label
		this.addMouseOutHandler(new ToolTipListener(type.getDescription()));
		this.addMouseOverHandler(new ToolTipListener(type.getDescription()));

	}

	/**
	 * Sets the current workpage. This is needed because as objects are added to this page, its unique id count has to
	 * increase
	 * 
	 * @param workpageinfo
	 *            the currently visible {@link Workpage}
	 */
	public void changePage(WorkpageDTO workpageinfo, Workpage w) {
		this.workpageinfo = workpageinfo;
		this.w = w;

	}

	/**
	 * Adds a new instance of a component of type {@link ComponentType} to the target panel representing the
	 * {@link workpage}.
	 * 
	 * @author Heila van der Merwe
	 * 
	 */
	public class PalletLableClickHandler implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			if (workpageinfo != null) { // the current workpage has not been set

				// increase unique id of the workpage
				int uniqueId = workpageinfo.getUniqueComponentId() + 1;
				workpageinfo.setUniqueComponentId(uniqueId);
				w.setUniqueComponentId(uniqueId);

				// create an instance of the object represented by this label
				final Widget object = PalletLabel.this.type.getEducatorWidget(uniqueId, null);

				// add it to the DOM so that offset width/height becomes available
				targetPanel.add(object, 0, 0);

				// determine random label location within target panel
				int left = Random.nextInt(DOMUtil.getClientWidth(targetPanel.getElement()) - 400
						- object.getOffsetWidth());
				int top = Random.nextInt(DOMUtil.getClientHeight(targetPanel.getElement()) - 400
						- object.getOffsetHeight());

				// move the object
				targetPanel.setWidgetPosition(object, left, top);
				if (object instanceof RadioBoxObject || object instanceof CheckBoxObject)
					PalletLabel.this.dragController.makeDraggable(object, ((Component) object).getDragHandle());
				else
					PalletLabel.this.dragController.makeDraggable(object);

			}
		}
	}

}
