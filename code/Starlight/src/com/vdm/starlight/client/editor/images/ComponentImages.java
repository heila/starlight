package com.vdm.starlight.client.editor.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface ComponentImages extends ClientBundle {
	ImageResource image();

	ImageResource label();

	ImageResource textbox();

	ImageResource combobox();

	ImageResource checkbox();

	ImageResource radiobox();

	ImageResource borderColour();

}