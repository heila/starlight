package com.vdm.starlight.client.editor;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.images.ComponentImages;
import com.vdm.starlight.client.editor.objects.CheckBoxObject;
import com.vdm.starlight.client.editor.objects.ComboBoxObject;
import com.vdm.starlight.client.editor.objects.ImageObject;
import com.vdm.starlight.client.editor.objects.LabelObject;
import com.vdm.starlight.client.editor.objects.RadioBoxObject;
import com.vdm.starlight.client.editor.objects.RectangleObject;
import com.vdm.starlight.client.editor.objects.TextBoxObject;

public enum ComponentType {

	LABEL(8, "Static Text", "Displays  static text"), IMAGE(12, "Image", "Displays an image from the media library."), TEXTBOX(
			5, "Text Input Box", "Input for one line of text."), COMBOBOX(1, "Option Box",
			"A box from which an option can be chooses"), CHECKBOX(2, "Check Box",
			"A checkbox that can be set to true or false."), RADIOBOX(3, "Radio Buttons",
			"Group of two or more radio buttons. Only one can be selected at a time."), RECTANGLE(4, "Rectangle",
			"Creates a rectangle which border and background can be set");

	//
	// CHECKBOX(1, "Checkbox", "A checkbox that can be set to true or false."), RADIOBUTTONS(2, "Radio Buttons",
	// "Group of two or more radio buttons. Only one can be selected at a time."), BASICBUTTON(3, "Button",
	// "A button that can be pressed."), LISTBOX(4, "Listbox", "Displays a list of items."), TEXTAREA(6, "Textarea",
	// "Input for more than one line of text."), BASICPOPUP(
	// 7, "Popup dialog box", "Pops up a dialog on screen"), VERTICALPANEL(
	// 9, "Vertical Panel", "Groups items together underneath each other."), HORIZONTALPANEL(10,
	// "Horizontal Panel", "Groups items together next to each other."), GRID(11, "Grid",
	// "Groups items in a grid."),

	private ComponentImages images;
	private int type;
	private String name;
	private String description;
	Widget w;

	private ComponentType(int type, String name, String description) {
		if (images == null) {
			images = GWT.create(ComponentImages.class);
		}
		this.type = type;
		this.name = name;
		this.description = description;
	}

	public int getType() {
		return type;
	}

	public String getName() {
		return name;
	}

	public ImageResource getImage() {
		switch (ComponentType.this) {
		case CHECKBOX:
			return images.checkbox();
		case IMAGE:
			return images.image();
		case COMBOBOX:
			return images.combobox();
		case LABEL:
			return images.label();
		case RADIOBOX:
			return images.radiobox();
		case TEXTBOX:
			return images.textbox();
		case RECTANGLE:
			return images.borderColour();
		default:
			break;
		}
		return null;
	}

	public String getDescription() {
		return description;
	}

	public int getId() {
		return this.type;
	}

	public static ComponentType getCompoType(String name) {
		for (ComponentType c : ComponentType.values()) {
			if (c.getName().equals(name))
				return c;
		}
		return null;
	}

	public Widget getEducatorWidget(int id, DatabaseServiceAsync dbService) {
		if (this.equals(ComponentType.LABEL)) {
			w = new LabelObject(id, true);
		} else if (this.equals(ComponentType.IMAGE)) {
			w = new ImageObject(id, dbService);
		} else if (this.equals(ComponentType.TEXTBOX)) {
			w = new TextBoxObject(id, true);
		} else if (this.equals(ComponentType.CHECKBOX)) {
			w = new CheckBoxObject(id);
		} else if (this.equals(ComponentType.RADIOBOX)) {
			w = new RadioBoxObject(id);
		} else if (this.equals(ComponentType.COMBOBOX)) {
			w = new ComboBoxObject(id);
		} else if (this.equals(ComponentType.RECTANGLE)) {
			w = new RectangleObject(id, dbService, true);
		}
		return w;
	}

	public Widget getStudentWidget(int id, DatabaseServiceAsync dbService) {
		if (this.equals(ComponentType.LABEL)) {
			w = new LabelObject(id, false);
		} else if (this.equals(ComponentType.IMAGE)) {
			w = new ImageObject(id, dbService);
		} else if (this.equals(ComponentType.TEXTBOX)) {
			w = new TextBoxObject(id, false);
		} else if (this.equals(ComponentType.CHECKBOX)) {
			w = new CheckBoxObject(id);
		} else if (this.equals(ComponentType.RADIOBOX)) {
			w = new RadioBoxObject(id);
		} else if (this.equals(ComponentType.COMBOBOX)) {
			w = new ComboBoxObject(id);
		} else if (this.equals(ComponentType.RECTANGLE)) {
			w = new RectangleObject(id, dbService, false);
		}
		return w;
	}
}
