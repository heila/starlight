package com.vdm.starlight.client.editor.objects;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gwt.dev.cfg.Properties;
import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.HasImage;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.client.resources.EducatorResources;
import com.vdm.starlight.shared.objects.Resource;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Component that can be added to a {@link Workpage}. It represents an image on the {@link Workpage}. The width height,
 * image and other {@link Property}(ies) of the {@link Component} can be set. The component is modeled using a gwt
 * {@link Image} object.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ImageObject extends Image implements Component, HasImage {
	Logger logger = Logger.getLogger(" ClassroomInfoPresenter");

	/**
	 * This is as a unique id of the component relative to the {@link Workpage} it is on. It is given to the component
	 * on creation and is stored and reloaded when the component is created. It uniquely identifies every component on
	 * the {@link Workpage} for when user input is saved for a component.
	 */
	private final int id;

	/** List of {@link Properties} that can be set by the user */
	private final Property[] properties = { Property.Image, Property.Pos, Property.Height, Property.Width,
			Property.Layer };

	/**
	 * The type of the component. Each component type has a description and name given in the {@link ComponentType} enum
	 */
	private final ComponentType type = ComponentType.IMAGE;

	/** The id of the image {@link Resource} displayed by this Component. -1 indicates the default image */
	private int imageId = -1;

	static EducatorResources resources = EducatorResources.INSTANCE;

	private DatabaseServiceAsync dbService;

	/**
	 * Constructor for a new Image Object;
	 */
	public ImageObject(int id, DatabaseServiceAsync dbService) {
		super(resources.defaultIcon().getURL());
		this.getElement().getStyle().setWidth(95, Unit.PX);
		this.getElement().getStyle().setHeight(92, Unit.PX);
		this.dbService = dbService;
		this.id = id;
		DOM.setIntStyleAttribute(this.getElement(), "zIndex", 56);
		this.getElement().getStyle().setBorderColor("transparent");
		this.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		this.getElement().getStyle().setBorderWidth(1, Unit.PX);
	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String call(String property, final String param, boolean get) {
		if (property.equals(Property.Image.getName())) {
			if (get) {
				return String.valueOf(this.imageId);
			} else {
				if (!param.equals(""))
					if (!param.equals("-1")) {
						dbService.getFullImage(Integer.parseInt(param), new AsyncCallback<String>() {

							@Override
							public void onSuccess(String result) {
								imageId = Integer.parseInt(param);
								ImageObject.this.setImage(imageId, "data:image/png;base64," + result, -1, -1);
							}

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Could not load new image");
							}
						});
					}
				return "";
			}
		} else if (property.equals(Property.Layer.getName())) {
			if (get) {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				// String z = this.getElement().getStyle().getZIndex();
				return String.valueOf(z);
			} else {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				String zIndex = this.getElement().getStyle().getZIndex();
				if (param.equals("foward")) {
					if (Integer.parseInt(zIndex) < 200)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) + 10);
				} else if (param.equals("back")) {
					if (Integer.parseInt(zIndex) > 0)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) - 10);
				} else {
					DOM.setIntStyleAttribute(this.getElement(), "zIndex", Integer.parseInt(param));
				}
				return "";
			}
		} else if (property.equals(Property.Pos.getName())) {
			if (get) {
				return "" + (this.getAbsoluteLeft() - 1 - this.getParent().getAbsoluteLeft()) + "^"
						+ (this.getAbsoluteTop() - 1 - this.getParent().getAbsoluteTop());
			} else {
				if (!param.equals("")) {
					String[] pos = param.split("\\^");
					((AbsolutePanel) this.getParent()).setWidgetPosition(this, Integer.parseInt(pos[0]),
							Integer.parseInt(pos[1]));
				}
				return "";
			}
		} else if (property.equals(Property.Height.getName())) {
			if (get) {
				logger.log(Level.INFO, String.valueOf(this.getHeight()));
				String w = this.getElement().getStyle().getHeight();
				return String.valueOf(this.getHeight());
			} else {
				if (!param.equals("")) {
					int newHeight = Integer.valueOf(param);
					if (this.getHeight() != 0) {
						float newWidth = (Float.valueOf(param) / this.getHeight()) * this.getWidth();
						this.setPixelSize(Math.round(newWidth), newHeight);
						this.setHeight((String.valueOf(newHeight)));
						logger.log(Level.INFO, String.valueOf(newHeight));
					} else {
						this.setPixelSize(this.getWidth(), newHeight);
						logger.log(Level.INFO, String.valueOf(newHeight));
					}
				}
				return "";
			}
		} else if (property.equals(Property.Width.getName())) {
			if (get) {
				return String.valueOf(this.getWidth());
			} else {
				if (!param.equals("")) {
					int newWidth = Integer.valueOf(param);
					if (this.getWidth() != 0) {
						float newHeight = (Float.valueOf(param) / this.getWidth()) * this.getHeight();
						this.setPixelSize(newWidth, Math.round(newHeight));
					} else
						this.setPixelSize(newWidth, this.getHeight());
				}
				return "";
			}
		}
		return "DefaultValue";
	}

	@Override
	public String[][] getProperties() {
		String[][] list = new String[properties.length][2];
		for (int i = 0; i < properties.length; i++) {
			list[i][0] = properties[i].getName();
			list[i][1] = call(properties[i].getName(), "", true);
		}
		return list;
	}

	@Override
	public void setProperties(String propertyList) {
		String[] property = propertyList.split("`");
		for (String s : property) {
			if (!s.equals("")) {
				String[] prop = s.split("~");
				if (prop.length < 2)
					call(prop[0], "", false);
				else {
					if (prop[0].equals("Width")) {
						this.setPixelSize(Integer.parseInt(prop[1]), this.getHeight());
					} else if (prop[0].equals("Height"))
						this.setPixelSize(this.getWidth(), Integer.parseInt(prop[1]));

					call(prop[0], prop[1], false);

				}
			}
		}
	}

	@Override
	public String toString() {
		String output = "";
		for (Property p : properties) {
			output += ("`" + p.getName() + "~" + call(p.getName(), "", true));
		}
		return type.getName() + "|" + this.id + "|" + output;
	}

	public void setImage(int imageId, String imageString, int width, int height) {
		this.setUrl(imageString);
		if (width != -1)
			this.getElement().getStyle().setWidth(width, Unit.PX);
		if (height != -1)
			this.getElement().getStyle().setHeight(height, Unit.PX);
		this.imageId = imageId;
	}

	@Override
	public Widget getDragHandle() {
		// TODO Auto-generated method stub
		return null;
	}
}
