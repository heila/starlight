package com.vdm.starlight.client.editor.images;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface PropertyImages extends ClientBundle {
	ImageResource colorfill();

	ImageResource TextColor();

	ImageResource layers();

	ImageResource horizontalArrow();

	ImageResource verticalArrow();

	ImageResource SizeSmaller();

	ImageResource alignRight();

	ImageResource alignLeft();

	ImageResource alignCenter();

	ImageResource bold();

	ImageResource image();

	ImageResource inputType();

	ImageResource borderColour();

	ImageResource borderWidth();

}