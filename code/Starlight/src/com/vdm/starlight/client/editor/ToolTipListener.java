package com.vdm.starlight.client.editor;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class displays a tooltip {@link PopupPanel} on a {@link MouseOverEvent} when attached to the object's
 * {@link MouseOverHandler}.
 * 
 * @author Heila van der Merwe
 * 
 */
class ToolTipListener implements MouseOverHandler, MouseOutHandler {
	/** X-offset of the {@link PopupPanel} from the left side of the object. */
	private final int X_OFFSET = 10;
	/** Y-offset of the {@link PopupPanel} from the top of the object. */
	private final int Y_OFFSET = 25;
	private static ToolTip tooltip;
	private String text;

	public ToolTipListener(String text) {
		this.text = text;
	}

	@Override
	public void onMouseOut(MouseOutEvent event) {
		if (tooltip != null) {
			tooltip.hide();
		}

	}

	@Override
	public void onMouseOver(MouseOverEvent event) {
		tooltip = new ToolTip((Widget) event.getSource(), X_OFFSET, Y_OFFSET, text);
		tooltip.getElement().getStyle().setBackgroundColor("#CBF1FB");
		tooltip.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		tooltip.getElement().getStyle().setBorderColor("#808080");
		tooltip.show();

	}
}