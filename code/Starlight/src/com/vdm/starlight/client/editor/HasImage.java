package com.vdm.starlight.client.editor;

public interface HasImage {
	public void setImage(int id, String imageData, int width, int height);
}
