package com.vdm.starlight.client.editor;

import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.PopupPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.StudentResources;

/**
 * The {@link PopupPanel} that is displayed on the {@link MouseOverEvent} event.
 * 
 * @author Heila van der Merwe
 * 
 */
public class ToolTip extends PopupPanel {
	private int delay;
	static StudentResources resources = StudentResources.INSTANCE;
	
	public ToolTip(Widget sender, int offsetX, int offsetY, final String text) {
		super(true);
		HTML contents = new HTML(text);
		add(contents);
		int left = sender.getAbsoluteLeft() + offsetX;
		int top = sender.getAbsoluteTop() + offsetY;
		setPopupPosition(left, top);
		setStyleName("tooltip");
	}
	
	public ToolTip(int offsetX, int offsetY, final String text) {
		super(true);
		HTML contents = new HTML(text);
		add(contents);
		int left = offsetX;
		int top = offsetY;
		setPopupPosition(left, top);
		setStyleName(resources.starlightCSS().speechBubble());
	}

	public void show() {
		super.show();

	}
}