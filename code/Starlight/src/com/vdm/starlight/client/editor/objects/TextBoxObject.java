package com.vdm.starlight.client.editor.objects;

import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.InputType;
import com.vdm.starlight.client.editor.NumberInputValidator;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Component that can be added to a {@link Workpage}. It represents an text input box on the {@link Workpage}. The text
 * color, size and other {@link Property}(ies) of the {@link Component} can be set. The component is modeled using a gwt
 * {@link TextBox} object.
 * 
 * @author Heila van der Merwe
 * 
 */
public class TextBoxObject extends TextArea implements Component {

	/**
	 * This is as a unique id of the component relative to the {@link Workpage} it is on. It is given to the component
	 * on creation and is stored and reloaded when the component is created. It uniquely identifies every component on
	 * the {@link Workpage} for when user input is saved for a component.
	 */
	private final int id;

	/** List of {@link Properties} that can be set by the user */
	private final Property[] properties = { Property.TextSize, Property.TextColour, Property.Pos, Property.Height,
			Property.Width, Property.InputType, Property.Layer, Property.Input };

	/**
	 * The type of the component. Each component type has a description and name given in the {@link ComponentType} enum
	 */
	private final ComponentType type = ComponentType.TEXTBOX;

	private InputType inputType = InputType.String;
	HandlerRegistration keyUpHandler = null;

	/**
	 * Constructor for a TextBox. Here all default style and other properties are set.
	 */
	public TextBoxObject(int id, boolean isEducator) {
		super();
		this.id = id;
		this.getElement().getStyle().setFontSize(14, Unit.PT);
		this.getElement().getStyle().setColor("black");
		this.setWidth("100px");
		this.setHeight("25px");
		this.getElement().getStyle().setMargin(0, Unit.PX);
		this.getElement().getStyle().setZIndex(57);
		this.getElement().getStyle().setProperty("fontFamily", "Arial,Helvetica,sans-serif");

		if (isEducator) {
			setEducatorProperties();
		} else {
			setStudentProperties();
		}

	}

	private void setStudentProperties() {
		this.setText("");
		this.getElement().getStyle().setBackgroundColor("#CCC");
		this.getElement().getStyle().setProperty("resize", "none");
	}

	private void setEducatorProperties() {
		this.setText("");
		this.setReadOnly(true);
		this.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				String position = call(Property.Pos.getName(), "", true); // Get position

				if (!position.equals("")) {
					String[] pos = position.split("\\^"); // width^height
					int height = Integer.parseInt(call(Property.Height.getName(), "", true));
					int width = Integer.parseInt(call(Property.Width.getName(), "", true));
					int borderWidth = 2;

					if (height + 4 + (1 * borderWidth) + Integer.parseInt(pos[1]) > 600) {
						int newHeight = 600 - Integer.parseInt(pos[1]) - 4 - (3 * borderWidth);
						call(Property.Height.getName(), String.valueOf(newHeight), false);
					}

					if (width + 4 + (1 * borderWidth) + Integer.parseInt(pos[0]) > 950) {
						int newWidth = 950 - Integer.parseInt(pos[0]) - 4 - (3 * borderWidth);
						call(Property.Width.getName(), String.valueOf(newWidth), false);
					}
				}
			}
		});

	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public String call(String property, String param, boolean get) {
		if (property.equals(Property.Input.getName())) {
			if (get) {
				return this.getText();
			} else {
				if (!param.equals(""))
					if (!param.equals("sample text")) {
						this.setText(param);

					} else
						this.setText("");
				return "";
			}
		} else if (property.equals(Property.InputType.getName())) {
			if (get) {
				return this.inputType.toString();
			} else {
				if (!param.equals("")) {
					InputType in = InputType.valueOf(param);
					if (in != null) {
						if (keyUpHandler != null)
							keyUpHandler.removeHandler();
						this.inputType = in;
						NumberInputValidator val = new NumberInputValidator(this, inputType);
						keyUpHandler = this.addKeyUpHandler(val);
					}
				}
				return "";
			}
		} else if (property.equals(Property.Layer.getName())) {
			if (get) {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				return String.valueOf(z);
			} else {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				String zIndex = this.getElement().getStyle().getZIndex();
				if (param.equals("foward")) {
					if (Integer.parseInt(zIndex) < 400)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) + 10);
				} else if (param.equals("back")) {
					if (Integer.parseInt(zIndex) > 12)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) - 10);
				} else {
					DOM.setIntStyleAttribute(this.getElement(), "zIndex", Integer.parseInt(param));
				}
				return "";
			}
		} else if (property.equals(Property.TextColour.getName())) {
			if (get) {
				return this.getElement().getStyle().getColor();
			} else {
				this.getElement().getStyle().setColor(param);
				return "";
			}
		} else if (property.equals(Property.TextSize.getName())) {
			if (get) {
				return this.getElement().getStyle().getFontSize();
			} else {
				this.getElement().getStyle()
						.setFontSize(Double.parseDouble(param.substring(0, param.length() - 2)), Unit.PT);
				return "";
			}
		} else if (property.equals(Property.Pos.getName())) {
			if (get) {
				if (this.getParent() != null) {
					String pos = "" + (this.getAbsoluteLeft() - 1 - this.getParent().getAbsoluteLeft()) + "^"
							+ (this.getAbsoluteTop() - 1 - this.getParent().getAbsoluteTop());
					return pos;
				} else
					return "";
			} else {
				String[] pos = param.split("\\^");
				((AbsolutePanel) this.getParent()).setWidgetPosition(this, Integer.parseInt(pos[0]),
						Integer.parseInt(pos[1]));
				return "";
			}
		} else if (property.equals(Property.Height.getName())) {
			if (get) {
				String height = this.getElement().getStyle().getHeight();
				return height.substring(0, height.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setHeight(Double.parseDouble(param), Unit.PX);
				return "";
			}
		} else if (property.equals(Property.Width.getName())) {
			if (get) {
				String width = this.getElement().getStyle().getWidth();
				return width.substring(0, width.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setWidth(Double.parseDouble(param), Unit.PX);
				return "";
			}
		}

		return "DefaultValue";
	}

	@Override
	public String[][] getProperties() {
		String[][] list = new String[properties.length][2];
		for (int i = 0; i < properties.length; i++) {
			list[i][0] = properties[i].getName();
			list[i][1] = call(properties[i].getName(), "", true);
		}
		return list;
	}

	@Override
	public void setProperties(String propertyList) {
		String[] property = propertyList.split("`");
		for (String s : property) {
			if (!s.equals("")) {
				String[] prop = s.split("~");
				if (prop.length < 2)
					call(prop[0], "", false);
				else
					call(prop[0], prop[1], false);
			}
		}
	}

	@Override
	public String toString() {
		String output = "";
		for (Property p : properties) {
			output += "`" + p.getName() + "~" + call(p.getName(), "", true);
		}
		return type.getName() + "|" + this.id + "|" + output;
	}

	@Override
	public Widget getDragHandle() {
		// this widget is dragable anywhere on the widget so it does not need an extra drag handle.
		return null;
	}

}
