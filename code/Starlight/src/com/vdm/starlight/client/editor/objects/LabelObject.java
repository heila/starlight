package com.vdm.starlight.client.editor.objects;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RequiresResize;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Component that can be added to a {@link Workpage}. It represents one line of static text on the {@link Workpage}. The
 * text color, size and other {@link Property}(ies) of the {@link Component} can be set. The component is modeled using
 * a gwt {@link Label} object.
 * 
 * @author Heila van der Merwe
 * 
 */
public class LabelObject extends TextArea implements Component, RequiresResize {
	/**
	 * This is as a unique id of the component relative to the {@link Workpage} it is on. It is given to the component
	 * on creation and is stored and reloaded when the component is created. It uniquely identifies every component on
	 * the {@link Workpage} for when user input is saved for a component.
	 */
	private final int id;

	/** List of {@link Properties} that can be set by the user */
	private final Property[] properties = { Property.Text, Property.TextSize, Property.TextColour, Property.TextBold,
			Property.Pos, Property.Height, Property.Width, Property.BackgroundColor, Property.Align, Property.Layer };

	/**
	 * The type of the component. Each component type has a description and name given in the {@link ComponentType} enum
	 */
	private final ComponentType type = ComponentType.LABEL;

	@Override
	public int getId() {
		return this.id;
	}

	/**
	 * Constructor for a Label. Here all default style and other properties are set.
	 */
	public LabelObject(int id, boolean isEducator) {
		super();
		this.id = id;
		this.setText("Label");
		this.getElement().getStyle().setFontSize(14, Unit.PT);
		this.getElement().getStyle().setColor("black");
		this.getElement().getStyle().setZIndex(59);
		this.getElement().getStyle().setFontWeight(FontWeight.NORMAL);
		this.getElement().getStyle().setBorderColor("transparent");
		this.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		this.getElement().getStyle().setBorderWidth(1, Unit.PX);
		this.getElement().getStyle().setBackgroundColor("transparent");
		this.getElement().getStyle().setProperty("fontFamily", "Arial,Helvetica,sans-serif");
		this.setReadOnly(true);
		this.setWidth("100px");
		this.setHeight("25px");
		this.getElement().getStyle().setMargin(0, Unit.PX);
		this.getElement().getStyle().setProperty("textAlign", "left");
		this.getElement().getStyle().setOverflow(Overflow.HIDDEN);
		this.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				String position = call(Property.Pos.getName(), "", true);
				if (!position.equals("")) {
					String[] pos = position.split("\\^"); // width^height
					int height = Integer.parseInt(call(Property.Height.getName(), "", true));
					int width = Integer.parseInt(call(Property.Width.getName(), "", true));

					if (height + 2 + Integer.parseInt(pos[1]) > 600) {
						int newHeight = 600 - Integer.parseInt(pos[1]) - 4;
						call(Property.Height.getName(), String.valueOf(newHeight), false);
					}

					if (width + 2 + Integer.parseInt(pos[0]) > 950) {
						int newWidth = 950 - Integer.parseInt(pos[0]) - 4;
						call(Property.Width.getName(), String.valueOf(newWidth), false);
					}
				}
			}
		});

		if (!isEducator)
			this.getElement().getStyle().setProperty("resize", "none");

	}

	@Override
	public String call(String property, String param, boolean get) {
		if (property.equals(Property.Text.getName())) {
			if (get) {
				return this.getText();
			} else {
				this.setText(param);
				return "";
			}
		} else if (property.equals(Property.TextColour.getName())) {
			if (get) {
				return this.getElement().getStyle().getColor();
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setColor(param);
				return "";
			}
		} else if (property.equals(Property.Align.getName())) {
			if (get) {
				return this.getElement().getStyle().getProperty("textAlign");
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setProperty("textAlign", param);
				return "";
			}

		} else if (property.equals(Property.BackgroundColor.getName())) {
			if (get) {
				return this.getElement().getStyle().getBackgroundColor();
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setBackgroundColor(param);
				return "";
			}
		} else if (property.equals(Property.TextSize.getName())) {
			if (get) {
				return this.getElement().getStyle().getFontSize();
			} else {
				if (!param.equals(""))
					this.getElement().getStyle()
							.setFontSize(Double.parseDouble(param.substring(0, param.length() - 2)), Unit.PT);
				return "";
			}
		} else if (property.equals(Property.TextBold.getName())) {
			if (get) {
				String weight = this.getElement().getStyle().getFontWeight();
				if (weight.equals("") || weight.equals("normal"))
					return "normal";
				else
					return "bold";
			} else {
				if (!param.equals(""))
					if (param.equals("bold")) {
						this.getElement().getStyle().setFontWeight(FontWeight.BOLD);
					} else {
						this.getElement().getStyle().setFontWeight(FontWeight.NORMAL);
					}
				return "";
			}
		} else if (property.equals(Property.Layer.getName())) {
			if (get) {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				return String.valueOf(z);
			} else {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				String zIndex = this.getElement().getStyle().getZIndex();
				if (param.equals("foward")) {
					if (Integer.parseInt(zIndex) < 400)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) + 10);
				} else if (param.equals("back")) {
					if (Integer.parseInt(zIndex) > 0)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) - 10);
				} else {
					DOM.setIntStyleAttribute(this.getElement(), "zIndex", Integer.parseInt(param));
				}
				return "";
			}
		} else if (property.equals(Property.Pos.getName())) {
			if (get) {
				return "" + (this.getAbsoluteLeft() - 1 - this.getParent().getAbsoluteLeft()) + "^"
						+ (this.getAbsoluteTop() - 1 - this.getParent().getAbsoluteTop());
			} else {
				if (!param.equals("")) {
					String[] pos = param.split("\\^");
					((AbsolutePanel) this.getParent()).setWidgetPosition(this, Integer.parseInt(pos[0]),
							Integer.parseInt(pos[1]));
				}
				return "";
			}
		} else if (property.equals(Property.Height.getName())) {
			if (get) {
				String height = this.getElement().getStyle().getHeight();
				return height.substring(0, height.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setHeight(Double.parseDouble(param), Unit.PX);
				return "";
			}
		} else if (property.equals(Property.Width.getName())) {
			if (get) {
				String width = this.getElement().getStyle().getWidth();
				return width.substring(0, width.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setWidth(Double.parseDouble(param), Unit.PX);
				return "";
			}
		}

		return "DefaultValue";
	}

	@Override
	public void setProperties(String propertyList) {
		String[] property = propertyList.split("`");
		for (String s : property) {
			if (!s.equals("")) {
				String[] prop = s.split("~");
				if (prop.length < 2)
					call(prop[0], "", false);
				else
					call(prop[0], prop[1], false);
			}
		}
	}

	@Override
	public String[][] getProperties() {
		String[][] list = new String[properties.length][2];
		for (int i = 0; i < properties.length; i++) {
			list[i][0] = properties[i].getName();
			list[i][1] = call(properties[i].getName(), "", true);
		}
		return list;
	}

	@Override
	public String toString() {
		String output = "";
		for (Property p : properties) {
			output += ("`" + p.getName() + "~" + call(p.getName(), "", true));
		}
		return type.getName() + "|" + this.id + "|" + output;
	}

	@Override
	public Widget getDragHandle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHeight(String height) {
		height = height.substring(0, height.length() - 2);
		int h = Integer.parseInt(height); // new height
		if (h > 10 && h < 600) {
			if (this.getParent() != null) {
				String position = call(Property.Pos.getName(), "", true);
				String[] pos = position.split("\\^"); // width^height

				if (h + 2 + Integer.parseInt(pos[1]) > 600) {
					int newHeight = 600 - Integer.parseInt(pos[1]) - 2;
					super.setHeight(newHeight + "px");
				}
			} else
				super.setHeight(h + "px");

		} else
			super.setHeight(100 + "px");
	}

	@Override
	public void setWidth(String width) {
		width = width.substring(0, width.length() - 2);
		int h = Integer.parseInt(width); // new height
		if (h > 10 && h < 950) {
			if (this.getParent() != null) {
				String position = call(Property.Pos.getName(), "", true);
				String[] pos = position.split("\\^"); // width^height

				if (h + 2 + Integer.parseInt(pos[0]) > 950) {
					int newWidth = 950 - Integer.parseInt(pos[0]) - 2;
					super.setWidth(newWidth + "px");
				}
			} else
				super.setWidth(h + "px");
		} else
			super.setWidth(100 + "px");
	}

	@Override
	public void onResize() {
		String position = call(Property.Pos.getName(), "", true);
		if (!position.equals("")) {
			String[] pos = position.split("\\^"); // width^height
			int height = Integer.parseInt(call(Property.Height.getName(), "", true));
			int width = Integer.parseInt(call(Property.Width.getName(), "", true));

			if (height + 2 + Integer.parseInt(pos[1]) > 600) {
				int newHeight = 600 - Integer.parseInt(pos[1]) - 2;
				call(Property.Height.getName(), String.valueOf(newHeight), false);
			}

			if (width + 2 + Integer.parseInt(pos[0]) > 950) {
				int newWidth = 950 - Integer.parseInt(pos[0]) - 2;
				call(Property.Width.getName(), String.valueOf(newWidth), false);
			}

		}

	}

	// @Override
	// public void onResize(ResizeEvent event) {
	// String position = call(Property.Pos.getName(), "", true);
	// if (!position.equals("")) {
	// String[] pos = position.split("\\^"); // width^height
	// // ((AbsolutePanel) this.getParent()).setWidgetPosition(this, Integer.parseInt(pos[0]),
	// // Integer.parseInt(pos[1]));
	// if (event.getHeight() + 2 + Integer.parseInt(pos[1]) > 600) {
	// int newHeight = 600 - Integer.parseInt(pos[1]) - 2;
	// this.call(Property.Height.getName(), String.valueOf(newHeight), false);
	// }
	//
	// if (event.getWidth() + 2 + Integer.parseInt(pos[0]) > 950) {
	// int newWidth = 950 - Integer.parseInt(pos[0]) - 2;
	// this.call(Property.Width.getName(), String.valueOf(newWidth), false);
	// }
	//
	// }
	// }
}
