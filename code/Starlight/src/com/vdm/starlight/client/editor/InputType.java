package com.vdm.starlight.client.editor;

public enum InputType {

	String, Integer, Float;

}
