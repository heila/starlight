package com.vdm.starlight.client.editor.objects;

import com.google.gwt.dom.client.Style.BorderStyle;
import com.google.gwt.dom.client.Style.Overflow;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.shared.objects.Resource;
import com.vdm.starlight.shared.objects.Workpage;

/**
 * Component that can be added to a {@link Workpage}. It represents one line of static text on the {@link Workpage}. The
 * text color, size and other {@link Property}(ies) of the {@link Component} can be set. The component is modeled using
 * a gwt {@link Label} object.
 * 
 * @author Heila van der Merwe
 * 
 */
public class RectangleObject extends TextArea implements Component {
	/**
	 * This is as a unique id of the component relative to the {@link Workpage} it is on. It is given to the component
	 * on creation and is stored and reloaded when the component is created. It uniquely identifies every component on
	 * the {@link Workpage} for when user input is saved for a component.
	 */
	private final int id;

	/** List of {@link Properties} that can be set by the user */
	private final Property[] properties = { Property.BorderWidth, Property.BackgroundColor, Property.BorderColour,
			Property.Pos, Property.Width, Property.Height, Property.Layer };

	/**
	 * The type of the component. Each component type has a description and name given in the {@link ComponentType} enum
	 */
	private final ComponentType type = ComponentType.RECTANGLE;

	/** The id of the image {@link Resource} displayed by this Component. -1 indicates the default image */
	private int imageId = -1;
	public DatabaseServiceAsync dbService;

	/**
	 * Constructor for a Label. Here all default style and other properties are set.
	 */
	public RectangleObject(int id, DatabaseServiceAsync dbService, boolean isEducator) {
		super();
		this.id = id;
		this.getElement().getStyle().setZIndex(31);
		this.getElement().getStyle().setBorderColor("red");
		this.getElement().getStyle().setBorderStyle(BorderStyle.SOLID);
		this.getElement().getStyle().setBorderWidth(20, Unit.PX);
		this.getElement().getStyle().setBackgroundColor("transparent");
		this.getElement().getStyle().clearBackgroundImage();
		this.setWidth("200px");
		this.setHeight("200px");
		this.getElement().getStyle().setMargin(0, Unit.PX);
		this.getElement().getStyle().setOverflow(Overflow.HIDDEN);
		this.dbService = dbService;

		this.addMouseUpHandler(new MouseUpHandler() {
			@Override
			public void onMouseUp(MouseUpEvent event) {
				String position = call(Property.Pos.getName(), "", true);
				if (!position.equals("")) {
					String[] pos = position.split("\\^"); // width^height
					int height = Integer.parseInt(call(Property.Height.getName(), "", true));
					int width = Integer.parseInt(call(Property.Width.getName(), "", true));

					int borderWidth = Integer.parseInt(call(Property.BorderWidth.getName(), "", true));
					if (height + 4 + (1 * borderWidth) + Integer.parseInt(pos[1]) > 600) {
						int newHeight = 600 - Integer.parseInt(pos[1]) - 4 - (3 * borderWidth);
						call(Property.Height.getName(), String.valueOf(newHeight), false);
					}

					if (width + 4 + (1 * borderWidth) + Integer.parseInt(pos[0]) > 950) {
						int newWidth = 950 - Integer.parseInt(pos[0]) - 4 - (3 * borderWidth);
						call(Property.Width.getName(), String.valueOf(newWidth), false);
					}
				}
			}
		});
		if (!isEducator)
			this.getElement().getStyle().setProperty("resize", "none");

	}

	@Override
	public int getId() {
		return this.id;
	}

	@Override
	public void setProperties(String propertyList) {
		String[] property = propertyList.split("`");
		for (String s : property) {
			if (!s.equals("")) {
				String[] prop = s.split("~");
				if (prop.length < 2)
					call(prop[0], "", false);
				else
					call(prop[0], prop[1], false);
			}
		}
	}

	@Override
	public String call(String property, final String param, boolean get) {
		if (property.equals(Property.BorderColour.getName())) { // Border COlour
			if (get) {
				return this.getElement().getStyle().getBorderColor();
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setBorderColor(param);
				return "";
			}
		} else if (property.equals(Property.BackgroundColor.getName())) {
			if (get) {
				return this.getElement().getStyle().getBackgroundColor();
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setBackgroundColor(param);
				return "";
			}
		} else if (property.equals(Property.BorderWidth.getName())) {
			if (get) {
				String width = this.getElement().getStyle().getBorderWidth();
				return width.substring(0, width.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setBorderWidth(Double.parseDouble(param), Unit.PX);
				return "";
			}
		} else if (property.equals(Property.Layer.getName())) {
			if (get) {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				// String z = this.getElement().getStyle().getZIndex();
				return String.valueOf(z);
			} else {
				int z = DOM.getIntStyleAttribute(this.getElement(), "zIndex");
				String zIndex = this.getElement().getStyle().getZIndex();
				if (param.equals("foward")) {
					if (Integer.parseInt(zIndex) < 200)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) + 10);
				} else if (param.equals("back")) {
					if (Integer.parseInt(zIndex) > 0)
						this.getElement().getStyle().setZIndex(Integer.parseInt(zIndex) - 10);
				} else {
					DOM.setIntStyleAttribute(this.getElement(), "zIndex", Integer.parseInt(param));
				}
				return "";
			}
		} else if (property.equals(Property.Pos.getName())) {
			if (get) {
				return ""
						+ (this.getAbsoluteLeft() - Integer.parseInt(call(Property.BorderWidth.getName(), "", true))
								- this.getParent().getAbsoluteLeft() - 1)
						+ "^"
						+ (this.getAbsoluteTop() - Integer.parseInt(call(Property.BorderWidth.getName(), "", true))
								- this.getParent().getAbsoluteTop() - 1);
			} else {
				String[] pos = param.split("\\^");
				((AbsolutePanel) this.getParent()).setWidgetPosition(this,
						Integer.parseInt(pos[0]) + Integer.parseInt(call(Property.BorderWidth.getName(), "", true)),
						Integer.parseInt(pos[1]) + Integer.parseInt(call(Property.BorderWidth.getName(), "", true)));
				return "";
			}
		} else if (property.equals(Property.Height.getName())) {
			if (get) {
				String height = this.getElement().getStyle().getHeight();
				return height.substring(0, height.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setHeight(Double.parseDouble(param), Unit.PX);
				return "";
			}
		} else if (property.equals(Property.Width.getName())) {
			if (get) {
				String width = this.getElement().getStyle().getWidth();
				return width.substring(0, width.length() - 2);
			} else {
				if (!param.equals(""))
					this.getElement().getStyle().setWidth(Double.parseDouble(param), Unit.PX);
				return "";
			}
		}

		return "DefaultValue";
	}

	@Override
	public String[][] getProperties() {
		String[][] list = new String[properties.length][2];
		for (int i = 0; i < properties.length; i++) {
			list[i][0] = properties[i].getName();
			list[i][1] = call(properties[i].getName(), "", true);
		}
		return list;
	}

	@Override
	public String toString() {
		String output = "";
		for (Property p : properties) {
			output += ("`" + p.getName() + "~" + call(p.getName(), "", true));
		}
		return type.getName() + "|" + this.id + "|" + output;
	}

	public void setImage(int imageId, String imageString, int width, int height) {
		this.getElement().getStyle().setBackgroundImage(imageString);
		if (width != -1)
			this.getElement().getStyle().setWidth(width, Unit.PX);
		if (height != -1)
			this.getElement().getStyle().setHeight(height, Unit.PX);
		this.imageId = imageId;
	}

	@Override
	public Widget getDragHandle() {
		// TODO Auto-generated method stub
		return null;
	}
}
