package com.vdm.starlight.client.editor.objects;

import com.google.gwt.user.client.ui.Widget;

/**
 * Represents a Component on the workbook
 * 
 * @author heilamvdm
 * 
 */
public interface Component {
	/**
	 * Returns a two dimensional array of {@link Property} name - value pairs.
	 * 
	 * @return the array of {@link Property} name-value pairs.
	 */
	public String[][] getProperties();

	/**
	 * Sets the {@link LableObject}'s {@link Property}'s from a string representation.
	 * 
	 * @param propertyList
	 *            the string containing {@link Property} name-value pairs.
	 */
	public void setProperties(String propertyList);

	/**
	 * Calls the {@code property} getter or setter with {@code param} parameter. If {@code get} is set to true, the
	 * getter is called otherwise the setter of the property is called.
	 * 
	 * @param property
	 *            The property of the Component.
	 * @param param
	 *            The param to the getter/setter.
	 * @param get
	 *            True if the getter is to be called.
	 * @return empty string or the return value of the getter.
	 */
	public String call(String methodname, String params, boolean get);

	public Widget getDragHandle();

	@Override
	public String toString();

	public int getId();
}
