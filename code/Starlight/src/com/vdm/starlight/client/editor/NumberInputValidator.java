package com.vdm.starlight.client.editor;

import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.HasText;

public class NumberInputValidator implements KeyUpHandler {
	HasText textBox;
	InputType type;

	public NumberInputValidator(HasText textbox, InputType type) {
		this.textBox = textbox;
		this.type = type;
	}

	public void onKeyUp(KeyUpEvent event) {
		if (type.equals(InputType.Integer)) {
			String text = textBox.getText();
			if ((event.getNativeKeyCode() < 48 || event.getNativeKeyCode() > 57)
					&& event.getNativeKeyCode() != KeyCodes.KEY_ENTER
					&& event.getNativeKeyCode() != KeyCodes.KEY_BACKSPACE
					&& event.getNativeKeyCode() != KeyCodes.KEY_DELETE) {
				textBox.setText(text.substring(0, text.length() - 1));
			}
		} else if (type.equals(InputType.Float)) {
			String text = textBox.getText();
			if ((event.getNativeKeyCode() < 48 || event.getNativeKeyCode() > 57)
					&& event.getNativeKeyCode() != KeyCodes.KEY_ENTER
					&& event.getNativeKeyCode() != KeyCodes.KEY_BACKSPACE
					&& event.getNativeKeyCode() != KeyCodes.KEY_DELETE) {
				textBox.setText(text.substring(0, text.length() - 1));
			}
		}
	}
}
