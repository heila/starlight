package com.vdm.starlight.client.view;

import com.google.gwt.user.client.ui.Widget;

public interface LogInView {

	public interface Presenter {
		void onLogInButtonClicked(String name, String password, boolean isEducator);
	}

	void setPresenter(Presenter presenter);

	Widget asWidget();
}