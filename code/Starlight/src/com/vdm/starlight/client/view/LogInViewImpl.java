package com.vdm.starlight.client.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.LoginResources;

public class LogInViewImpl extends Composite implements LogInView {

	@UiTemplate("LogInWidget.ui.xml")
	interface LogInViewUiBinder extends UiBinder<Widget, LogInViewImpl> {
	}

	private static LogInViewUiBinder uiBinder = GWT.create(LogInViewUiBinder.class);

	static {
		LoginResources.INSTANCE.loginCSS().ensureInjected();
	}

	@UiField
	Button btnLogIn;
	@UiField
	TextBox tbxName;
	@UiField
	TextBox tbxPassword;
	@UiField
	Label lblStatus;

	private Presenter presenter;

	public LogInViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		// TODO validate input

		tbxName.setTabIndex(1);
		tbxPassword.setTabIndex(2);
		btnLogIn.setTabIndex(3);

		tbxPassword.addKeyDownHandler(new KeyDownHandler() {

			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER && presenter != null) {
					String name = tbxName.getText();
					String password = tbxPassword.getText();
					boolean isEducator = true;
					clearText();
					presenter.onLogInButtonClicked(name, password, isEducator);
				}

			}
		});
	}

	@UiHandler("btnLogIn")
	void onLogInButtonClicked(ClickEvent event) {
		if (presenter != null) {
			String name = tbxName.getText();
			String password = tbxPassword.getText();
			boolean isEducator = true;

			clearText();
			presenter.onLogInButtonClicked(name, password, isEducator);
		}
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
	}

	/**
	 * Resets the text boxes
	 */
	public void clearText() {
		tbxName.setText("");
		tbxPassword.setText("");
		tbxName.setFocus(true);
		tbxName.setSelectionRange(0, 0);
		tbxName.setCursorPos(0);
	}

	public void setStatus(String text) {
		lblStatus.setText(text);
	}

	public void clearStatus() {
		lblStatus.setText("");

	}

	public void setFocus() {
		tbxName.setFocus(true);
		tbxName.setSelectionRange(0, 0);
		tbxName.setCursorPos(0);

	}

	@Override
	public Widget asWidget() {
		return this;
	}

}
