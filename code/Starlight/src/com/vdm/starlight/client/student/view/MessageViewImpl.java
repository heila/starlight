package com.vdm.starlight.client.student.view;

import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.EmoticonImages;

public class MessageViewImpl extends Composite implements MessageView {
	static Logger logger = Logger.getLogger(" MessageViewImpl");
	static EmoticonImages resources = EmoticonImages.INSTANCE;

	@UiTemplate("MessageView.ui.xml")
	interface MessageViewUiBinder extends UiBinder<Widget, MessageViewImpl> {
	}

	private static MessageViewUiBinder uiBinder = GWT.create(MessageViewUiBinder.class);

	@UiField
	public DialogBox messageDialog;

	Presenter presenter;
	@UiField
	public FlowPanel emoticons;

	@UiField
	public Button cancelButton;
	@UiField
	public Button sendButton;
	@UiField
	RichTextArea messageBox;

	public MessageViewImpl() {
		uiBinder.createAndBindUi(this);
		messageDialog.getElement().getStyle().setZIndex(200);
		emoticons.getElement().getStyle().setProperty("float", "left");
		setEmoticons();

	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	@Override
	public Widget asWidget() {
		messageDialog.center();
		messageBox.setFocus(true);
		DeferredCommand.add(new Command() {
			public void execute() {
				messageBox.setFocus(true);
			}
		});
		return this;
	}

	public void setEmoticons() {
		Image im = new Image(resources.angry().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.frustrated().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.hungry().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sad().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sick2().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.ashamed().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.greet2().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.music().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.schocked2().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sick().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.birthday().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.greet().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.no().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.shocked2().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sleep().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.confused().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.happy().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sad2().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.shocked().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.thirsty().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.eat().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.hug().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.sad3().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.shy().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

		im = new Image(resources.toilet().getSafeUri());
		im.addClickHandler(new EmoticonClick());
		im.getElement().getStyle().setPadding(4, Unit.PX);
		emoticons.add(im);

	}

	public class EmoticonClick implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			messageBox.setHTML(messageBox.getHTML() + "<img src=\"" + ((Image) event.getSource()).getUrl()
					+ "\" style='padding: 4px;' />");
		}

	}

	@UiHandler("cancelButton")
	void onCancelButtonClick(ClickEvent event) {
		messageDialog.hide();
		messageBox.setHTML("");
	}

	@UiHandler("sendButton")
	void onSendButtonClick(ClickEvent event) {
		presenter.sendMessage(messageBox.getHTML());
		messageBox.setHTML("");
		messageDialog.hide();

	}
}
