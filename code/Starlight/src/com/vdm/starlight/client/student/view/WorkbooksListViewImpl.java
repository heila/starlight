package com.vdm.starlight.client.student.view;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.StudentResources;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public class WorkbooksListViewImpl extends Composite implements WorkbooksListView {
	static Logger logger = Logger.getLogger("WorkbooksListViewImpl");
	static StudentResources resources = StudentResources.INSTANCE;

	/** The default list size. */
	private static final int DEFAULT_NUM_COLS = 4;

	@UiTemplate("WorkbooksListView.ui.xml")
	interface WorkbooksListViewUiBinder extends UiBinder<Widget, WorkbooksListViewImpl> {
	}

	private static WorkbooksListViewUiBinder uiBinder = GWT.create(WorkbooksListViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		StudentResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of WorkbookInfo objects */
	@UiField(provided = true)
	ScrollPanel WorkbookList;
	@UiField
	Anchor classListLink;
	FlowPanel flow;

	@UiField
	Anchor exitLink;
	private Presenter presenter;

	public WorkbooksListViewImpl() {
		WorkbookList = new ScrollPanel();
		initWidget(uiBinder.createAndBindUi(this));
		flow = new FlowPanel();
		flow.setStyleName(resources.starlightCSS().flowPanel());
		WorkbookList.add(flow);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		flow.clear();
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public void newDataToDisplay(List<WorkbookDTO> newList) {
		for (int i = 0; i < newList.size(); i++) {
			flow.add(new WorkbookCell(newList.get(i)));
		}
	}

	@Override
	public void clear() {
	}

	/**
	 * The Cell used to render a Workbook cell
	 */
	public class WorkbookCell extends VerticalPanel {

		WorkbookDTO info;

		public WorkbookCell(WorkbookDTO info) {
			this.info = info;

			Image image = new Image();
			if (info.isDefaultImage()) {
				ImageResource im = resources.deur();
				image.setUrl(im.getSafeUri());

			} else {
				image.setUrl(info.getImage());
			}
			image.setHeight("250px");
			image.getElement().getStyle().setProperty("margin", "0 auto");
			this.setStyleName(resources.starlightCSS().imageAnimation());
			Label name = new Label(info.getName());
			name.getElement().getStyle().setColor("white");
			name.getElement().getStyle().setFontSize(30, Unit.PX);
			name.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
			name.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
			name.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			this.add(image);
			this.add(name);
			image.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					if (WorkbookCell.this.info.getImage() != null) {
						presenter.workbookClicked(WorkbookCell.this.info.getId(), WorkbookCell.this.info.getName(),
								WorkbookCell.this.info.getImage());
					} else {
						ImageResource im = resources.deur();
						presenter.workbookClicked(WorkbookCell.this.info.getId(), WorkbookCell.this.info.getName(),
								im.getURL());
					}
				}
			});

		}

		public WorkbookDTO getWorkbookInfo() {
			return info;
		}
	}

	@UiHandler("classListLink")
	void onClassListLinkClick(ClickEvent event) {
		presenter.goBack();
	}

	@UiHandler("exitLink")
	void onExitLinkClick(ClickEvent event) {
		presenter.logoutItemClicked();
	}
}
