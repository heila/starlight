package com.vdm.starlight.client.student.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.view.client.AsyncDataProvider;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.editor.ComponentType;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.StudentsEvent;
import com.vdm.starlight.client.events.StudentsEvent.StudentEventType;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.resources.StudentResources;
import com.vdm.starlight.client.student.view.WorkbookView;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkpageDataDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class WorkbookViewPresenter implements Presenter, WorkbookView.Presenter {
	Logger logger = Logger.getLogger(" WorkbookViewPresenter");

	static {
		/** Injects css into uibinder view. */
		StudentResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Student */
	private int workbookId;

	/** View */
	private final WorkbookView view;

	private int pagenumber;
	private int numPages;
	private int studentId;
	private int workpageId;
	private int classId;

	AsyncDataProvider<StudentDTO> dataProvider;

	public WorkbookViewPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, WorkbookView workbookView,
			int studentId, int workbookId, int classId, String name, String image) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookView;
		view.setName(name, image);
		this.view.setPresenter(this);
		this.workbookId = workbookId;
		this.studentId = studentId;
		this.classId = classId;
		bind();

	}

	public void bind() {
		this.pagenumber = 1; // show first page
		getNumberOfPages(); // get the page count to display
		getWorkpage(); // get the workpage to display and stores the workpageid
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(view.asWidget());

	}

	public void getWorkpage() {
		dbService.getWorkpage(workbookId, pagenumber, new AsyncCallback<WorkpageDataDTO>() {
			public void onSuccess(WorkpageDataDTO result) {
				if (result.getData() != null) {
					view.viewPage(result.getData(), pagenumber);
					workpageId = result.getWorkpageId();
					getStudentData();
				}
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' workpages list" + caught.getMessage());
			}
		});
	}

	private void getNumberOfPages() {
		// get number of page in this book an update label
		dbService.getNumberOfPages(workbookId, new AsyncCallback<Integer>() {
			public void onSuccess(Integer result) {
				numPages = result;
				view.setNumPages(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' workpages list" + caught.getMessage());
			}
		});

	}

	@Override
	public Widget getComponent(String[] description) {
		ComponentType t = ComponentType.getCompoType(description[0]);
		return t.getStudentWidget(Integer.parseInt(description[1]), dbService);
	}

	@Override
	public void getNextPage() {
		if (pagenumber < numPages) {
			pagenumber += 1;
			getWorkpage();
		} else
			Window.alert("No next pages");

	}

	@Override
	public void getPreviousPage() {
		if (pagenumber > 1) {
			pagenumber -= 1;
			getWorkpage();
		} else
			Window.alert("No previous pages");
	}

	@Override
	public void showComment(String comment) {
		eventBus.fireEvent(new StudentsEvent(StudentEventType.MESSAGE, 0, 0, 0, comment, null));

	}

	@Override
	public void saveStudentData(String data) {
		// get number of page in this book an update label
		dbService.setStudentWorkpageData(data, studentId, workpageId, new AsyncCallback<Void>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Not Saved");

			}

			@Override
			public void onSuccess(Void result) {

			}
		});

	}

	@Override
	public void getStudentData() {
		// get number of page in this book an update label
		dbService.getWorkpageStudentData(studentId, workpageId, new AsyncCallback<List<String>>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(List<String> result) {
				view.setStudentData(result);

			}
		});
		// get number of page in this book an update label
		dbService.getWorkpageComment(studentId, workpageId, new AsyncCallback<String>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(String comment) {
				if (!comment.trim().equals(""))
					eventBus.fireEvent(new StudentsEvent(StudentEventType.MESSAGE, 0, 0, 0, comment, ""));

			}
		});

	}

	@Override
	public void goBack() {
		eventBus.fireEvent(new StudentsEvent(StudentsEvent.StudentEventType.WORKBOOKS, studentId, classId, 0, "", null));

	}

	@Override
	public void logoutItemClicked() {
		eventBus.fireEvent(new LogOutEvent());

	}

}
