package com.vdm.starlight.client.student.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.events.MessageEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.resources.StudentResources;
import com.vdm.starlight.client.student.view.MessageView;
import com.vdm.starlight.shared.dto.StudentDTO;

/**
 * 
 * @author Heila van der Merwe
 * 
 */
public class MessageViewPresenter implements Presenter, MessageView.Presenter {
	Logger logger = Logger.getLogger(" WorkbookViewPresenter");

	static {
		/** Injects css into uibinder view. */
		StudentResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** Registration of global events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** View */
	private final MessageView view;

	AsyncDataProvider<StudentDTO> dataProvider;

	public MessageViewPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, MessageView workbookView) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = workbookView;
		this.view.setPresenter(this);

	}

	@Override
	public void go(HasWidgets container) {

	}

	@Override
	public void sendMessage(String html) {
		eventBus.fireEvent(new MessageEvent(html, "", null));
	}
}
