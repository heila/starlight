package com.vdm.starlight.client.student.presenter;

import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.MessagingServiceAsync;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.MessageEvent;
import com.vdm.starlight.client.events.MessageEventHandler;
import com.vdm.starlight.client.events.StudentsEvent;
import com.vdm.starlight.client.events.StudentsEventHandler;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.student.view.ClassroomsListView;
import com.vdm.starlight.client.student.view.ClassroomsListViewImpl;
import com.vdm.starlight.client.student.view.MessageView;
import com.vdm.starlight.client.student.view.StudentHomeView;
import com.vdm.starlight.client.student.view.StudentHomeViewImpl;
import com.vdm.starlight.client.student.view.WorkbookView;
import com.vdm.starlight.client.student.view.WorkbookViewImpl;
import com.vdm.starlight.client.student.view.WorkbooksListView;
import com.vdm.starlight.client.student.view.WorkbooksListViewImpl;
import com.vdm.starlight.shared.dto.StudentDTO;

public class StudentHomePresenter implements Presenter, StudentHomeView.Presenter {
	Logger logger = Logger.getLogger("ProfessorHomePresenter");

	public int studentId;
	public StudentDTO student;

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	private final MessagingServiceAsync messageService;

	/** Sub widgets */
	private final StudentHomeViewImpl view;

	/* *************************** WorkbookViews **************************************** */
	private WorkbookView workbookView;
	private WorkbooksListView worbooksListView;

	/* *************************** ClassroomViews **************************************** */
	// private ClassroomView classroomView;
	private ClassroomsListView classroomsListView;

	/* *************************** Constructor **************************************** */
	public StudentHomePresenter(DatabaseServiceAsync rpcService, MessagingServiceAsync messageService,
			HandlerManager eventBus, StudentHomeViewImpl homeView, int studentId) {
		this.studentId = studentId;
		this.dbService = rpcService;
		this.messageService = messageService;
		this.eventBus = eventBus;
		this.view = homeView;
		this.view.setPresenter(this);
		bind();
		dbService.getStudent(studentId, false, new AsyncCallback<StudentDTO>() {

			@Override
			public void onFailure(Throwable caught) {

			}

			@Override
			public void onSuccess(StudentDTO result) {
				student = result;

			}
		});

	}

	private void bind() {
		eventBus.addHandler(StudentsEvent.TYPE, new StudentsEventHandler() {
			@Override
			public void studentsEvent(StudentsEvent event) {
				if (event.getType().equals(StudentsEvent.StudentEventType.WORKBOOKS)) {
					if (worbooksListView == null) {
						worbooksListView = new WorkbooksListViewImpl();
					}
					new WorkbooksListPresenter(dbService, eventBus, worbooksListView, event.getStudentId(), event
							.getClassId()).go(view.getPageContainer());

				} else if (event.getType().equals(StudentsEvent.StudentEventType.VIEW)) {
					if (workbookView == null) {
						workbookView = new WorkbookViewImpl();
					}
					new WorkbookViewPresenter(dbService, eventBus, workbookView, event.getStudentId(), event
							.getWorkbookId(), event.getClassId(), event.getComment(), event.getImage()).go(view
							.getPageContainer());

				} else if (event.getType().equals(StudentsEvent.StudentEventType.MESSAGE)) {
					view.displayTooltip(event.getComment());

				} else if (event.getType().equals(StudentsEvent.StudentEventType.CLASSROOMS)) {
					if (classroomsListView == null) {
						classroomsListView = new ClassroomsListViewImpl();

					}
					new ClassroomsListPresenter(dbService, eventBus, classroomsListView, studentId).go(view
							.getPageContainer());
				}
			}
		});

		eventBus.addHandler(MessageEvent.TYPE, new MessageEventHandler() {

			@Override
			public void message(final MessageEvent event) {
				if (event.getType() == com.vdm.starlight.client.events.MessageType.RECEIVE) {
					view.addMessage(event.getMessage(), event.getReceiver());
				} else {
					messageService.send(event.getMessage(), event.getReceiver(), new AsyncCallback<Void>() {

						@Override
						public void onFailure(Throwable caught) {
							// TODO Auto-generated method stub
						}

						@Override
						public void onSuccess(Void result) {
							view.addMessage(event.getMessage(), event.getReceiver());
						}

					});

				}
			}
		});

	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		if (classroomsListView == null) {
			classroomsListView = new ClassroomsListViewImpl();

		}
		new ClassroomsListPresenter(dbService, eventBus, classroomsListView, studentId).go(view.getPageContainer());
		container.add(view.asWidget());

	}

	@Override
	public void classroomsItemClicked() {
		if (classroomsListView == null) {
			classroomsListView = new ClassroomsListViewImpl();

		}
		new ClassroomsListPresenter(dbService, eventBus, classroomsListView, studentId).go(view.getPageContainer());

	}

	@Override
	public void logoutItemClicked() {
		eventBus.fireEvent(new LogOutEvent());
	}

	@Override
	public void showMessageDialog(MessageView messageView) {
		new MessageViewPresenter(this.dbService, this.eventBus, messageView);
		messageView.asWidget();

	}

}
