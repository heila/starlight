package com.vdm.starlight.client.student.view;

import com.google.gwt.user.client.ui.Widget;

public interface MessageView {

	public interface Presenter {

		void sendMessage(String html);

	}

	Widget asWidget();

	void setPresenter(Presenter presenter);
}