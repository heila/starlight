package com.vdm.starlight.client.student.view;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.Property;
import com.vdm.starlight.client.editor.objects.Component;
import com.vdm.starlight.client.resources.StudentResources;

public class WorkbookViewImpl extends Composite implements WorkbookView {

	static StudentResources res = StudentResources.INSTANCE;

	@UiTemplate("WorkbookView.ui.xml")
	interface WorkbookViewUiBinder extends UiBinder<Widget, WorkbookViewImpl> {
	}

	private static WorkbookViewUiBinder uiBinder = GWT.create(WorkbookViewUiBinder.class);
	// @UiField
	// HorizontalPanel pagerPanel;

	@UiField
	AbsolutePanel targetPanel;

	Presenter presenter;

	@UiField
	Label nameLabel;

	@UiField
	Image image;
	@UiField
	Anchor WorkbooksLink;
	@UiField
	Anchor exitLink;
	@UiField
	Anchor prevPageLink;
	@UiField
	Anchor nextPageLink;
	@UiField
	Label pageNumber;

	/**
	 * // * The label that shows the current range. //
	 */
	// private final Anchor nextLabel = new Anchor("Next");
	// /**
	// * The label that shows the current range.
	// */
	// private final Anchor prevLabel = new Anchor("Prev");

	private String pageLabel = "Page 1 of ";
	private String page2Label = "";

	// Components on a page
	private List<Component> components = new ArrayList<Component>();

	public WorkbookViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@Override
	public void viewPage(List<String> widgets, int pagenum) {
		targetPanel.clear();
		components.clear();
		if (widgets.size() != 0) {
			for (String w : widgets) {
				addComponent(w);
			}
		}
		pageLabel = "Page " + pagenum + " of ";
		pageNumber.setText(pageLabel + page2Label);
	}

	/**
	 * Parses the String parameter into a component and places it onscreen
	 * 
	 * @param component
	 */
	public void addComponent(String component) {
		String[] description = component.split("\\|");
		Widget widget = presenter.getComponent(description);
		targetPanel.add(widget);
		((Component) widget).setProperties(description[2]);
		components.add((Component) widget);
	}

	@Override
	public void setNumPages(Integer result) {
		page2Label = " " + String.valueOf(result);
	}

	@Override
	public void saveStudentData() {
		String output = "";
		for (Component c : components) { // loop through all widgets
			String dataComp = c.call(Property.Input.getName(), "", true);
			if (!dataComp.equals("DefaultValue")) {
				output += c.getId() + "|" + dataComp + "$";
			}
		}
		presenter.saveStudentData(output);
	}

	@Override
	public void setStudentData(List<String> dataDescription) {
		if (dataDescription != null) { // no data was filled in yet
			for (int i = 0; i < dataDescription.size(); i++) {
				String[] option = dataDescription.get(i).split("\\|");
				int id = Integer.parseInt(option[0].trim());
				String description = option[1];
				Component c = get(id);
				c.call(Property.Input.getName(), description, false); // setdata
			}
		}
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;

	}

	public Component get(int componentId) {
		for (Component c : components) {
			if (c.getId() == componentId) {
				return c;
			}
		}
		return null;
	}

	@UiHandler("WorkbooksLink")
	void onWorkbooksLinkClick(ClickEvent event) {
		presenter.goBack();
	}

	@UiHandler("prevPageLink")
	void onPrevPageLinkClick(ClickEvent event) {
		saveStudentData();
		presenter.getPreviousPage();
	}

	@UiHandler("nextPageLink")
	void onNextPageLinkClick(ClickEvent event) {
		saveStudentData();
		presenter.getNextPage();
	}

	@UiHandler("exitLink")
	void onExitLinkClick(ClickEvent event) {
		saveStudentData();
		presenter.logoutItemClicked();
	}

	@Override
	public void setName(String name, String image) {
		this.nameLabel.setText(name);
		this.image.setUrl("data:image/" + image);

	}
}
