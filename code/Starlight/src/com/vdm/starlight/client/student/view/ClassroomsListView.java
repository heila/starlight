package com.vdm.starlight.client.student.view;

import java.util.List;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public interface ClassroomsListView {

	public interface Presenter {

		void classClicked(int id);

		void logoutItemClicked();

	}

	void setPresenter(Presenter presenter);

	public void newDataToDisplay(List<ClassroomDTO> result);

	Widget asWidget();

	void clear();
}