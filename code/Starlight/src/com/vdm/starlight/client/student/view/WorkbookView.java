package com.vdm.starlight.client.student.view;

import java.util.List;

import com.google.gwt.user.client.ui.Widget;

public interface WorkbookView {

	public interface Presenter {

		public Widget getComponent(String[] description);

		public void getPreviousPage();

		public void getNextPage();

		void showComment(String comment);

		void saveStudentData(String data);

		void getStudentData();

		public void goBack();

		public void logoutItemClicked();

	}

	Widget asWidget();

	void viewPage(List<String> widgets, int pagenumber);

	void setNumPages(Integer result);

	void saveStudentData();

	public void setStudentData(List<String> dataDescription);

	void setPresenter(Presenter presenter);

	void setName(String name, String image);
}