package com.vdm.starlight.client.student.view;

import com.google.gwt.user.client.ui.Widget;

public interface StudentHomeView {

	public interface Presenter {

		public void classroomsItemClicked();

		public void logoutItemClicked();

		public void showMessageDialog(MessageView messageView);

	}

	void setPresenter(Presenter presenter);

	Widget asWidget();
}