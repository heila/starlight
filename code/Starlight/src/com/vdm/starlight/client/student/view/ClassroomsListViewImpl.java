package com.vdm.starlight.client.student.view;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.FontStyle;
import com.google.gwt.dom.client.Style.FontWeight;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.resources.StudentResources;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomsListViewImpl extends Composite implements ClassroomsListView {
	static Logger logger = Logger.getLogger("ClassroomsListViewImpl");
	static StudentResources resources = StudentResources.INSTANCE;

	@UiTemplate("ClassroomsListView.ui.xml")
	interface ClassroomsListViewUiBinder extends UiBinder<Widget, ClassroomsListViewImpl> {
	}

	private static ClassroomsListViewUiBinder uiBinder = GWT.create(ClassroomsListViewUiBinder.class);

	static {
		/** Injects css into uibinder view. */
		StudentResources.INSTANCE.starlightCSS().ensureInjected();
	}

	/** CellList of ClassroomDTO objects */
	@UiField(provided = true)
	ScrollPanel classroomList;
	FlowPanel flow;
	private Presenter presenter;

	@UiField
	Anchor exitLink;

	public ClassroomsListViewImpl() {
		classroomList = new ScrollPanel();
		initWidget(uiBinder.createAndBindUi(this));
		flow = new FlowPanel();
		flow.setStyleName(resources.starlightCSS().flowPanel());
		classroomList.add(flow);
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		flow.clear();
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public void newDataToDisplay(List<ClassroomDTO> newList) {
		for (int i = 0; i < newList.size(); i++) {
			flow.add(new ClassroomCell(newList.get(i)));
		}
	}

	@Override
	public void clear() {
	}

	/**
	 * The Cell used to render a classroom cell
	 */
	public class ClassroomCell extends VerticalPanel {

		ClassroomDTO info;

		public ClassroomCell(ClassroomDTO info) {
			this.info = info;

			Image image = new Image();
			if (info.isDefaultImage()) {
				ImageResource im = resources.deur();
				image.setUrl(im.getSafeUri());

			} else {
				image.setUrl(info.getImage());
			}
			image.setHeight("250px");
			image.getElement().getStyle().setProperty("margin", "0 auto");
			this.setStyleName(resources.starlightCSS().imageAnimation());
			Label name = new Label(info.getName());
			name.getElement().getStyle().setColor("white");
			name.getElement().getStyle().setFontSize(28, Unit.PX);
			name.getElement().getStyle().setFontStyle(FontStyle.NORMAL);
			name.getElement().getStyle().setFontWeight(FontWeight.BOLDER);
			name.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);
			this.add(image);
			this.add(name);
			image.addClickHandler(new ClickHandler() {

				@Override
				public void onClick(ClickEvent event) {
					presenter.classClicked(ClassroomCell.this.info.getId());

				}
			});

		}

		public ClassroomDTO getClassroomInfo() {
			return info;
		}
	}

	@UiHandler("exitLink")
	void onExitLinkClick(ClickEvent event) {
		presenter.logoutItemClicked();
	}

}
