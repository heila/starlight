package com.vdm.starlight.client.student.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.StudentsEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.student.view.WorkbooksListView;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public class WorkbooksListPresenter implements Presenter, WorkbooksListView.Presenter {
	Logger logger = Logger.getLogger("ProfessorHomePresenter");

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int studentId;

	/** View */
	private final WorkbooksListView view;

	AsyncDataProvider<WorkbookDTO> dataProvider;

	private final int classId;

	public WorkbooksListPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, WorkbooksListView view,
			final int stduentId, int classId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = view;
		this.view.setPresenter(this);
		this.studentId = stduentId;
		this.classId = classId;
		bind();
	}

	public void bind() {
		final int start = 0;
		int end = 1000;

		dbService.getStudentWorkbooks(studentId, classId, true, new AsyncCallback<List<WorkbookDTO>>() {
			public void onSuccess(List<WorkbookDTO> result) {
				if (result.size() != 0)
					WorkbooksListPresenter.this.view.newDataToDisplay(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' students details" + caught.getMessage());
			}
		});
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		view.clear(); // clear selection
		container.add(view.asWidget());

	}

	@Override
	public void workbookClicked(int workbookId, String name, String image) {
		eventBus.fireEvent(new StudentsEvent(StudentsEvent.StudentEventType.VIEW, studentId, classId, workbookId, name,
				image));

	}

	@Override
	public void goBack() {
		eventBus.fireEvent(new StudentsEvent(StudentsEvent.StudentEventType.CLASSROOMS, studentId, classId, 0, "", ""));

	}

	@Override
	public void logoutItemClicked() {
		eventBus.fireEvent(new LogOutEvent());

	}

}
