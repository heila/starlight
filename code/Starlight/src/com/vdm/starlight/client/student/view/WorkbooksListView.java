package com.vdm.starlight.client.student.view;

import java.util.List;

import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.shared.dto.WorkbookDTO;

public interface WorkbooksListView {

	public interface Presenter {
		void workbookClicked(int workbookId, String name, String image);

		void goBack();

		void logoutItemClicked();
	}

	void setPresenter(Presenter presenter);

	public void newDataToDisplay(List<WorkbookDTO> result);

	Widget asWidget();

	void clear();
}