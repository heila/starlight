package com.vdm.starlight.client.student.presenter;

import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.view.client.AsyncDataProvider;
import com.vdm.starlight.client.DatabaseServiceAsync;
import com.vdm.starlight.client.events.LogOutEvent;
import com.vdm.starlight.client.events.StudentsEvent;
import com.vdm.starlight.client.presenter.Presenter;
import com.vdm.starlight.client.student.view.ClassroomsListView;
import com.vdm.starlight.shared.dto.ClassroomDTO;

public class ClassroomsListPresenter implements Presenter, ClassroomsListView.Presenter {
	Logger logger = Logger.getLogger("ClassroomsListPresenter");

	/** Listens for registered events */
	private final HandlerManager eventBus;

	/** Database Connection */
	private final DatabaseServiceAsync dbService;

	/** The logged in Educator */
	private int studentId;

	/** View */
	private final ClassroomsListView view;

	AsyncDataProvider<ClassroomDTO> dataProvider;

	public ClassroomsListPresenter(DatabaseServiceAsync rpcService, HandlerManager eventBus, ClassroomsListView view,
			final int stduentId) {
		this.dbService = rpcService;
		this.eventBus = eventBus;
		this.view = view;
		this.view.setPresenter(this);
		this.studentId = stduentId;
		bind();

	}

	public void bind() {
		final int start = 0;
		int end = 1000;

		dbService.getStudentClassrooms(studentId, true, new AsyncCallback<List<ClassroomDTO>>() {
			public void onSuccess(List<ClassroomDTO> result) {
				if (result.size() != 0)
					ClassroomsListPresenter.this.view.newDataToDisplay(result);
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error fetching class' students details" + caught.getMessage());
			}
		});
	}

	@Override
	public void go(HasWidgets container) {
		container.clear();
		view.clear(); // clear selection
		container.add(view.asWidget());

	}

	@Override
	public void classClicked(int classId) {
		eventBus.fireEvent(new StudentsEvent(StudentsEvent.StudentEventType.WORKBOOKS, studentId, classId, 0, "", null));

	}

	@Override
	public void logoutItemClicked() {
		eventBus.fireEvent(new LogOutEvent());

	}

}
