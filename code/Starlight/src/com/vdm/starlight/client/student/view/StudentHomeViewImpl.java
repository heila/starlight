package com.vdm.starlight.client.student.view;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Position;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.uibinder.client.UiTemplate;
import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.DeferredCommand;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.AbsolutePanel;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.RichTextArea;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.vdm.starlight.client.editor.ToolTip;
import com.vdm.starlight.client.resources.StudentResources;

/**
 * Home view of the Student. Contains the menubar and manages all change of content on the page.
 * 
 * @author Heila van der Merwe
 * 
 */
public class StudentHomeViewImpl extends Composite implements StudentHomeView {

	@UiTemplate("StudentHomeView.ui.xml")
	interface StudentHomeViewUiBinder extends UiBinder<Widget, StudentHomeViewImpl> {
	}

	static StudentResources resources = StudentResources.INSTANCE;

	private static StudentHomeViewUiBinder uiBinder = GWT.create(StudentHomeViewUiBinder.class);

	static {
		StudentResources.INSTANCE.starlightCSS().ensureInjected();
	}

	@UiField
	SimplePanel pagePanel;
	@UiField
	VerticalPanel chatPanel;
	@UiField
	AbsolutePanel helperPanel;
	@UiField
	Image lionImage;
	@UiField
	Button messageButton;
	@UiField
	RichTextArea messageBox;

	MessageView messageView;

	private Presenter presenter;

	private ToolTip tooltip;

	public StudentHomeViewImpl() {
		initWidget(uiBinder.createAndBindUi(this));
		lionImage.setUrl(resources.leeuAf().getSafeUri());
		lionImage.getElement().getStyle().setPosition(Position.ABSOLUTE);
		messageBox.getElement().getStyle().setBackgroundColor("#E5C2F0");
		messageBox.setEnabled(false);
		lionImage.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				displayTooltip("Hello there!");

			}
		});
		messageBox.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				DeferredCommand.add(new Command() {
					public void execute() {
						messageBox.setFocus(false);
					}
				});
				event.stopPropagation();
				messageButton.click();
			}
		});
		messageBox.addMouseDownHandler(new MouseDownHandler() {

			@Override
			public void onMouseDown(MouseDownEvent event) {
				DeferredCommand.add(new Command() {
					public void execute() {
						messageBox.setFocus(false);
					}
				});
				event.stopPropagation();
				messageBox.setFocus(false);

			}
		});
	}

	public void bind() {
		displayTooltip("Welkom!<br>Kies een van die klasse om te begin.");
	}

	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = presenter;
		bind();
	}

	public HasWidgets getPageContainer() {
		return pagePanel;
	}

	@Override
	public Widget asWidget() {
		return this;
	}

	public void displayTooltip(String message) {
		int left = ((Window.getClientWidth() > 1348) ? (Window.getClientWidth() - 1348) / 2 : 0);
		tooltip = new ToolTip(left + 108, 340, message);

		final Timer hideTimer = new Timer() {
			public void run() {
				tooltip.hide();
				lionImage.setUrl(resources.leeuAf().getSafeUri());
			}
		};

		Timer showTimer = new Timer() {
			public void run() {
				tooltip.show();
				hideTimer.schedule(5000);
			}
		};
		lionImage.setUrl(resources.leeuOp().getSafeUri());
		showTimer.schedule(400);
	}

	@UiHandler("messageButton")
	void onMessageButtonClick(ClickEvent event) {
		if (messageView == null)
			messageView = new MessageViewImpl();
		this.presenter.showMessageDialog(messageView);

	}

	public void addMessage(String messageHTML, String imageString) {
		// String image;
		// if (imageString != null) {
		// String encodedData = "data:image/" + imageString;
		// image = "<img src=\"" + encodedData + "\" style='padding: 4px;height: 35px' />";
		// } else {
		// ImageResource im = resources.defaultIcon();
		// image = "<img src=\"" + im.getURL() + "\" style='padding: 4px;height: 35px' />";
		// }

		messageBox.setHTML(messageBox.getHTML() + imageString + ": " + messageHTML + "<hr />");
	}
}
