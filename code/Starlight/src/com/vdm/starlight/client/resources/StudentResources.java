package com.vdm.starlight.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface StudentResources extends ClientBundle {

	public static final StudentResources INSTANCE = GWT.create(StudentResources.class);

	@Source("spinner.gif")
	ImageResource spinner();

	@Source("big-spinner.gif")
	ImageResource bigSpinner();

	@Source("speech_bubble.png")
	ImageResource speechBubble();

	@Source("Leeu-op.gif")
	ImageResource leeuOp();

	@Source("Leeu-af.gif")
	ImageResource leeuAf();

	@ImageOptions(repeatStyle = RepeatStyle.Horizontal)
	@Source("toolbarbg.png")
	ImageResource toolbarbg();

	@Source("toolbarHoverbg.jpg")
	ImageResource toolbarHoverbg();

	@Source("right_arrow.png")
	ImageResource rightArrow();

	@Source("left_arrow.png")
	ImageResource leftArrow();

	@Source("secondary_nav_bg.png")
	ImageResource secondaryNavBg();

	@Source("StarLightLogo.png")
	ImageResource starLightLogo();

	@Source("book.jpg")
	ImageResource book();

	@Source("geelDeur.png")
	ImageResource deur();

	@Source("default_icon.jpg")
	ImageResource defaultIcon();

	@Source("purple-stars-background.png")
	ImageResource purpleStars();

	@Source("starlightStudent.css")
	Style starlightCSS();

	public interface Style extends CssResource {
		String flowPanel();

		String imageAnimation();

		String divContainer();

		String divToolbar();

		String toolBar();

		String divBody();

		String starlightText();

		String menuBar();

		String pagerLabelRight();

		String pagerLabelLeft();

		String menuBarAnchor();

		String page();

		String logoutPanel();

		String windowHeading();

		String subHeading();

		String scrollPanel();

		String transPanel();

		String fitToParent();

		String loading();

		String imagePanel();

		String speechBubble();

		// ***********//
		String title();

		String heading();

		String headingblue();

		String subTitle();

		String titleDivMargin();

		String windowDiv();

		String formInput();

		String formLabel();

		String backPanel();

		String footer();

		String blackHeading();

		String pager();

		String anchorLinks();
	}

}
