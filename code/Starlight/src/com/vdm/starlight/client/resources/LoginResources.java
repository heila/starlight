package com.vdm.starlight.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface LoginResources extends ClientBundle {

	public static final LoginResources INSTANCE = GWT.create(LoginResources.class);

	@ImageOptions(repeatStyle = RepeatStyle.Both)
	@Source("stars.jpg")
	ImageResource stars();

	@Source("StarLightLogoLarge.png")
	ImageResource starLightLogo();

	@Source("starlightLogin.css")
	Style loginCSS();

	public interface Style extends CssResource {

		String loginWindow();

		String divBody();

		String starLightLogo();

	}

}
