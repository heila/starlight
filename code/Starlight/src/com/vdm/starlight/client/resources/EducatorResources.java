package com.vdm.starlight.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.resources.client.ImageResource;
import com.google.gwt.resources.client.ImageResource.ImageOptions;
import com.google.gwt.resources.client.ImageResource.RepeatStyle;

public interface EducatorResources extends ClientBundle {

	public static final EducatorResources INSTANCE = GWT.create(EducatorResources.class);

	@Source("spinner.gif")
	ImageResource spinner();

	@Source("big-spinner.gif")
	ImageResource bigSpinner();

	@ImageOptions(repeatStyle = RepeatStyle.Horizontal)
	@Source("toolbarbg.png")
	ImageResource toolbarbg();

	@Source("toolbarHoverbg.jpg")
	ImageResource toolbarHoverbg();

	@Source("default_profile_image.png")
	ImageResource defaultProfile();

	@Source("right_arrow.png")
	ImageResource rightArrow();

	@Source("left_arrow.png")
	ImageResource leftArrow();

	@Source("secondary_nav_bg.png")
	ImageResource secondaryNavBg();

	@Source("StarLightLogo.png")
	ImageResource starLightLogo();

	@Source("book.jpg")
	ImageResource book();

	@Source("geelDeur.png")
	ImageResource deur();

	@Source("default_icon.jpg")
	ImageResource defaultIcon();

	@Source("newMessage.png")
	ImageResource newMessage();

	@Source("noMessage.png")
	ImageResource noMessage();

	@Source("starlightEducator.css")
	Style starlightCSS();

	public interface Style extends CssResource {

		String divContainer();

		String divToolbar();

		String toolBar();

		String divBody();

		String starlightText();

		String menuBar();

		String menuBarAnchor();

		String page();

		String logoutPanel();

		String pagerLabelRight();

		String pagerLabelLeft();

		String anchor();

		String windowHeading();

		String subHeading();

		String secondaryNavigation();

		String currentPage();

		String scrollPanel();

		String transPanel();

		String pictureGrid();

		String helpTip();

		String fitToParent();

		String loading();

		String imagePanel();

		// ***********//
		String title();

		String heading();

		String headingblue();

		String subTitle();

		String titleDivMargin();

		String titleDiv();

		String windowDiv();

		String formInput();

		String formLabel();

		String image();

		String backPanel();

		String footer();

		String blackHeading();

		String newMessage();

		String noNewMessage();

		String chatPanel();

		String messagePanel();

	}

}
