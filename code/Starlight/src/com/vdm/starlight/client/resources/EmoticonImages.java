package com.vdm.starlight.client.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

public interface EmoticonImages extends ClientBundle {
	public static final EmoticonImages INSTANCE = GWT.create(EmoticonImages.class);

	@Source("angry.gif")
	ImageResource angry();

	@Source("frustrated.gif")
	ImageResource frustrated();

	@Source("hungry.gif")
	ImageResource hungry();

	@Source("sad.gif")
	ImageResource sad();

	@Source("sick2.gif")
	ImageResource sick2();

	@Source("ashamed.gif")
	ImageResource ashamed();

	@Source("greet2.gif")
	ImageResource greet2();

	@Source("music.gif")
	ImageResource music();

	@Source("schocked2.gif")
	ImageResource schocked2();

	@Source("sick.gif")
	ImageResource sick();

	@Source("birthday.gif")
	ImageResource birthday();

	@Source("greet.gif")
	ImageResource greet();

	@Source("no.gif")
	ImageResource no();

	@Source("shocked2.gif")
	ImageResource shocked2();

	@Source("sleep.gif")
	ImageResource sleep();

	@Source("confused.gif")
	ImageResource confused();

	@Source("happy.gif")
	ImageResource happy();

	@Source("sad2.gif")
	ImageResource sad2();

	@Source("shocked.gif")
	ImageResource shocked();

	@Source("thirsty.gif")
	ImageResource thirsty();

	@Source("eat.gif")
	ImageResource eat();

	@Source("hug.gif")
	ImageResource hug();

	@Source("sad3.gif")
	ImageResource sad3();

	@Source("shy.gif")
	ImageResource shy();

	@Source("toilet.gif")
	ImageResource toilet();

}
