package com.vdm.starlight.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.vdm.starlight.shared.dto.UserDTO;
import com.vdm.starlight.shared.objects.ChatException;
import com.vdm.starlight.shared.objects.StatusUpdate.Status;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("chat")
public interface MessagingService extends RemoteService {

	UserDTO getUsername() throws ChatException;

	void logout(String username) throws ChatException;

	public void send(String message, String receiver) throws ChatException;

	void setStatus(Status status) throws ChatException;

	boolean isValidSession(int id);

	int authenticate(String username, String password);

}
