package com.vdm.starlight.client;

import java.util.List;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.vdm.starlight.shared.dto.ClassroomDTO;
import com.vdm.starlight.shared.dto.EducatorDTO;
import com.vdm.starlight.shared.dto.ResourceDTO;
import com.vdm.starlight.shared.dto.StudentDTO;
import com.vdm.starlight.shared.dto.WorkbookDTO;
import com.vdm.starlight.shared.dto.WorkbookPagesDTO;
import com.vdm.starlight.shared.dto.WorkbookStatsDTO;
import com.vdm.starlight.shared.dto.WorkpageDTO;
import com.vdm.starlight.shared.dto.WorkpageDataDTO;

/**
 * The client side stub for the RPC service.
 */
@RemoteServiceRelativePath("dbmanager")
public interface DatabaseService extends RemoteService {

	void addStudentToClassroom(int studentId, int classroomId);

	void addWorkbookToClassroom(int workbookId, int classroomId);

	void addWorkbookToClassroomStudents(int workbookId, int classroomId);

	void addWorkbookToStudent(int workbookId, int studentId, int classroomId);

	/* ********************************* CREATE ************************************* */

	int createEducator(String userName, String password, String name, String surname, String email);

	int createResource(String name, String type, String path, String imageData);

	int createStudent(String userName, String password, String name, String surname, int age);

	int createStudent(String userName, String password, String name, String surname, int age, int imageID);

	int createWorkbook(String name, String description, String ageGroup, int creatorId);

	int createWorkbook(String name, String description, String ageGroup, int creatorId, int imageID);

	int createWorkpage(String name, int workbookId, int pageNumber);

	/* *************************************************************************** */

	void deleteResource(int resourceID);

	/* ********************************** GET ********************************* */

	List<ResourceDTO> getAllResources(int start);

	ClassroomDTO getClassroom(int classroomID, boolean fullImage);

	List<StudentDTO> getClassroomStudents(int classroomId, boolean fullImage);

	List<WorkbookDTO> getClassroomWorkbooks(int classId, boolean fullImage);

	String getDummyImage();

	public EducatorDTO getEducator(int id);

	List<ClassroomDTO> getEducatorClassrooms(int educatorId, int start, int end, boolean fullImage);

	List<StudentDTO> getEducatorStudents(int educatorId, int start, int end, boolean fullImage);

	List<WorkbookDTO> getEducatorWorkbooks(int educatorID, int start, int end, boolean fullImage);

	String getFullImage(int id);

	WorkpageDTO getNewWorkpage(String name, int workbookId, int pageNumber);

	int getNumberOfPages(int workbookId);

	StudentDTO getStudent(int studentId, boolean fullImage);

	WorkbookDTO getWorkbook(int workbookId, boolean fullImage);

	List<ClassroomDTO> getWorkbookClassrooms(int workbookId, int start, int end, boolean b);

	List<WorkbookDTO> getWorkbooks();

	WorkbookStatsDTO getWorkbookStat(int workbookId);

	List<StudentDTO> getWorkbookStudents(int workbookId, boolean fullImage);

	List<StudentDTO> getWorkbookStudents(int filter, int start, int end, boolean b);

	WorkpageDataDTO getWorkpage(int workbookId, int pagenumber);

	String getWorkpageComment(int studentId, int workpageId);

	List<String> getWorkpageData(int workbookId);

	/* *************************************************************************** */

	WorkbookPagesDTO getWorkpages(int workpageId);

	/* ********************************** SAVE ********************************* */

	List<String> getWorkpageStudentData(int studentId, int workpageId);

	int saveImage(String filename);

	/* *********************************** SET *********************************** */

	void saveWorkbook(WorkbookDTO WorkbookDTO, int imageID);

	void saveWorkpageDescription(int workpageID, String description, int uniqueID);

	void setClassroomImage(int classroomId, int resourceId);

	void setEducatorImage(int educatorId, int resourceId);

	void setStudentImage(int studentId, int resourceId);

	void setStudentWorkpageData(String data, int studentId, int workpageId);

	void setWorkbookImage(int workbookId, int resourceId);

	void setWorkpageComment(int studentId, int workpageId, String comment);

	List<ClassroomDTO> getStudentClassrooms(int studentId, boolean fullImage);

	List<ClassroomDTO> getStudentClassrooms(int studentId, int start, int end, boolean fullImage);

	List<WorkbookDTO> getStudentWorkbooks(int studentId, boolean fullImage);

	List<WorkbookDTO> getStudentWorkbooks(int studentId, int classId, boolean fullImage);

	List<WorkbookDTO> getStudentWorkbooks(int studentId, int start, int end, boolean fullImage);

	List<ClassroomDTO> getWorkbookAvailableClassrooms(int educatorId, int workbookId, int start, int end,
			boolean fullImage);

	int createClassroom(String name, String description, String ageGroup, int creatorId, int imageID);

	void saveClassroom(ClassroomDTO w, int imageID);

	int createClassroom(String name, int creatorId, String ageGroup, String description);

}
