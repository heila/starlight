package com.vdm.starlight.client.presenter;

import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.ui.HasWidgets;
import com.vdm.starlight.client.events.LogInEvent;
import com.vdm.starlight.client.events.LogInEvent.UserType;
import com.vdm.starlight.client.view.LogInView;
import com.vdm.starlight.client.view.LogInViewImpl;

public class LogInPresenter implements Presenter, LogInView.Presenter {

	private final HandlerManager eventBus;
	private final LogInViewImpl view;

	public LogInPresenter(HandlerManager eventBus, LogInViewImpl view) {
		this.eventBus = eventBus;
		this.view = view;
		this.view.setPresenter(this);
	}

	@Override
	public void onLogInButtonClicked(String name, String password, boolean isEducator) {
		eventBus.fireEvent(new LogInEvent(name, password, ((isEducator) ? UserType.EDUCATOR : UserType.STUDENT)));
	}

	@Override
	public void go(final HasWidgets container) {
		container.clear();
		container.add(view.asWidget());
		view.setFocus();
	}

}
